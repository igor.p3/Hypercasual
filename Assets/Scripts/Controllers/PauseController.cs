﻿using UnityEngine;
using System;

namespace Matchmania
{
    public class PauseController : MonoBehaviour
    {
        public static event Action PauseOnGameEventHandler;
        public static event Action PauseOffGameEventHandler;

        public static void PauseGame(bool isOnPause)
        {
            if (isOnPause)
            {
                if (LevelController.Instance.IsPlayingLevel)
                {
                    PauseOnGameEventHandler?.Invoke();
                }
            }
            else
            {
                PauseOffGameEventHandler?.Invoke();
            }
        }
    }
}