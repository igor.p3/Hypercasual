﻿using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

namespace Matchmania
{
    public class PauseView : MonoBehaviour
    {
        [SerializeField] private LevelView levelView;
        [SerializeField] private GameObject pauseScreen;

        [Header("Buttons")]

        [SerializeField] private Button restartButton;
        [SerializeField] private Button homeButton;
        [SerializeField] private Button closeButton;

        void Start()
        {
            PauseController.PauseOnGameEventHandler += OpenPauseScreen;
            PauseController.PauseOffGameEventHandler += ClosePauseScreen;

            homeButton.onClick.AddListener(HomeButtonPressed);
            restartButton.onClick.AddListener(RestartButtonPressed);
            closeButton.onClick.AddListener(CloseButtonPressed);
        }
        private void HomeButtonPressed()
        {
            PauseController.PauseGame(false);
            levelView.OnHomeButtonPressed();
        }
        private void RestartButtonPressed()
        {
            PauseController.PauseGame(false);
            levelView.RestartLevelButtonPressed();
        }
        private void CloseButtonPressed()
        {
            PauseController.PauseGame(false);
        }
        private void ClosePauseScreen()
        {
            LevelView.CloseScreen(pauseScreen);
            Time.timeScale = 1;
            LevelController.Instance.IsPlayingLevel = true;
            LevelController.Instance.IsCanMakeTurn = true;
        }
        private void OpenPauseScreen()
        {
            pauseScreen.SetActive(true);
            pauseScreen.transform.GetChild(0).GetComponent<Image>().DOFade(0.7f, 0);
            pauseScreen.transform.localScale = Vector3.one;
            Time.timeScale = 0;
            LevelController.Instance.IsPlayingLevel = false;
        }
    }
}