using UnityEngine;
using Dreamteck.Splines;
using DG.Tweening;

namespace Matchmania
{
    public class StarUIEffect : MonoBehaviour
    {
        [SerializeField] private VFXConfig VFX_Config;
        [SerializeField] private SFXConfig SFX_Config;

        private SplineFollower splineFollower;
        void Start()
        {
            splineFollower = GetComponent<SplineFollower>();

            //transform.DOScale(Vector3.one, 0.5f);
        }
        void Update()
        {
            SplineSample result = splineFollower.result;

            if (result.percent >= 0.96d)
            {
                //Vector3 worldPos = Camera.main.ViewportToWorldPoint(GetComponent<RectTransform>().position);
                //var effect = VFX_Config.PlayEffect(worldPos, VFX_Config.starGlow);
                //effect.transform.SetParent(transform);
                //effect.transform.localPosition = Vector3.zero;
                //effect.transform.parent = null;
                SFX_Config.PlaySoundEffect(SFX_Config.starHitsUI);
                Destroy(gameObject);
            }
        }
    }
}