﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using Lean.Pool;
using HyperCasualTemplate;
using DG.Tweening;
using MoreMountains.NiceVibrations;

namespace Matchmania
{
    public class LevelController : MonoBehaviour
    {
        #region Singleton
        private static LevelController _instance;
        public static LevelController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<LevelController>();
                }

                return _instance;
            }
        }
        #endregion

        public event Action StartLevelEventHandler;
        public event Action MarchEventHandler;
        public event Action VictoryEventHandler;
        public event Action<bool> DefeatEventHandler;

        [SerializeField] private LevelView levelView;
        [SerializeField] private VFXConfig VFXConfig;
        [SerializeField] private Camera UICamera;
        [SerializeField] private Canvas canvas;

        [SerializeField] private Animator hintTextAnimator;
        [SerializeField] private Animator fingerAnimator;

        public const float comboBonusTimerMaxStart = 5f;
        public  float comboBonusTimerMax;
        public float comboBonusTimer  { get;set; } = 0f;
        public int comboBonusIndex { get; set; } = 1;
        public int maxCombo { get; set; }
        public List<ItemController> CurrentLevelItems { get; set; } = new List<ItemController>();
        public LevelModel CurrentLevelModel { get; set; }
        public static float Timer { get; set; }
        public bool IsPlayingLevel { get; set; }
        public bool IsCanMakeTurn { get; set; }
        public bool isReviveScreenOpened { get; set; }

        private float timerSpeed = 1;

        public ItemSlotController[] slots;
        private static int _currentLevelIndex;
        public static int CurrentLevelIndex
        {
            get => _currentLevelIndex;
            set
            {
                _currentLevelIndex = value;
                OnLevelNumChanged?.Invoke(value);
            }
        }
        public static event Action<int> OnLevelNumChanged;
        public int CurrentLevelStars { get; set; }
        public bool isOutOfBox { get; set; }

        private bool isCoroutineWork;

        public bool isMarching;

        private bool isToSpeedUpTimer;

        public static bool isRestartLevelAfterInterstitional = false;

        public static bool isCanShowInterstitialAfterHomeAndRestartButtons = true;

        public static int interstitialAfterHomeAndRestartButtonsCooldown = 90;
        public static float floatinterstitialAfterHomeAndRestartButtonTimer;
        public void StartLevel(int index)
        {
            ClearLevel();
            comboBonusIndex = 1;
            comboBonusTimer = -1;
            isReviveScreenOpened = false;

            BannerController.CheckBanner();

            if (CurrentLevelIndex == 0)
            {
                fingerAnimator.gameObject.SetActive(true);
            }
            if(CurrentLevelIndex == 2)
            {
                PushNotificationController.Instance.RequestAuthorization();
            }

            PlayerController.ClearSelectedItems();
            comboBonusTimerMax = comboBonusTimerMaxStart;

            isToSpeedUpTimer = false;
            IsPlayingLevel = true;

            if (index >= GameModel.Levels.Levels.Length - 1)
            {
                index = GameModel.Levels.Levels.Length - 1;
            }
            CurrentLevelModel = GameModel.Levels.Levels[index];
            CurrentLevelIndex = index;
            maxCombo = 0;

            AdvertisementSystem.ShowBanner();
           HintController.Instance.CheckBoostersIcons();

            if (index > 0)
            {
                if (GameController.isFirstSession == true)
                {
                    GameController.isFirstSession = false;
                    GameController.SaveGameData();
                }
            }

            StartLevelEventHandler?.Invoke();

            isRestartLevelAfterInterstitional = false;

            AdvertisementSystem.isCanShowInactiveAdd = false;


        }
        private void Start()
        {
            floatinterstitialAfterHomeAndRestartButtonTimer = interstitialAfterHomeAndRestartButtonsCooldown;
        }
        private void Update()
        {
            if (IsPlayingLevel)
            {
                Timer -= Time.deltaTime;

                if (Timer <= 0)
                {
                    DefeatLogic(false);
                }

                if (comboBonusTimer >= 0)
                {
                    comboBonusTimer -= Time.deltaTime;
                }
                else
                {
                    comboBonusIndex = 1;
                    comboBonusTimerMax = comboBonusTimerMaxStart;
                }
                if (LevelController.Instance.IsPlayingLevel == true)
                {
                    AudioController.Instance.TurnOnMusicAL(true);
                }
                timerSpeed = 1;
            }
            else if (IsPlayingLevel == false && isToSpeedUpTimer == true)
            {
                Timer -= Time.deltaTime * timerSpeed;

                timerSpeed += Time.deltaTime * 40;

                if (Timer <= 0)
                {
                    isToSpeedUpTimer = false;
                }
            }
            floatinterstitialAfterHomeAndRestartButtonTimer -= Time.deltaTime;

            if (floatinterstitialAfterHomeAndRestartButtonTimer < 0)
            {
                isCanShowInterstitialAfterHomeAndRestartButtons = true;
            }
            else isCanShowInterstitialAfterHomeAndRestartButtons = false;


        }
        public void ClearLevel()
        {
            for (int i = 0; i < CurrentLevelItems.Count; i++)
            {
                LeanPool.Despawn(CurrentLevelItems[i]);
            }
            CurrentLevelItems.Clear();

            foreach (var slot in slots)
            {
                slot.ItemController = null;
            }
        }
        private void VictoryLogic()
        {
            isToSpeedUpTimer = true;
            VictoryEventHandler?.Invoke();
            AnalyticsEvents.ExecuteEvent("Level_" + (CurrentLevelIndex + 1).ToString());

            DailyBonusController.CheckDailyBonus();
            IsPlayingLevel = false;
            GameController.isFirstSession = false;
            GameController.SaveGameData();

          
        }
        private void DefeatLogic(bool isOutOfBox)
        {
            DefeatEventHandler?.Invoke(isOutOfBox);
            IsPlayingLevel = false;
            IsCanMakeTurn = false;
            isReviveScreenOpened = true;
        }
        public static ItemController GetItemByName(string name)
        {
            ItemController itemController = null;

            foreach (var item in GameController.items)
            {
                if (name == item.name)
                {
                    itemController = item;
                    break;
                }
            }

            return itemController;
        }
        public void CheckTurnResult()
        {
            //StopAllCoroutines();

            StartCoroutine(CheckMatchedItems(0));
            StartCoroutine(CheckMatchedItems(1));
            StartCoroutine(CheckMatchedItems(2));
            StartCoroutine(CheckMatchedItems(3));
            StartCoroutine(CheckMatchedItems(4));

            CheckIfAllSlotsAreFull();
            CheckIfAllSlotsAreEmpty();

            fingerAnimator.gameObject.SetActive(false);
        }
        public IEnumerator SetCanMakeTurn(float timer)
        {
            if (isCoroutineWork == false)
            {
                isCoroutineWork = true;
                IsCanMakeTurn = false;
                yield return new WaitForSeconds(timer);
                IsCanMakeTurn = true;
                isCoroutineWork = false;
            }
            else yield return null;
        }
        public int CheckIfAllSlotsAreFull()
        {
            int itemsCount = 0;

            foreach (var slot in slots)
            {
                if (slot.ItemController != null)
                {
                    itemsCount++;
                }
            }
            if (itemsCount == 7)
            {
                if (LevelController.CurrentLevelIndex <= 4)
                {
                    HintController.Instance.BackItemsToPlayfiled();
                    hintTextAnimator.gameObject.SetActive(true);
                    hintTextAnimator.Play("Play");
                }
                else
                {
                    DefeatLogic(true);
                    IsCanMakeTurn = false;
                }
            }
            return itemsCount;
        }
        private void CheckIfAllSlotsAreEmpty()
        {
            CurrentLevelItems.RemoveAll(x => x == null);

            if (CurrentLevelItems.Count == 0)
            {
                VictoryLogic();
            }
        }
        private IEnumerator CheckMatchedItems(int startIndex)
        {
            CurrentLevelItems.RemoveAll(x => x == null);

            if (slots[startIndex].ItemController != null && slots[startIndex + 1].ItemController != null && slots[startIndex + 2].ItemController != null)
            {
                if (slots[startIndex].ItemController.name == slots[startIndex + 1].ItemController.name && slots[startIndex + 1].ItemController.name == slots[startIndex + 2].ItemController.name)
                {

                    ItemController item_01 = slots[startIndex].ItemController;
                    ItemController item_02 = slots[startIndex + 1].ItemController;
                    ItemController item_03 = slots[startIndex + 2].ItemController;

                    CurrentLevelItems.Remove(item_03);
                    CurrentLevelItems.Remove(item_02);
                    CurrentLevelItems.Remove(item_01);

                    slots[startIndex].ItemController = null;
                    slots[startIndex + 1].ItemController = null;
                    slots[startIndex + 2].ItemController = null;

                    while (Vector3.Distance(slots[startIndex + 2].transform.position, item_03.transform.position) > 1)
                    {
                        yield return null;
                    }

                    item_01.transform.DOScale(0.1f, 1f);
                    item_02.transform.DOScale(0.1f, 1f);
                    item_03.transform.DOScale(0.1f, 1f);


                    item_01.StartCoroutine(item_01.MoveToTarget(item_02.transform.position));
                    item_03.StartCoroutine(item_03.MoveToTarget(item_02.transform.position, () =>
                        {
                            VFXConfig.PlayEffect(slots[startIndex + 1].transform.position + Vector3.up, VFXConfig.matchEffect, new Vector3(90, 0, 0));

                            StartCoroutine(CreateStarEffect(comboBonusIndex, startIndex));

                            if (SettingsController.isVibrationOn == true)
                                MMVibrationManager.Haptic(HapticTypes.SoftImpact, false, true, this);

                            LeanPool.Despawn(item_01.gameObject);
                            LeanPool.Despawn(item_02.gameObject);
                            LeanPool.Despawn(item_03.gameObject);

                            comboBonusIndex++;
                            if (comboBonusIndex > maxCombo)
                            {
                                maxCombo = comboBonusIndex;
                            }
                            if(comboBonusTimerMax >= 1.5f)
                            {
                                comboBonusTimerMax -= 0.5f;
                            }
                            comboBonusTimer = comboBonusTimerMax;
                        }));
                    CurrentLevelStars += comboBonusIndex;
                    MarchEventHandler?.Invoke();
                    MoveItemsPositions();
                }
            }
        }
        public void MoveItemsPositions()
        {

            for (int i = 1; i < slots.Length; i++)
            {
                for (int j = 0; j < slots.Length; j++)
                {
                    if (slots[i].ItemController != null)
                    {
                        if (j < i)
                        {
                            if (slots[j].ItemController == null)
                            {
                                slots[i].StopAllCoroutines();
                                slots[i].StartCoroutine(slots[i].ItemController.MoveToTarget(slots[j].transform.position, null));
                                slots[j].ItemController = slots[i].ItemController;
                                slots[i].ItemController = null;
                                break;
                            }
                        }
                        else break;
                    }
                }
            }
        }
        IEnumerator CreateStarEffect(int count, int startIndex)
        {
            for (int i = 0; i < count; i++)
            {
              
                    GameObject starEffect = VFXConfig.PlayEffect(Vector3.zero, VFXConfig.star, Vector3.zero);

                    starEffect.transform.SetParent(canvas.transform);

                    RectTransform rect = starEffect.GetComponent<RectTransform>();

                    Vector3 startPosition = CanvasPositioningExtensions.WorldToCanvasPosition(canvas, slots[startIndex + 1].transform.position);

                    //var random = UnityEngine.Random.Range(-75f, 75f);

                    //startPosition.x -= random;
                    //startPosition.y -= random;
  
                    rect.localPosition = startPosition;

                    rect.transform.localScale = Vector3.one;

                rect.localScale = new Vector3(0.1f, 0.1f, 0.1f);

                rect.transform.DOScale(1, 0.1f);

            yield return new WaitForSeconds(0.075f);
            }
        }
    }
}
//public void LoadSavedGameLevelProgression(Transform itemParent)
//{
//    foreach (var item in GameController.SavedData.LevelProgressData.itemsData)
//    {
//        string name = item.name;

//        name = name.Remove(name.Length - 7);
//        var itemGO = Instantiate(GetItemByName(name));

//        itemGO.name = item.name;

//        Vector3 position;

//        position.x = item.positionX;
//        position.y = item.positionY;
//        position.z = item.positionZ;

//        itemGO.transform.parent = itemParent;
//        itemGO.transform.localPosition = position;

//        itemGO.transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);

//        CurrentLevelItems.Add(itemGO);

//        if (item.slotIndex != -1)
//        {
//            slots[item.slotIndex].ItemController = itemGO;
//            Destroy(itemGO.GetComponent<Rigidbody>());
//            itemGO.GetComponent<Collider>().enabled = false;
//            itemGO.isRotate = true;
//            itemGO.transform.position = slots[item.slotIndex].transform.position + Vector3.up;
//            itemGO.transform.localScale = Vector3.one;
//        }

//    }
//}