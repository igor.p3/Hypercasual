using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using HyperCasualTemplate;
using Matchmania;
using UnityEngine;
using VoxelBusters.EssentialKit;

public class AchievementsController : MonoBehaviour
{
    private enum Achievement
    {
        Level5 = 0,
        Level10 = 1,
        Level15 = 2,
        Level20 = 3,
        Level50 = 4,
        Level100 = 5,
        Level150 = 6,
        Level200 = 7,
        Level500 = 8,
        Level1000 = 9,
        Level2000 = 10,
    }

    private void Start()
    {
        // Check for all achievements that player should have already got, if he didn't - give him achievements
        foreach (var lvlNum in Enumerable.Range(0,LevelController.CurrentLevelIndex))
        {
            LevelCheckForAchievement(lvlNum);
        }

        AdvertisementSystem.OnEndRewardVideo += () => LevelCheckForAchievement(LevelController.CurrentLevelIndex);
        MaxSdkCallbacks.OnInterstitialHiddenEvent += (s) => LevelCheckForAchievement(LevelController.CurrentLevelIndex);
        //LevelController.OnLevelNumChanged += LevelCheckForAchievement;
    }

    private static void ReportAchievement(string achievementId, double percentageCompleted)
    {
        GameServices.ReportAchievementProgress(achievementId, percentageCompleted, (error) =>
        {
            if (error == null)
            {
                //Debug.Log("Request to submit progress finished successfully.");
            }
            else
            {
                Debug.LogError("Request to submit progress failed with error. Error: " + error);
            }
        });
    }

    private static void MarkAchievementCompleted(Achievement achievement)
    {
        var achID = ((int)achievement).ToString();
        ReportAchievement(achID, 100);
        
        // GameServices.ShowAchievements((result, error) =>
        // {
        //     Debug.Log("Achievements view closed");
        // });

        Debug.Log($"Achievement {achID} get!");
    }

    public static void LevelCheckForAchievement(int lvl)
    {
        var actualLvl = lvl + 1;
        switch (actualLvl)
        {
            case 5:
                MarkAchievementCompleted(Achievement.Level5);
                break;
            case 10:
                MarkAchievementCompleted(Achievement.Level10);
                break;
            case 15:
                MarkAchievementCompleted(Achievement.Level15);
                break;
            case 20:
                MarkAchievementCompleted(Achievement.Level20);
                break;
            case 50:
                MarkAchievementCompleted(Achievement.Level50);
                break;
            case 100:
                MarkAchievementCompleted(Achievement.Level100);
                break;
            case 150:
                MarkAchievementCompleted(Achievement.Level150);
                break;
            case 200:
                MarkAchievementCompleted(Achievement.Level200);
                break;
            case 500:
                MarkAchievementCompleted(Achievement.Level500);
                break;
            case 1000:
                MarkAchievementCompleted(Achievement.Level1000);
                break;
            case 2000:
                MarkAchievementCompleted(Achievement.Level2000);
                break;
        }
    }
}
