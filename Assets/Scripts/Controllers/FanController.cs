using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using HyperCasualTemplate;

namespace Matchmania
{
    public class FanController : MonoBehaviour
    {
        #region Singleton
        private static FanController _instance;
        public static FanController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<FanController>();
                }

                return _instance;
            }
        }
        #endregion

        public Action FanUsedEventHandler;

        [SerializeField] private SFXConfig SFXConfig;

        private bool isCanUseFan = true;

        public void UseFan()
        {
            if (CurrencyController.Fans > 0)
            {
                StartCoroutine(_UseFan());
            }
            else
            {
                if(AdvertisementSystem.IsRewardedAdReady())
                {
                    AdvertisementSystem.ShowRewardedAd();
                    CurrencyController.Fans++;
                   
                }
            }
            HintController.Instance.CheckBoostersIcons();
        }

        private IEnumerator _UseFan()
        {
            isCanUseFan = false;
            CurrencyController.Fans--;
            FanUsedEventHandler?.Invoke();

            foreach (var item in LevelController.Instance.CurrentLevelItems)
            {
                if(item.isRotate == false)
                {
                    float rndY = UnityEngine.Random.Range(5, 10); 
                    float rndZX = UnityEngine.Random.Range(-3, 3);
                    item.GetComponent<Rigidbody>().velocity = new Vector3(0, rndY, rndZX);
                }
            }
            GameController.SaveGameData();
            yield return new WaitForSeconds(1f);
            isCanUseFan = true;
        }
    }
}