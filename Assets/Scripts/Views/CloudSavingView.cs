using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VoxelBusters.EssentialKit;

public class CloudSavingView : MonoBehaviour
{

    [SerializeField] private Button signInButton, signOutButton, loadProgressButton;

    private void Start()
    {
        signInButton.onClick.AddListener(CloudSavingController.Instance.SignIn);
        signOutButton.onClick.AddListener(CloudSavingController.Instance.SignOut);
        loadProgressButton.onClick.AddListener(CloudSavingController.LoadProgressButton);
        UpdateButtons();
    }

    public void UpdateButtons()
    {
        if (GameServices.IsAuthenticated)
        {
            signInButton.gameObject.SetActive(false);
            signOutButton.gameObject.SetActive(true);
            loadProgressButton.gameObject.SetActive(true);
        }
        else
        {
            signInButton.gameObject.SetActive(true);
            signOutButton.gameObject.SetActive(false);
            loadProgressButton.gameObject.SetActive(false);
        }
    }
}
