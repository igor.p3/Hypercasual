﻿using Matchmania;
using System.Collections.Generic;
using UnityEngine;

namespace Matchmania
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float speed = 15f;
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private SFXConfig SFXConfig;

        public static List<List<ItemController>> selectedItems = new List<List<ItemController>>();

        public static Vector3 startItemPosition;
        public static Vector3 targetItemPosition;
        public static float startTime;
        public static float journeyLength;

        private void Start()
        {
        
        }

        public static void ClearSelectedItems()
        {
            selectedItems.Clear();
            for (int i = 0; i < 20; i++)
            {
                List<ItemController> items = new List<ItemController>();
                selectedItems.Add(items);
            }
        }
        void Update()
        {
            PlayerTurn();
        }
        private void PlayerTurn()
        {
            RaycastHit hit;

            var tapCount = Input.touchCount;
            for (var k = 0; k < tapCount; k++)
            {
                var touch = Input.GetTouch(k);
                ItemController item = null;
                Transform itemTramsform = null;

                Ray ray = Camera.main.ScreenPointToRay(touch.position);

                if (Physics.Raycast(ray, out hit, 100f, layerMask))
                {
                    if (hit.collider.tag == "Item" && LevelController.Instance.IsCanMakeTurn)
                    {
                        if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Moved)
                        {
                            itemTramsform = hit.collider.transform;
                            item = hit.collider.GetComponent<ItemController>();
                            itemTramsform = hit.collider.transform;
                            startItemPosition = hit.collider.transform.position;
                            startTime = Time.time;
                            targetItemPosition = transform.position + Vector3.up;

                            journeyLength = Vector3.Distance(startItemPosition, targetItemPosition);

                            if (itemTramsform != null && itemTramsform == hit.collider.transform && LevelController.Instance.IsCanMakeTurn)
                            {
                                float distCovered = (Time.time - startTime) /** speed*/;
                                float fractionOfJourney = distCovered / journeyLength;
                                itemTramsform.position = Vector3.Lerp(startItemPosition, targetItemPosition, fractionOfJourney);
                                itemTramsform.GetComponent<ItemController>().SetSelectedItem(k);
                            }

                            if (touch.phase == TouchPhase.Ended /*&& LevelController.Instance.IsPlayingLevel && LevelController.Instance.IsCanMakeTurn*/)
                            {
                                if (item != null)
                                {
                                    ItemSlotController[] slots = LevelController.Instance.slots;

                                    item.DeselectItem();

                                    SFXConfig.PlaySoundEffect(SFXConfig.pickUpItem);

                                    for (int i = 0; i < slots.Length; i++)
                                    {
                                        if (slots[i].ItemController == null)
                                        {
                                            StartCoroutine(item.MoveToTarget(slots[i].transform.position, () =>
                                            {
                                                LevelController.Instance.CheckTurnResult();
                                            }));
                                            slots[i].ItemController = item;
                                            item.StopAllCoroutines();
                                            item.isMovingToBar = true;
                                            item.StartCoroutine(item.ScaleItem(item.slotScale));
                                            break;
                                        }
                                        else if (slots[i].ItemController != null && slots[i].ItemController.name == item.name)
                                        {
                                            for (int j = slots.Length - 1; j > i; j--)
                                            {
                                                if (slots[j].ItemController != null)
                                                {
                                                    if (j + 1 < slots.Length)
                                                    {
                                                        StartCoroutine(slots[j].ItemController.MoveToTarget(slots[j + 1].transform.position));
                                                        slots[j + 1].ItemController = slots[j].ItemController;
                                                    }
                                                    else return;
                                                }
                                            }
                                            if (i + 1 <= slots.Length)
                                            {
                                                StartCoroutine(item.MoveToTarget(slots[i + 1].transform.position, () =>
                                                {
                                                    LevelController.Instance.CheckTurnResult();
                                                }));
                                                item.StopAllCoroutines();
                                                item.isMovingToBar = true;
                                                item.StartCoroutine(item.ScaleItem(item.slotScale));
                                                slots[i + 1].ItemController = item;

                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            GameController.SaveGameData();
                        }
                    }
                    else if (hit.collider.tag != "Item")
                    {
                        if (selectedItems != null && selectedItems.Count != 0)
                        {
                            for (int i = 0; i < selectedItems[k].Count; i++)
                            {
                                if (selectedItems[k] != null)
                                {
                                    if (selectedItems[k][i] != null)
                                    {
                                        selectedItems[k][i].DeselectItem();
                                    }
                                }
                            }
                            selectedItems[k].Clear();
                        }
                    }
                }
            }
        }
    }
}