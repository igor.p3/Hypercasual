using UnityEngine;
using UnityEngine.UI;

namespace Matchmania
{
    public class SettingsView : MonoBehaviour
    {
        [Header("Buttons")]

        [SerializeField] private Button rateUsButton;
        [SerializeField] private Button vibrationButton;
        [SerializeField] private Button soundButton;
        [SerializeField] private Button musicButton;

        [Header("Sprites")]

        [SerializeField] private Sprite vibrationOnSprite;
        [SerializeField] private Sprite vibrationOffSprite;
        [SerializeField] private Sprite soundOnSprite;
        [SerializeField] private Sprite soundOffSprite;

        [SerializeField] GameObject musicIconX;

        private void Start()
        {
            rateUsButton.onClick.AddListener(OnRateUsButtonPresed);
            vibrationButton.onClick.AddListener(OnVibrationButtonPressed);
            soundButton.onClick.AddListener(OnSoundButtonPressed);
            musicButton.onClick.AddListener(OnMusicButtonPressed);

            CheckIcon();
        }
        public void OnVibrationButtonPressed()
        {
            SettingsController.Instance.TurnOnVibration();

            CheckIcon();
        }
        public void OnSoundButtonPressed()
        {
            SettingsController.Instance.TurnOnSound();

            CheckIcon();
        }
        public void OnMusicButtonPressed()
        {
            SettingsController.Instance.TurnOnMusic();

            CheckIcon();
        }
        public void OnRateUsButtonPresed()
        {
#if UNITY_ANDROID

            Application.OpenURL("https://play.google.com/store/apps/details?id=com.Moonee.Matchmania3d");

#elif UNITY_IOS
          
            Application.OpenURL("https://apps.apple.com/us/app/matchmania-3d/id1557428119");
#endif
        }
        private void CheckIcon()
        {
            if (SettingsController.isVibrationOn == true)
            {
                vibrationButton.GetComponent<Image>().sprite = vibrationOnSprite;
            }
            else vibrationButton.GetComponent<Image>().sprite = vibrationOffSprite;

            if (SettingsController.isSoundOn == true)
            {
                soundButton.GetComponent<Image>().sprite = soundOnSprite;
            }
            else soundButton.GetComponent<Image>().sprite = soundOffSprite;

            if (SettingsController.isMusicOn)
            {
                musicIconX.SetActive(false);
            }
            else musicIconX.SetActive(true);
        }
    }
}