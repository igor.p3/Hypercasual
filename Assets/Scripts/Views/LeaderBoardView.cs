using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Matchmania
{
    public class LeaderBoardView : MonoBehaviour
    {
        [SerializeField] private Sprite[] flags;

        [SerializeField] private Sprite goldenCup;
        [SerializeField] private Sprite silverCup;
        [SerializeField] private Sprite bronzeCup;

        [SerializeField] private LeaderboardPanel panelPrefab;
        [SerializeField] private Transform panelPrefabParent;
        [SerializeField] private GameObject loadingImage;


        public List<LeaderboardPanel> top20Users = new List<LeaderboardPanel>();

        [SerializeField] private Button worldSortButton;
        [SerializeField] private Button countrySortButton;
        [SerializeField] private Button leaderBoardsStartButton;
        [SerializeField] private Button closeButton;


        [SerializeField] private GameObject leaderboardScreen;
        [SerializeField] private GameObject mainMenuScreen;

        [SerializeField] FooterView footerView;


        private void Start()
        {
            flags = Resources.LoadAll<Sprite>("Flags");

            worldSortButton.onClick.AddListener(WorldSortButtonPressed);
            countrySortButton.onClick.AddListener(CountrySortButtonPressed);
            leaderBoardsStartButton.onClick.AddListener(WorldSortButtonPressed);
            closeButton.onClick.AddListener(CloseBUttonPressed);
            
            if (DatabaseModel.FirestoreDatabase == null || InternetCheckerController.isNoInternetConnection)
            {
                HideLeaderBoardMenuButton();
            }

            ChangeNicknameView.Instance.OnNicknameChanged += (s) =>
                StartCoroutine(StartLoadingView(LeaderboardSortingType.World));
            
            InternetCheckerController.OnNoInternet += HideLeaderBoardMenuButton;
        }
        private void CloseBUttonPressed()
        {
            footerView.HomeButtonPressed();
        }

        public void HideLeaderBoardMenuButton()
        {
            leaderBoardsStartButton.gameObject.SetActive(false);
        }

        public void WorldSortButtonPressed()
        {
            leaderboardScreen.SetActive(true);
            worldSortButton.interactable = false;
            countrySortButton.interactable = true;
            ChangeNicknameView.Instance.ShowNicknameChangeScreen();
            LeaderboardController.GetLeadersFromLeaderBoard(LeaderboardSortingType.World);
            StartCoroutine(StartLoadingView(LeaderboardSortingType.World));
        }
        private void CountrySortButtonPressed()
        {
            leaderboardScreen.SetActive(true);
            worldSortButton.interactable = true;
            countrySortButton.interactable = false;
            LeaderboardController.GetLeadersFromLeaderBoard(LeaderboardSortingType.Country);
            StartCoroutine(StartLoadingView(LeaderboardSortingType.Country));
        }
        IEnumerator StartLoadingView(LeaderboardSortingType leaderboardSortingType)
        {
            if (leaderboardSortingType == LeaderboardSortingType.World)
            {
                StartCoroutine(ShowLeaders(LeaderboardController.worldLeaders));
            }

            if (leaderboardSortingType == LeaderboardSortingType.Country)
            {
                StartCoroutine(ShowLeaders(LeaderboardController.countryLeaders));
            }

            // Show loading image
            if(!LeaderboardController.internalCountryCDTimer.IsDone) loadingImage.SetActive(true);

            yield break;
        }
        
        private IEnumerator ShowLeaders(List<PlayerDataModel> listOfLeaders)
        {
            yield return new WaitUntil(() => listOfLeaders.Count >= 1);

            ClearView();
            
            //yield return new WaitForSeconds(0.2f);
            int counter = 1;

            foreach (var player in listOfLeaders)
            {
                panelPrefab.transform.SetAsLastSibling();
                panelPrefab.gameObject.SetActive(true);
                bool isCurrentPlayer = player.playerName == GameController.playerName ||
                                       player.playerName.Equals(GameController.playerName);
                if (counter == 1)
                {
                    panelPrefab.SetView(goldenCup, GetFlag(player.country), player.playerName, (int) player.passedLevel,
                        counter, player.country, isCurrentPlayer);
                }
                else if (counter == 2)
                {
                    panelPrefab.SetView(silverCup, GetFlag(player.country), player.playerName, (int) player.passedLevel,
                        counter, player.country, isCurrentPlayer);
                }
                else if (counter == 3)
                {
                    panelPrefab.SetView(bronzeCup, GetFlag(player.country), player.playerName, (int) player.passedLevel,
                        counter, player.country, isCurrentPlayer);
                }
                else
                {
                    panelPrefab.SetView(null, GetFlag(player.country), player.playerName, (int) player.passedLevel, counter,
                        player.country, isCurrentPlayer);
                }
                
                var playerPanel = Instantiate(panelPrefab, panelPrefabParent, true);
                playerPanel.transform.localScale = Vector3.one;
                playerPanel.transform.localPosition = Vector3.zero;
                
                counter++;
                top20Users.Add(playerPanel);
            }

            // Hot fix
            //yield return new WaitForSeconds(0.1f);
            //top20Users[0].gameObject.SetActive(false);
            loadingImage.SetActive(false);
        }

        private void ClearView()
        {
            for (int i = 0; i < top20Users.Count; i++)
            {
                Destroy(top20Users[i].gameObject);
            }
            top20Users.Clear();
        }

        public Sprite GetFlag(string countryName)
        {
            Sprite sprite = null;

            foreach (var _sprite in flags)
            {
                if (_sprite.name == countryName)
                {
                    sprite = _sprite;
                    break;
                }
            }
            return sprite;
        }
    }
}