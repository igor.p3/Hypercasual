﻿using UnityEngine;
using HyperCasualTemplate;
using System.Collections;
using Firebase.Firestore;
using VoxelBusters.EssentialKit;
using System.Collections.Generic;

namespace Matchmania
{
    public class GameController : MonoBehaviour
    {
        public static  List<ItemController> items { get; private set; }
        public static List<ItemController> unlockedItems { get; private set; } = new List<ItemController>();
        public static List<ItemController> lastUnlockedItems { get; private set; } = new List<ItemController>();
        public static SaveDataModel SavedData { get; private set; }

        public static bool isFirstSession = true;

        public static bool isTookFirstDailyReward = false;

        public static string playerName;
        public static int passedLevels;


        void Awake()
        {
            SavedData = LoadGameData();
            items = new List<ItemController>();

            LoadItems("Items");
            //LoadItems("Items_02");
            //LoadItems("Items_03");
            //LoadItems("Items_04");
        }

        private void LoadItems(string itemsFolder)
        {
           var itemssLsit = Resources.LoadAll<ItemController>(itemsFolder);

            foreach (var item in itemssLsit)
            {
                items.Add(item);
            }
        }
        private void Start()
        {
            Application.targetFrameRate = 300;

            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
        public static void SaveGameData()
        {
            SaveDataModel saveGameData = new SaveDataModel();

            saveGameData.starsCount = CurrencyController.Stars;
            saveGameData.coinsCount = CurrencyController.Coins;
            // Hearts
            saveGameData.heartsCount = CurrencyController.Hearts;
            saveGameData.isUnlimitedHearts = HeartsController.isUnlimited;
            saveGameData.unlimTimeHearts = HeartsController.timeForUnlim;
            saveGameData.heartsDateTime = HeartsController.HeartsStartTime.ToString();
            // TT events
            saveGameData.ttCurrentEvent = TTEventController.currentEvent;
            saveGameData.ttTokensCollected = CurrencyController.TTTokens;
            saveGameData.ttCurrentTier = TTEventController.CurrentTier;
            saveGameData.ttEventDateTime = TTEventController.TtStartTime.ToString();
            // Tournaments
            saveGameData.savedTournamentId = TournamentsController.savedTournamentId;
            saveGameData.savedTakenPlaceTournament = TournamentsController.savedTakenPlace;
            saveGameData.savedTourProgression = TournamentsController.savedTournamentProgression;
            saveGameData.savedJoinedRoomReference = TournamentsController.savedJoinedRoomReference.ToString();
            
            saveGameData.hintsCount = CurrencyController.Hints;
            saveGameData.fansCount = CurrencyController.Fans;
            saveGameData.starsChestCount = CurrencyController.StarsChestCount;
            saveGameData.levelsChestCount = CurrencyController.LevelsChestCount;
            saveGameData.levelIndex = LevelController.CurrentLevelIndex;
            saveGameData.isFirstSession = isFirstSession;
            // Special offer
            saveGameData.savedCurrentOfferType = SpecialOfferController.SavedCurrentOfferType;
            saveGameData.savedOfferStartTime = SpecialOfferController.OfferStartTime.ToString();
            saveGameData.isSpecialOfferHidden = SpecialOfferController.IsOfferHidden;
            // Region related
            saveGameData.countryDisplayName = RegionIdentifier.savedCountryName;
            saveGameData.isRegionFetchedByIp = RegionIdentifier.isRegionFetchedByIP;
            
            saveGameData.isNicknameChanged = ChangeNicknameView.IsNicknameChanged;
            saveGameData.isCanShowRateUs = RateUsView.isCanShowRateUs;
            saveGameData.isMusicOn =  SettingsController.isMusicOn;
            saveGameData.isVibroOn =  SettingsController.isVibrationOn;
            saveGameData.isSoundOn = SettingsController.isSoundOn;
            saveGameData.stringLastRewardDate = DailyBonusController.stringLastRewardDate;
            saveGameData.playerName = playerName;
            saveGameData.passedLevels = passedLevels;
            saveGameData.isStarterBundlePurchased = IAPController.isStarterBundlePurchased;
            saveGameData.isTookFirstDailyReward = isTookFirstDailyReward;
            saveGameData.isAdsRemoved = AdvertisementSystem.isAdsRemoved;

            LevelProgressDataModel levelProgress = new LevelProgressDataModel();

            levelProgress.timer = LevelController.Timer;

            foreach (var item in LevelController.Instance.CurrentLevelItems)
            {
                ItemDataModel itemData = new ItemDataModel();

                if (item != null)
                {
                    itemData.positionX = item.transform.position.x;
                    itemData.positionY = item.transform.position.y;
                    itemData.positionZ = item.transform.position.z;
                    itemData.name = item.name;
                }
                for (int i = 0; i < LevelController.Instance.slots.Length; i++)
                {
                    if (item == LevelController.Instance.slots[i].ItemController)
                    {
                        itemData.slotIndex = i;
                        break;
                    }
                }
                levelProgress.itemsData.Add(itemData);
            }
            levelProgress.timer = LevelController.Timer;
            levelProgress.levelsStars = LevelController.Instance.CurrentLevelStars;

            saveGameData.LevelProgressData = levelProgress;

            if (LevelController.Instance.IsPlayingLevel == false)
            {
                saveGameData.LevelProgressData.itemsData.Clear();
            }

            SavingSystem<SaveDataModel>.SaveJsonGameData(saveGameData);

            if (GameServices.IsAuthenticated)
            {
                CloudSavingController.SaveDataToCloud(saveGameData);
            }
        }

        public static void OnDataChangedLoad()
        {
            SavedData = LoadGameData();
            
        }
        public static SaveDataModel LoadGameData(SaveDataModel optionalDataModel = null)
        {
            var saveGameData = optionalDataModel ?? SavingSystem<SaveDataModel>.LoadJsonGameData();

            // else
            // {
            //     saveGameData = CloudSavingController.LoadData() ?? SavingSystem<SaveDataModel>.LoadJsonGameData();
            // }

            if (saveGameData != null)
            {
                CurrencyController.Stars = saveGameData.starsCount;
                CurrencyController.Coins = saveGameData.coinsCount;
                CurrencyController.Hints = saveGameData.hintsCount;
                CurrencyController.Fans = saveGameData.fansCount;
                // TT events
                CurrencyController.TTTokens = saveGameData.ttTokensCollected;
                TTEventController.currentEvent = saveGameData.ttCurrentEvent;
                TTEventController.TtEventSavedStartTime = saveGameData.ttEventDateTime;
                TTEventController.CurrentTier = saveGameData.ttCurrentTier;
                // Tournaments
                TournamentsController.savedTournamentId = saveGameData.savedTournamentId;
                TournamentsController.savedTakenPlace = saveGameData.savedTakenPlaceTournament;
                TournamentsController.savedTournamentProgression = saveGameData.savedTourProgression;
                TournamentsController.savedJoinedRoomReference = saveGameData.savedJoinedRoomReference;
                // Hearts
                HeartsController.isUnlimited = saveGameData.isUnlimitedHearts;
                HeartsController.timeForUnlim = saveGameData.unlimTimeHearts;
                HeartsController.HeartsSavedStartTime = saveGameData.heartsDateTime;
                CurrencyController.Hearts = saveGameData.heartsCount;

                CurrencyController.LevelsChestCount = saveGameData.levelsChestCount;
                CurrencyController.StarsChestCount = saveGameData.starsChestCount;
                LevelController.CurrentLevelIndex = saveGameData.levelIndex;
                LevelController.Timer = saveGameData.LevelProgressData.timer;
                // Settings
                SettingsController.isMusicOn = saveGameData.isMusicOn;
                SettingsController.isSoundOn = saveGameData.isSoundOn;
                SettingsController.isVibrationOn = saveGameData.isVibroOn;
                isFirstSession = saveGameData.isFirstSession;
                // Special offer
                SpecialOfferController.SavedCurrentOfferType = saveGameData.savedCurrentOfferType;
                SpecialOfferController.SavedOfferStartTime = saveGameData.savedOfferStartTime;
                SpecialOfferController.IsOfferHidden = saveGameData.isSpecialOfferHidden;
                // Region related
                RegionIdentifier.isRegionFetchedByIP = saveGameData.isRegionFetchedByIp;
                RegionIdentifier.savedCountryName = saveGameData.countryDisplayName;
                
                ChangeNicknameView.IsNicknameChanged = saveGameData.isNicknameChanged;
                RateUsView.isCanShowRateUs = saveGameData.isCanShowRateUs;
                AdvertisementSystem.isAdsRemoved = saveGameData.isAdsRemoved;
                DailyBonusController.stringLastRewardDate = saveGameData.stringLastRewardDate;
                playerName = saveGameData.playerName;
                passedLevels = saveGameData.passedLevels;
                IAPController.isStarterBundlePurchased = saveGameData.isStarterBundlePurchased;
                isTookFirstDailyReward = saveGameData.isTookFirstDailyReward;

            }
            return saveGameData;
        }
    }
}