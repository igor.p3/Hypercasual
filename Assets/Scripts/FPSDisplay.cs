using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FPSDisplay : MonoBehaviour
{
    private float fps;
    public TextMeshProUGUI text;
    void Start()
    {
        InvokeRepeating("FPS", 0.5f, 0.5f);
    }

    // Update is called once per frame
    void FPS()
    {
        fps = (int)(1f / Time.unscaledDeltaTime);
        text.text = "FPS: " +fps.ToString();
    }
}
