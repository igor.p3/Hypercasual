using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Extensions;
using Firebase.Firestore;
using GameAnalyticsSDK.Setup;
using Matchmania;
using UnityEngine;
using Random = UnityEngine.Random;

public class TournamentRoomHandler
{
    private string roomIDBase = "room_";
    private string playerAmountField = "playersAmount";
    private string roomClosedField = "isClosed";

    private static DocumentReference joinedRoomReference;
    private CollectionReference roomsCollection;

    public TournamentRoomHandler(CollectionReference roomsCollection, string savedJoinedRoomReference = null)
    {
        this.roomsCollection = roomsCollection;

        if (savedJoinedRoomReference == "")
            return;
        joinedRoomReference = DatabaseModel.FirestoreDatabase.Document(savedJoinedRoomReference);
    }

    public static DocumentReference JoinedRoomReference
    {
        get => joinedRoomReference;
        set
        {
            joinedRoomReference = value;
            TournamentsController.savedJoinedRoomReference = value.Path;
            GameController.SaveGameData();
        }
    }

    public void GetCurrentRoomLeaderboard(TournamentType tournamentType)
    {
        if (joinedRoomReference == null)
        {
            Debug.LogError("There is no room collection reference");
            return;
        }

        // Check if current room is closed, if it is, join different room
        joinedRoomReference.GetSnapshotAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError(task.Exception);
            }
            else if (task.IsCompleted)
            {
                var documentSnapshot = task.Result;
                try
                {
                    bool isClosedValue = documentSnapshot.GetValue<bool>(roomClosedField);
                    if (isClosedValue)
                    {
                        JoinFreeRoom(new TourLeaderboardPlayer());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
        });

        switch (tournamentType)
        {
            case TournamentType.Level:
                JoinedRoomReference.Collection("roomPlayers").OrderByDescending("passedLevel")
                    .Limit(100)
                    .GetSnapshotAsync()
                    .ContinueWith(TournamentsController.HandleData);
                break;
            case TournamentType.Star:
                JoinedRoomReference.Collection("roomPlayers").OrderByDescending("collectedStars")
                    .Limit(100)
                    .GetSnapshotAsync()
                    .ContinueWith(TournamentsController.HandleData);
                break;
        }
    }

    public void JoinFreeRoom(TourLeaderboardPlayer currentPlayerInfo)
    {
        if (GameController.playerName == "" || string.IsNullOrEmpty(GameController.playerName))
        {
            Debug.LogError("The player's name is empty, can't add him to DB");
            return;
        }

        roomsCollection.WhereLessThan(playerAmountField, 100).WhereEqualTo(roomClosedField, false)
            .Limit(1)
            .GetSnapshotAsync()
            .ContinueWithOnMainThread(task =>
            {
                var playerName = GameController.playerName;
                if (task.IsFaulted)
                {
                    Debug.LogError(task.Exception);
                }
                else if (task.IsCompleted)
                {
                    if (task.Result.Count == 0)
                    {
                        // No such a room - create a new empty room
                        CreateNewRoomAndJoin(currentPlayerInfo);
                        return;
                    }

                    var documentSnapshot = task.Result[0];

                    // Assigned joined room to a joined room reference
                    JoinedRoomReference = documentSnapshot.Reference;

                    // Increment the amount of players in the room field
                    var currentAmountOfPlayers = documentSnapshot.GetValue<int>(playerAmountField);
                    JoinedRoomReference.UpdateAsync(playerAmountField, currentAmountOfPlayers + 1);

                    JoinedRoomReference.Collection("roomPlayers").Document(playerName).SetAsync(currentPlayerInfo);
                    Debug.LogWarning($"Successfully joined a new room with id {JoinedRoomReference.Id}");
                }
            });


    }

    public void CloseCurrentRoomJoinNewFree()
    {
        if (joinedRoomReference == null)
        {
            Debug.LogError("There is no room collection reference");
            return;
        }

        // Closes current room and joins new room
        joinedRoomReference.UpdateAsync(roomClosedField, true)
            .ContinueWith((task) => JoinFreeRoom(new TourLeaderboardPlayer()));
    }

    public void RejoinPlayerWithNewNickname(string oldPlayerName)
    {
        if (joinedRoomReference == null)
        {
            Debug.LogError("There is no room collection reference");
            return;
        }
        
        if (GameController.playerName == "" || string.IsNullOrEmpty(GameController.playerName))
        {
            Debug.LogError("The player's name is empty, can't add him to DB");
            return;
        }

        var newPlayerName = GameController.playerName;
        
        // Deletes the player before from DB and adds a new player
        joinedRoomReference.Collection("roomPlayers").Document(oldPlayerName).DeleteAsync().ContinueWith(task =>
        {
            joinedRoomReference.Collection("roomPlayers").Document(newPlayerName).SetAsync(new TourLeaderboardPlayer()).ContinueWithOnMainThread(
                task1 =>
                {
                    Debug.LogWarning($"The player's new name {newPlayerName} has been updated in the database. The old name {oldPlayerName} deleted.");
                });
        });
    }

    private void CreateNewRoomAndJoin(TourLeaderboardPlayer currentPlayerInfo)
    {
        var newRoomId = GenerateRoomID();
        roomsCollection.Document(newRoomId).Collection("roomPlayers").Document(GameController.playerName)
            .SetAsync(currentPlayerInfo).ContinueWith(
                task =>
                {
                    roomsCollection.Document(newRoomId).SetAsync( new
                    {
                        isClosed = false,
                        playersAmount = 1
                    });

                    Debug.LogWarning("New room was created and the player joined it");
                });

        JoinedRoomReference = roomsCollection.Document(newRoomId);
    }

    public void UpdatePlayerInfoInCurrentRoom(TourLeaderboardPlayer currentPlayerInfo)
    {
        if (joinedRoomReference == null)
        {
            Debug.LogError("There is no room collection reference");
            return;
        }
        if (GameController.playerName == "" || string.IsNullOrEmpty(GameController.playerName))
        {
            Debug.LogError("The player's name is empty, can't add him to DB");
            return;
        }
        JoinedRoomReference.Collection("roomPlayers").Document(GameController.playerName).SetAsync(currentPlayerInfo)
            .ContinueWith(
                task =>
                {
                    Debug.LogWarning($"Player's info was updated in the room with id {JoinedRoomReference.Id}");
                });
    }
    public string GenerateRoomID()
    {
        return
            $"{roomIDBase}{(char) Random.Range('a', 'z')}{(char) UnityEngine.Random.Range('A', 'Z')}" +
            $"{Random.Range(0, 9)}{Random.Range(0, 9)}{Random.Range(0, 9)}{Random.Range('a', 'z')}";
    }
}
