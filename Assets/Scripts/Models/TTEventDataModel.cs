using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TTEventDataModel : MonoBehaviour
{
    public enum LootType
    {
        Heart,
        Hint,
        Fan,
        Coins
    }
    
    public struct Loot
    {
        public int amountOfLoot;
        public LootType lootType;

        public Loot(int amountOfLoot, LootType lootType)
        {
            this.amountOfLoot = amountOfLoot;
            this.lootType = lootType;
        }
    }
    public Dictionary<int, Loot> LootTable = new Dictionary<int, Loot>();
    public enum TTEventName
    {
        NeverStarted,
        ZooRama,
        PostZooRamaPause,
        SummerBreeze,
        PostSummerBreezePause,
        GodsEscape,
        PostGodsEscapePause,
    }
    
    public Dictionary<int, int> stepsToMake = new Dictionary<int, int>()
    {
        {0, 5},
        {1, 10},
        {2, 20},
        {3, 30},
        {4, 50},
        {5, 100},
        {6, 100},

    };
    private void Awake()
    {
        LootTable[0] = new Loot(100, LootType.Coins);
        LootTable[1] = new Loot(2, LootType.Hint);
        LootTable[2] = new Loot(250, LootType.Coins);
        LootTable[3] = new Loot(5, LootType.Hint);
        LootTable[4] = new Loot(500, LootType.Coins);
        LootTable[5] = new Loot(10, LootType.Hint);
        LootTable[6] = new Loot(1000, LootType.Coins);
    }
    
    
}
