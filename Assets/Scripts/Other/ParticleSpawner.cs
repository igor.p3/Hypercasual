using System;
using System.Collections;
using Matchmania;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    [SerializeField] private RectTransform heartsWalletTransform;
    [SerializeField] private RectTransform coinsWalletTransform;
    [SerializeField] private RectTransform tokensWalletTransform;
    [SerializeField] private RectTransform middleOfScreenTransform;

    [SerializeField] private GameObject genericVFX;
    [SerializeField] private GameObject canvasToSpawnOn;
    [SerializeField] private MainMenuView mainMenuView;
    [SerializeField] private TTEventView ttEventView;

    #region Singleton

    private static ParticleSpawner _instance;

    public static ParticleSpawner Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ParticleSpawner>();
            }

            return _instance;
        }
    }

    #endregion

    public enum ParticleObjects
    {
        ZooRamaToken,
        SummerBreezeToken,
        GodsEscapeToken,
        Coins,
        Hearts,
        Hints,
    }

    public void SpawnFromMiddleOfScreen(ParticleObjects particleObject, int amount)
    {
        SpawnRewardVFX(particleObject, amount, middleOfScreenTransform.position);
    }
    public void SpawnRewardVFX(ParticleObjects particleObject, int amount, Vector3 spawnPosition)
    {
        switch (particleObject)
        {
            case ParticleObjects.GodsEscapeToken:
            {
                StartCoroutine(SpawnAndFlyLine(amount, spawnPosition, tokensWalletTransform, ttEventView.godsEscapeIcon, 0.5f));
                return;
            }
            case ParticleObjects.SummerBreezeToken:
            {
                StartCoroutine(SpawnAndFlyLine(amount, spawnPosition, tokensWalletTransform, ttEventView.summerBreezeIcon, 0.5f));
                return;
            }
            case ParticleObjects.ZooRamaToken:
            {
                StartCoroutine(SpawnAndFlyLine(amount, spawnPosition, tokensWalletTransform, ttEventView.zooRamaIcon, 0.5f));
                return;
            }
            case ParticleObjects.Hearts:
            {
                SpawnAndFly(amount, spawnPosition, heartsWalletTransform, mainMenuView.unlimHeartsSprite);
                return;
            }
            case ParticleObjects.Coins:
            {
                StartCoroutine(SpawnAndFlyLine(amount, spawnPosition, coinsWalletTransform, mainMenuView.coinsSprite, 0.1f));
                return;
            }
            case ParticleObjects.Hints:
            {
                StartCoroutine(SpawnAndFlyLine(amount, spawnPosition, coinsWalletTransform, mainMenuView.hintSprite, 0.5f));
                return;
            }
        }
    }

    private void SpawnAndFly(int amount, Vector3 spawnPosition, RectTransform target, Sprite spriteForParticle)
    {
        var actualAmount = amount >= 50 ? 50 : amount;
        for (int i = 0; i < actualAmount; i++)
        {
            var particleToSpawn =
                Instantiate(genericVFX, spawnPosition, Quaternion.identity, canvasToSpawnOn.transform);
            var movingParticleEffect = particleToSpawn.GetComponent<MovingParticleEffect>();
            movingParticleEffect.ChangeSprite(spriteForParticle);
            movingParticleEffect.SetTarget(target);
            movingParticleEffect.StartAnimation();
        }
    }

    public IEnumerator SpawnAndFlyLine(int amount, Vector3 spawnPosition, RectTransform target, Sprite spriteForParticle, float timeAmount)
    {
        if (amount > 50)
            amount = 50;
        for (int i = 0; i < amount; i++)
        {
            var particleToSpawn =
                Instantiate(genericVFX, spawnPosition, Quaternion.identity, canvasToSpawnOn.transform);
            var movingParticleEffect = particleToSpawn.GetComponent<MovingParticleEffect>();
            movingParticleEffect.ChangeSprite(spriteForParticle);
            movingParticleEffect.SetTarget(target);
            movingParticleEffect.StartAnimation();
            float amountToWait = timeAmount - (i * 0.1f);
            yield return new WaitForSeconds(Mathf.Clamp(amountToWait, 0.05f, 1f));
        }
    }
}