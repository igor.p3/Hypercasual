using System;
using System.Collections;
using System.Collections.Generic;
using HyperCasualTemplate;
using Matchmania;
using TMPro;
using UnityEngine;

public class HeartsController : MonoBehaviour
{
    #region Singleton
    private static HeartsController _instance;
    public static HeartsController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<HeartsController>();
            }

            return _instance;
        }
    }
    #endregion
    
    [Header("Timer related")]
    private Timer heartsTimer;
    public static string HeartsSavedStartTime;
    public static DateTime HeartsStartTime;
    [SerializeField] private float timeForRegain;
    
    // Unlimited hearts
    public static bool isUnlimited = false;
    public static int timeForUnlim = 0;
    private Timer heartsUnlimTimer;
    private float timeForUnlimOff;    

    private int _currentHeartAmount;
    public int CurrentHeartsAmount 
    {
        get => _currentHeartAmount;
        set
        {
            if (isUnlimited) return;
            _currentHeartAmount = Mathf.Clamp(value, 0, maxHeartsAmount);
            CurrencyController.Hearts = _currentHeartAmount;
            
            if (value < maxHeartsAmount && value >= 0)
            {
                // also saves the data
                StartTimer();
            }


            heartsView.UpdateHeartsAmounts(_currentHeartAmount);
        }
        
    }

    [field: SerializeField] public int maxHeartsAmount { get; } = 5;
    public int heartsRegainCost = 125;

    [SerializeField] private HeartsView heartsView;

    private void Start()
    {
        _currentHeartAmount = CurrencyController.Hearts;
        heartsView.UpdateHeartsAmounts(_currentHeartAmount);
        if (CurrentHeartsAmount != maxHeartsAmount)
        {
            RealTimerCheck();
        }

        if (isUnlimited)
        {
            heartsView.ShowUnlimitedIcon();
            RealTimeUnlimCheck();
        }

    }

    private void RealTimeUnlimCheck(string optionalHeartsStartTime = default)
    {
        if (optionalHeartsStartTime == default)
            DateTime.TryParse(HeartsSavedStartTime, out HeartsStartTime);
        else
            DateTime.TryParse(optionalHeartsStartTime, out HeartsStartTime);
        
        RealTimeTimer _realTimer = new RealTimeTimer(HeartsStartTime, timeForUnlim);
        if (_realTimer.IsDone)
        {
            SwitchToLimited();
        }
        else
        {
            var timeForTimerDouble = timeForUnlim - _realTimer.Elapsed;
            var timeForTimer = (int) timeForTimerDouble;
            StartTimer(timeForTimer);
        }

    }

    private void OnApplicationFocus(bool pauseStatus)
    {
        if (CurrentHeartsAmount != maxHeartsAmount)
        {
            RealTimerCheck(HeartsStartTime.ToString());
        }

        if (isUnlimited)
        {
            RealTimeUnlimCheck(HeartsStartTime.ToString());
        }
    }

    private void RealTimerCheck(string optionalHeartsStartTime = default)
    {
        // Checking how much time has elapsed since closing the game and how many hearts can be gained
        if (optionalHeartsStartTime == default)
            DateTime.TryParse(HeartsSavedStartTime, out HeartsStartTime);
        else
            DateTime.TryParse(optionalHeartsStartTime, out HeartsStartTime);
        
        var timeLeftForAllHeartsToBeBack = (maxHeartsAmount - CurrentHeartsAmount) * timeForRegain;
        RealTimeTimer _realTimer = new RealTimeTimer(HeartsStartTime, timeLeftForAllHeartsToBeBack);
        
        
        if (_realTimer.IsDone)
        {
            CurrentHeartsAmount = maxHeartsAmount;
            GameController.SaveGameData();
        }
        else
        {
            int heartsToAdd = (int) (_realTimer.Elapsed / timeForRegain);
            if(heartsToAdd != 0)
                CurrentHeartsAmount += heartsToAdd;
            var timeForTimer = (timeLeftForAllHeartsToBeBack - _realTimer.Elapsed) % timeForRegain;
            StartTimer(timeForTimer);
        }
    }

    private void StartTimer()
    {
        heartsTimer = new Timer(timeForRegain);
        HeartsStartTime = DateTime.Now;
        GameController.SaveGameData();
    }
    
    private void StartTimer(float timeForTimer)
    {
        heartsTimer = new Timer(timeForTimer);
    }

    public bool HeartsCheck()
    {
        if (CurrentHeartsAmount != 0) return true;
        
        heartsView.ShowOutOfHearts();
        return false;
    }

    public bool AddHeartsForCoins()
    {
        if (CurrencyController.Coins < heartsRegainCost)
        {
            LevelView.Instance.OpenShop();
            return false;
        }

        CurrencyController.Coins -= heartsRegainCost;
        LevelView.Instance.mainMenuView.UpdateMainMenuView();
        CurrentHeartsAmount = maxHeartsAmount;
        return true;
    }

    public void SwitchToUnlimited(int time)
    {
        if (isUnlimited)
        {
            timeForUnlim += time;
            StartTimer(timeForUnlim);
            GameController.SaveGameData();
            return;
        }
        CurrentHeartsAmount = 5;
        heartsView.ShowUnlimitedIcon();
        timeForUnlim = time;
        isUnlimited = true;
        StartTimer(timeForUnlim);
        GameController.SaveGameData();
    }

    public void SwitchToLimited()
    {
        isUnlimited = false;
        CurrentHeartsAmount = 5;
        heartsView.HideUnlimitedIcon(CurrentHeartsAmount);
        timeForUnlim = 0;
    }
    private void FixedUpdate()
    {
        TimeSpan timeSpan;
        if (isUnlimited)
        {
            if (heartsTimer.IsDone)
            {
                SwitchToLimited();
            }

            timeSpan = TimeSpan.FromSeconds(heartsTimer.TimeLeft);
            if (timeSpan.TotalMinutes <= 60)
            {
                heartsView.UpdateTimersText($"{timeSpan.ToString(@"mm\:ss")}");
                return;

            }
            heartsView.UpdateTimersText($"{timeSpan.ToString(@"hh\:mm\:ss")}");
            return;
        }
        if (CurrentHeartsAmount == maxHeartsAmount)
        {
            heartsView.UpdateTimersText("Full");
            return;
        }

        if (heartsTimer.IsDone)
        {
            CurrentHeartsAmount++;
            StartTimer();
        }
        timeSpan = TimeSpan.FromSeconds(heartsTimer.TimeLeft);
        if (timeSpan.TotalMinutes <= 60)
        {
            heartsView.UpdateTimersText($"{timeSpan.ToString(@"mm\:ss")}");
            return;

        }
        heartsView.UpdateTimersText($"{timeSpan.ToString(@"hh\:mm\:ss")}");
    }
    
    
}
