using System.Collections.Generic;

[System.Serializable]
public class ItemsProgressionModel
{
    public int levelComplete;

    public string item_01;
    public string item_02;
    public string item_03;
    public string item_04;
    public string item_05;
    public string item_06;
    public string item_07;
    public string item_08;
    public string item_09;
    public string item_10;
    public string item_11;
    public string item_12;
    public string item_13;
    public string item_14;
    public string item_15;
    public string item_16;
    public string item_17;
    public string item_18;
    public string item_19;
    public string item_20;
    public string item_21;
    public string item_22;
    public string item_23;
    public string item_24;
    public string item_25;
    public string item_26;
    public string item_27;
    public string item_28;
    public string item_29;
    public string item_30;
    public string item_31;
    public string item_32;
    public string item_33;
    public string item_34;
    public string item_35;
    public string item_36;
    public string item_37;
    public string item_38;
    public string item_39;
    public string item_40;
    public string item_41;

    public List<string> progressionItems = new List<string>();

    public void CheckItems()
    {
        CheckItemID(item_01);
        CheckItemID(item_02);
        CheckItemID(item_03);
        CheckItemID(item_04);
        CheckItemID(item_05);
        CheckItemID(item_06);
        CheckItemID(item_07);
        CheckItemID(item_08);
        CheckItemID(item_09);
        CheckItemID(item_10);
        CheckItemID(item_11);
        CheckItemID(item_12);
        CheckItemID(item_13);
        CheckItemID(item_14);
        CheckItemID(item_15);
        CheckItemID(item_16);
        CheckItemID(item_17);
        CheckItemID(item_18);
        CheckItemID(item_19);
        CheckItemID(item_20);
        CheckItemID(item_21);
        CheckItemID(item_22);
        CheckItemID(item_23);
        CheckItemID(item_24);
        CheckItemID(item_25);
        CheckItemID(item_26);
        CheckItemID(item_27);
        CheckItemID(item_28);
        CheckItemID(item_29);
        CheckItemID(item_30);
        CheckItemID(item_31);
        CheckItemID(item_32);
        CheckItemID(item_33);
        CheckItemID(item_34);
        CheckItemID(item_35);
        CheckItemID(item_36);
        CheckItemID(item_37);
        CheckItemID(item_38);
        CheckItemID(item_39);
        CheckItemID(item_40);
        CheckItemID(item_41);
    }
    private void CheckItemID(string id)
    {
        if (id != "" && id != null)
        {
            if (!progressionItems.Contains(id))
            {
                progressionItems.Add(id);
            }
        }
    }
}
