﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using System.Linq;
using Lean.Pool;


namespace Matchmania
{
    public class ItemsSpawnController : MonoBehaviour
    {
        #region Singleton
        private static ItemsSpawnController _instance;
        public static ItemsSpawnController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<ItemsSpawnController>();
                }

                return _instance;
            }
        }
        #endregion

        private float movementDuration = 1f;

        [SerializeField] private Transform tubeStartPosition;
        [SerializeField] private Transform tubeLevelPosition;
        [SerializeField] private Animator tubeAnimator;
        [SerializeField] private LevelView levelView;

        private bool isSpawningItems;

        public void SpawnItems(Transform parent, bool isTutorial, Transform[] tutorialLevelPos)
        {
            if (isSpawningItems == false)
            {
                StartCoroutine(_SpawnItems(parent, isTutorial, tutorialLevelPos));
            }
        }
        public IEnumerator _SpawnItems(Transform parent, bool isTutorial, Transform[] tutorialLevelPos)
        {
            isSpawningItems = true;

            SceneLoadingController.UpdateUnlockedItems();

            LevelController.Instance.IsCanMakeTurn = false;

            List<ItemController> levelItems = new List<ItemController>();
            List<ItemController> lastUnlockedItems = new List<ItemController>();

            if (isTutorial == true)
            {

                List<ItemController> tutoraiItems = new List<ItemController>();

                levelItems = Extensions.GetRandomElements(GameController.unlockedItems, 3);

                for (int i = 0; i < levelItems.Count; i++)
                {
                    for (int j = 0; j < GameModel.itemMatchCount; j++)
                    {
                        tutoraiItems.Add(levelItems[i]);
                    }
                }
                for (int i = 0; i < tutoraiItems.Count; i++)
                {
                    ItemController item = LeanPool.Spawn(tutoraiItems[i], levelView.itemStartPosition.position, Quaternion.identity, parent);

                    item.transform.position = tutorialLevelPos[i].position;
                    LevelController.Instance.CurrentLevelItems.Add(item);

                    item.isRotate = false;
                    item.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    item.GetComponent<Rigidbody>().isKinematic = false;
                    item.isMovingToBar = false;

                    item.transform.rotation = Quaternion.Euler(item.slotRotation);
                }
                LevelController.Instance.IsCanMakeTurn = true;
                LevelController.Instance.IsPlayingLevel = true;
                isSpawningItems = false;
                yield break;
            }

            tubeAnimator.transform.DOMove(tubeLevelPosition.position, movementDuration);

            yield return new WaitForSeconds(movementDuration);
            LevelController.Instance.IsPlayingLevel = false;
            tubeAnimator.SetTrigger("ChangeAnimation");

            //Get the list of current level items
            List<ItemController> itemsToSpawn = new List<ItemController>();


            int unlockedItemsToSpawnCount = GameController.lastUnlockedItems.Count / 2;

            if(LevelController.CurrentLevelIndex <= 4)
            {
                levelItems = Extensions.GetRandomElements(GameController.unlockedItems, LevelController.Instance.CurrentLevelModel.itemsCount);
            }
            else
            {
                levelItems = Extensions.GetRandomElements(GameController.unlockedItems, LevelController.Instance.CurrentLevelModel.itemsCount - unlockedItemsToSpawnCount);
                lastUnlockedItems = Extensions.GetRandomElements(GameController.lastUnlockedItems, unlockedItemsToSpawnCount);
            }

            for (int i = 0; i < levelItems.Count; i++)
            {
                for (int j = 0; j < GameModel.itemMatchCount; j++)
                {
                    itemsToSpawn.Add(levelItems[i]);
                }
            }
            for (int i = 0; i < lastUnlockedItems.Count; i++)
            {
                for (int j = 0; j < GameModel.itemMatchCount; j++)
                {
                    itemsToSpawn.Add(lastUnlockedItems[i]);
                }
            }

            // Mix items in list
            System.Random RND = new System.Random();
            for (int i = 0; i < itemsToSpawn.Count; i++)
            {
                var tmp = itemsToSpawn[0];
                itemsToSpawn.RemoveAt(0);
                itemsToSpawn.Insert(RND.Next(itemsToSpawn.Count), tmp);
            }

            //Spawn items
            for (int i = 0; i < itemsToSpawn.Count; i++)
            {
                Vector3 randomRotation = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));

                Quaternion rotation = Quaternion.Euler(randomRotation);
                var itemByName = itemsToSpawn[i];
                if (itemByName == null) continue;
                ItemController item = LeanPool.Spawn(itemByName, levelView.itemStartPosition.position, rotation, parent);
                LevelController.Instance.CurrentLevelItems.Add(item);

                item.isRotate = false;
                item.rigidbody.velocity = new Vector3(0, -15, 0);
               
                item.collider.enabled = true;

                item.rigidbody.isKinematic = false;
                item.isMovingToBar = false;
                item.outline.enabled = false;
                item.transform.DOScale(item.normalScale, 0.5f);
                if (SettingsController.isVibrationOn == true)
                    MMVibrationManager.Haptic(HapticTypes.LightImpact, false, true, this);
                yield return new WaitForSeconds(0.03f);
            }

            tubeAnimator.SetTrigger("ChangeAnimation");
            tubeAnimator.transform.DOMove(tubeStartPosition.position, movementDuration);

            yield return new WaitForSeconds(1);
            LevelController.Instance.IsPlayingLevel = true;
            LevelController.Instance.IsCanMakeTurn = true;
            isSpawningItems = false;
        }
        public void DespawnItems()
        {
            for (int i = 0; i < LevelController.Instance.CurrentLevelItems.Count; i++)
            {
                LeanPool.Despawn(LevelController.Instance.CurrentLevelItems[i].gameObject);
            }
        }
    }
}