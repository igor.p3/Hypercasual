﻿using Firebase.Firestore;

namespace Matchmania
{
    public class SaveDataModel 
    {
        public int starsCount;
        public int coinsCount;
        public int heartsCount;
        public bool isUnlimitedHearts;
        public int unlimTimeHearts;
        public TTEventDataModel.TTEventName ttCurrentEvent;
        public int ttCurrentTier;
        public int ttTokensCollected;
        public string savedTournamentId;
        public int savedTakenPlaceTournament;
        public TourLeaderboardPlayer savedTourProgression;
        public string? savedJoinedRoomReference;
        public bool isNicknameChanged;
        public int hintsCount;
        public int fansCount;
        public int levelIndex;
        public int starsChestCount;
        public int levelsChestCount;
        public bool isFirstSession = true;
        public SpecialOfferType savedCurrentOfferType;
        public string savedOfferStartTime;
        public bool isSpecialOfferHidden;
        public string heartsDateTime;
        public string ttEventDateTime;
        public bool isCanShowRateUs = true;
        public bool isSoundOn = true;
        public bool isMusicOn = true;
        public bool isVibroOn = true;
        public bool isAdsRemoved;
        public string stringLastRewardDate;
        public string playerName;
        public int passedLevels;
        public bool isStarterBundlePurchased;
        public bool isTookFirstDailyReward;
        public string countryDisplayName;
        public bool isRegionFetchedByIp { get; set; }


        public LevelProgressDataModel LevelProgressData;
    }
}