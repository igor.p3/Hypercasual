﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using MoreMountains.NiceVibrations;

namespace Matchmania
{
    public class ItemController : MonoBehaviour
    {
        public float normalScale = 1.4f;
        public float selectedScale = 1.6f;
        public float slotScale = 1f;

        public Vector3 slotRotation;
        const  float movementDuration = 0.2f;
        public bool isRotate { get; set; }

        new public Collider collider;
        new public Rigidbody rigidbody;
        public Outline outline;

        private float posMinX = -4;
        private float posMaxX = 4;
        private float posMinY = 0.7f;
        private float posMaxY = 10;
        private float posMinZ = -4;
        private float posMaxZ = 9;

        VFXConfig VFXConfig;

        public bool isMovingToBar { get; set; }

        public bool isOutlining;

        private static float yRotation;
        private float yRotationSpeed = 1f;

        private void Awake()
        {
            collider = GetComponent<Collider>();
            rigidbody = GetComponent<Rigidbody>();
            outline = GetComponent<Outline>();

            VFXConfig = Resources.Load<VFXConfig>("VFX");
        }
        public IEnumerator MoveToTarget(Vector3 targetPosition, Action OnComplete = null)
        {
            collider.enabled = false;
            rigidbody.isKinematic = true;

            Vector3 startPosition = transform.position;
            Quaternion startRoation = transform.rotation;

            Quaternion endRotation = Quaternion.Euler(new Vector3(slotRotation.x, transform.rotation.eulerAngles.y, slotRotation.z) );

            isRotate = true;
            var move =  transform.DOMove(targetPosition + Vector3.up, movementDuration);
            move.SetEase(Ease.Unset);
            //transform.DORotate(slotRotation, movementDuration);

            yield return new WaitForSeconds(movementDuration);

            //transform.rotation = endRotation;
            transform.position = targetPosition + Vector3.up;

            OnComplete?.Invoke();
        }
        private void Update()
        {
            if (!isRotate)
            {
                Vector3 tmpPos = transform.position;
                tmpPos.x = Mathf.Clamp(tmpPos.x, posMinX, posMaxX);
                tmpPos.y = Mathf.Clamp(tmpPos.y, posMinY, posMaxY);
                tmpPos.z = Mathf.Clamp(tmpPos.z, posMinZ, posMaxZ);

                transform.position = tmpPos;
            }
        
            else
            {
                transform.rotation = Quaternion.Euler(slotRotation.x, yRotation, slotRotation.z);
                slotRotation.y = yRotation;
            }
            yRotation += Time.deltaTime * yRotationSpeed;
     

        }
        public void SetSelectedItem(int touchIndex)
        {
            if (LevelController.Instance.IsCanMakeTurn == true && LevelController.Instance.IsPlayingLevel == true)
            {
                outline.enabled = true;
                rigidbody.isKinematic = true;
                if (isOutlining == false)
                {
                    StopAllCoroutines();
                    StartCoroutine(EnableOutline(touchIndex));
                    StartCoroutine(ScaleItem(selectedScale));
                }
            }
        }
        public void DeselectItem()
        {
            if (rigidbody != null && isRotate == false)
            {
                rigidbody.isKinematic = false;
            }
            if (outline != null)
            {
                outline.enabled = false;
            }
            if (isMovingToBar == false)
            {
                StopAllCoroutines();
                StartCoroutine(ScaleItem(normalScale));
            }
            isOutlining = false;
        }
        private IEnumerator EnableOutline(int touchIndex)
        {
            isOutlining = true;
            float fadeDuration = 0.4f;
            float elapsedTime = 0;
            for (int i = 0; i < PlayerController.selectedItems[touchIndex].Count; i++)
            {
                PlayerController.selectedItems[touchIndex][i].DeselectItem();
                PlayerController.selectedItems[touchIndex].Clear();
            }
            if (SettingsController.isVibrationOn == true)
                MMVibrationManager.Haptic(HapticTypes.SoftImpact, false, true, this);
            PlayerController.selectedItems[touchIndex].Add(this);
            while (elapsedTime < fadeDuration)
            {
                float alpha = Mathf.Lerp(0f, 4f, (elapsedTime / fadeDuration));
                outline.OutlineWidth = alpha;
                elapsedTime += Time.deltaTime;
                yield return null;
            }
      
        }
        public IEnumerator ScaleItem(float scale)
        {

            isOutlining = true;
            float scaleDuration = 0.15f;
            float elapsedTime = 0;

            Vector3 targetScale = new Vector3(scale, scale, scale);
            Vector3 startScale = transform.localScale;

            while (elapsedTime < scaleDuration)
            {
                transform.localScale = Vector3.Lerp(startScale, targetScale, (elapsedTime / scaleDuration));
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            transform.localScale = targetScale;
        }
        public IEnumerator ScaleItem(float startScaleValue, float targetScaleValue, float duration)
        {
            float scaleDuration = duration;
            float elapsedTime = 0;

            Vector3 targetScale = new Vector3(targetScaleValue, targetScaleValue, targetScaleValue);
            Vector3 startScale = new Vector3(startScaleValue, startScaleValue, startScaleValue);

            while (elapsedTime < scaleDuration)
            {
                transform.localScale = Vector3.Lerp(startScale, targetScale, (elapsedTime / scaleDuration));
                elapsedTime += Time.deltaTime;
                yield return null;
            }
            transform.localScale = targetScale;
        }
    }
}