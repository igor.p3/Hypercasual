using UnityEngine;
using System.Globalization;
using Firebase.Database;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using Firebase.Extensions;
using Firebase.Firestore;
using UnityEngine.Networking;
using VoxelBusters.EssentialKit;
using Newtonsoft.Json;

namespace Matchmania
{
    public class LeaderboardController : MonoBehaviour
    {
        private static RegionInfo region;

        private static FirebaseFirestore firestoreDatabase;
        private static CollectionReference leaderboardsCollection;

        public static List<PlayerDataModel> worldLeaders = new List<PlayerDataModel>();
        public static List<PlayerDataModel> countryLeaders = new List<PlayerDataModel>();

        public static Timer internalWorldCDTimer, internalCountryCDTimer;
        private static float amountOfCDSeconds = 100;

        private static bool isNoConnectionToDB;

        void Start()
        {
            firestoreDatabase = DatabaseModel.FirestoreDatabase;
            
            // User name check and fill if it's empty
            if (GameController.playerName == "" || GameController.playerName == string.Empty ||
                GameController.playerName == null)
            {
                GameController.playerName = GenerateUserName();
                GameController.SaveGameData();
                Debug.LogWarning($"Generated new user name: {GameController.playerName}");
            }

            region = RegionIdentifier.Instance.GetCurrentRegion();
            
            if (firestoreDatabase == null || InternetCheckerController.isNoInternetConnection)
            {
                isNoConnectionToDB = true;
                return;
            }

            leaderboardsCollection = firestoreDatabase.Collection("leaderboards");
            LevelController.OnLevelNumChanged += (a) => SendPlayersScoreData(null, null);
            RegionIdentifier.Instance.OnRegionChange += info => region = info;
        }

        public static void SendPlayersScoreData(Action successCallback, Action failedCallback)
        {
            if (isNoConnectionToDB) return;
            if (string.IsNullOrEmpty(GameController.playerName) || GameController.playerName == "")
            {
                Debug.LogError("The player's name is empty, can't add player to DB yet");
                failedCallback?.Invoke();
                return;
            }

            if (region == null)
            {
                Debug.LogError("The region is empty, can't add player to DB yet");
                failedCallback?.Invoke();
                return;
            }
            
            PlayerDataModel playerModel = new PlayerDataModel(GameController.playerName,
                LevelController.CurrentLevelIndex + 1, region.DisplayName);
            leaderboardsCollection.Document(playerModel.playerName).SetAsync(playerModel).ContinueWithOnMainThread(task =>
            {
                Debug.LogWarning("The database has been updated with players score data");
                successCallback?.Invoke();
            });
        }

        public static void DeletePlayerData()
        {
            if (isNoConnectionToDB) return;
            if (GameController.playerName == "" || string.IsNullOrEmpty(GameController.playerName))
            {
                Debug.LogError("The player's name is empty, can't add him to DB");
                return;
            }
            
            leaderboardsCollection.Document(GameController.playerName).DeleteAsync();
            
            // Remove player from local leaderboards
            var playerInWorldLeaders = worldLeaders.Find((name) => name.playerName == GameController.playerName);
            var playerInCountryLeaders = countryLeaders.Find((name) => name.playerName == GameController.playerName);
            if (playerInWorldLeaders != null)
            {
                worldLeaders.Remove(playerInWorldLeaders);
            }

            if (playerInCountryLeaders != null)
            {
                countryLeaders.Remove(playerInCountryLeaders);
            }
            
        }

        public static void TestSendPlayerData(string playerName, long playerScore)
        {
            if (isNoConnectionToDB) return;

            if (GameController.playerName == "" || string.IsNullOrEmpty(GameController.playerName))
            {
                Debug.LogError("The player's name is empty, can't add him to DB");
                return;
            }
            PlayerDataModel playerModel = new PlayerDataModel(playerName,
                playerScore, region.DisplayName);
            leaderboardsCollection.Document(playerName).SetAsync(playerModel).ContinueWith(task =>
            {
                Debug.LogWarning("The database has been updated with players score data");
            });
        }
        private string GenerateUserName()
        {
            var randomUniqueString =
                $"{UnityEngine.Random.Range(1, 10)}{UnityEngine.Random.Range(1, 10)}" +
                $"{UnityEngine.Random.Range(1, 10)}{UnityEngine.Random.Range(1, 10)}" +
                $"{UnityEngine.Random.Range(1, 10)}{UnityEngine.Random.Range(1, 10)}" +
                $"{UnityEngine.Random.Range(1, 10)}{UnityEngine.Random.Range(1, 10)}" +
                $"{(char) UnityEngine.Random.Range('a', 'z')}";

            return $"Player_{randomUniqueString}";
        }

        public static void ChangePlayersNickname(string oldNickname)
        {
            var oldPlayerInWorldList = worldLeaders.FirstOrDefault(player => player.playerName == oldNickname);
            if (oldPlayerInWorldList == null) return;
            
            // Remove old nickname entry from leaderboard
            leaderboardsCollection.Document(oldNickname).DeleteAsync();
            
            // Change the field with player name locally
            oldPlayerInWorldList.playerName = GameController.playerName;
        }
        public static void GetLeadersFromLeaderBoard(LeaderboardSortingType sortingType)
        {
            if (isNoConnectionToDB) return;
            if (region == null)
            {
                Debug.LogError("Couldn't connect because region wasn't specified");
                return;
            }

            //var m_leaderBoardDBReference = firestoreDatabase.Child("leaderboards");

            // Get the data, order by child "passedLevel"
            if (sortingType == LeaderboardSortingType.World)
            {
                if (internalWorldCDTimer.IsDone == false)
                {
                    Debug.LogError("The CD timer hasn't finished yet");
                    return;
                }

                internalWorldCDTimer = new Timer(amountOfCDSeconds);
                Debug.LogWarning("Loading of world leaderboard has been started");
                
                leaderboardsCollection.OrderByDescending("passedLevel")
                    .Limit(50)
                    .GetSnapshotAsync().ContinueWithOnMainThread((Task<QuerySnapshot> task) =>
                    {
                        QuerySnapshot querySnapshot = task.Result;
                        if (task.IsFaulted)
                        {
                            Debug.LogError("Data was not retrieved with error");
                        }
                        else if (task.IsCompleted)
                        {
                            Debug.LogWarning(
                                $"Received snapshot of the size of {querySnapshot.Count}");
                            
                            worldLeaders.Clear();
                            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
                            {
                                PlayerDataModel playerData = documentSnapshot.ConvertTo<PlayerDataModel>();
                                worldLeaders.Add(playerData);
                            
                            }
                            Debug.LogWarning($"Loaded all world leaders {worldLeaders.Count} of them"); 
                        }
                    });
            }

            // Get the data, order by child "Country"
            if (sortingType == LeaderboardSortingType.Country)
            {
                if (internalCountryCDTimer.IsDone == false)
                {
                    Debug.LogError("The CD timer hasn't finished yet");
                    return;
                }

                internalCountryCDTimer = new Timer(amountOfCDSeconds);
                Debug.LogWarning("Loading of country leaderboard has been started");

                leaderboardsCollection
                    .WhereEqualTo("country", region.DisplayName)
                    .OrderByDescending("passedLevel")
                    .Limit(50)
                    .GetSnapshotAsync().ContinueWithOnMainThread((Task<QuerySnapshot> task) =>
                    {
                        QuerySnapshot querySnapshot = task.Result;
                        if (task.IsFaulted)
                        {
                            Debug.LogError($"Data was not retrieved with error {task.Exception}");
                        }
                        else if (task.IsCompleted)
                        {
                            Debug.LogWarning(
                                $"Received snapshot of the size of {querySnapshot.Count}");
                            
                            countryLeaders.Clear();
                            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
                            {
                                PlayerDataModel playerData = documentSnapshot.ConvertTo<PlayerDataModel>();
                                countryLeaders.Add(playerData);
                            
                            }
                            Debug.LogWarning($"Loaded all country leaders {countryLeaders.Count} of them"); 
                        }
                        
                    });
                
            }
        }

        public static void GetLeadersFromLeaderBoardNoCD(LeaderboardSortingType sortingType)
        {
            if (isNoConnectionToDB) return;
            if (region == null)
            {
                Debug.LogError("Couldn't connect because region wasn't specified");
                return;
            }
            
            if (sortingType == LeaderboardSortingType.World)
            {
                Debug.LogWarning("Loading of world leaderboard has been started");

                leaderboardsCollection.OrderByDescending("passedLevel")
                    .Limit(50)
                    .GetSnapshotAsync().ContinueWithOnMainThread((Task<QuerySnapshot> task) =>
                    {
                        QuerySnapshot querySnapshot = task.Result;
                        if (task.IsFaulted)
                        {
                            Debug.LogError("Data was not retrieved with error");
                        }
                        else if (task.IsCompleted)
                        {
                            Debug.LogWarning(
                                $"Received snapshot of the size of {querySnapshot.Count}");

                            worldLeaders.Clear();
                            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
                            {
                                PlayerDataModel playerData = documentSnapshot.ConvertTo<PlayerDataModel>();
                                worldLeaders.Add(playerData);
                            }

                            Debug.LogWarning($"Loaded all world leaders {worldLeaders.Count} of them");
                        }
                    });
            }

            // Get the data, order by child "Country"
            if (sortingType == LeaderboardSortingType.Country)
            {
                Debug.LogWarning("Loading of country leaderboard has been started");

                leaderboardsCollection
                    .WhereEqualTo("country", region.DisplayName)
                    .OrderByDescending("passedLevel")
                    .Limit(50)
                    .GetSnapshotAsync().ContinueWithOnMainThread((Task<QuerySnapshot> task) =>
                    {
                        QuerySnapshot querySnapshot = task.Result;
                        if (task.IsFaulted)
                        {
                            Debug.LogError($"Data was not retrieved with error {task.Exception}");
                        }
                        else if (task.IsCompleted)
                        {
                            Debug.LogWarning(
                                $"Received snapshot of the size of {querySnapshot.Count}");

                            countryLeaders.Clear();
                            foreach (DocumentSnapshot documentSnapshot in querySnapshot.Documents)
                            {
                                PlayerDataModel playerData = documentSnapshot.ConvertTo<PlayerDataModel>();
                                countryLeaders.Add(playerData);
                            }

                            Debug.LogWarning($"Loaded all country leaders {countryLeaders.Count} of them");
                        }
                    });
            }
        }
    }
}
