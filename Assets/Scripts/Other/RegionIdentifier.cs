using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Matchmania;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class RegionIdentifier : MonoBehaviour
{
    #region Singleton
    private static RegionIdentifier _instance;
    public static RegionIdentifier Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<RegionIdentifier>();
            }

            return _instance;
        }
    }
    #endregion
    
    public static string savedCountryName;
    private RegionInfo region;
    public static bool isRegionFetchedByIP;
    public event Action<RegionInfo> OnRegionChange;

    private void Start()
    {
        SetCurrentRegion();
    }

    private void SetCurrentRegion()
    {
        if (isRegionFetchedByIP)
        {
            SetSavedOrSystemRegion();
            return;
        }
        
        // Determine the country name if there are internet - by IP
        if (InternetCheckerController.isNoInternetConnection == false)
        {
            StartCoroutine(DetermineCountryByIP(
                () =>
                {
                    isRegionFetchedByIP = true; 
                    Debug.LogWarning("Fetched IP's country");
                },
                () =>
                {
                    isRegionFetchedByIP = false;
                    SetSavedOrSystemRegion();
                    Debug.LogWarning("Couldn't fetch the country IP, using system region");
                }));
        }
        // Otherwise by system
        else
        {
            SetSavedOrSystemRegion();
        }
    }

    private void SetSavedOrSystemRegion()
    {
        if (savedCountryName != null || savedCountryName != "" || savedCountryName != string.Empty)
        {
            // Use saved region
            region = new RegionInfo(savedCountryName);
        }
        else
        {
            SetSystemRegion();
        }
    }

    private void SetSystemRegion()
    {
        savedCountryName = RegionInfo.CurrentRegion.Name;
        region = RegionInfo.CurrentRegion;
        isRegionFetchedByIP = true;
    }

    private IEnumerator DetermineCountryByIP(Action successCallback, Action failCallback)
    {
        UnityWebRequest www = UnityWebRequest.Get("https://ipapi.co/country/");
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogWarning(www.error);
            failCallback?.Invoke();
        }
        else
        {
            try
            {
                var info = www.downloadHandler.text;
                if (info != null)
                {
                    region = new RegionInfo(info);
                    if (region == null)
                    {
                        failCallback?.Invoke();
                        yield break;
                    }

                    savedCountryName = region.Name;
                    OnRegionChange?.Invoke(region);
                    successCallback?.Invoke();
                }
                
                else
                {
                    failCallback?.Invoke();
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning(e);
                failCallback?.Invoke();
            }
        }
    }
    [System.Serializable]
    public class IpInfo
    {
        public string country { get; set; }
    }
    public RegionInfo GetCurrentRegion()
    {
        return region ?? RegionInfo.CurrentRegion;
    }
}