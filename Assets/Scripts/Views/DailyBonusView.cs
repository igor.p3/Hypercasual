using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

namespace Matchmania
{
    public class DailyBonusView : MonoBehaviour
    {
        [SerializeField] private MainMenuView mainMenuView;

        [SerializeField] private GameObject dailyBonusScreen;
        [SerializeField] private GameObject chestsScreen;
        [SerializeField] private GameObject rewardScreen;

        [SerializeField] private Image rewardImage;
        [SerializeField] private TextMeshProUGUI rewardCountText;

        [SerializeField] private Animator[] dailyBonusChests;

        [SerializeField] private Button[] dailyBonusButtons;

        [SerializeField] private Button mainMenuDailyBonusButton;

        private bool isCanTapChest = true;

        void Start()
        {
            DailyBonusController.OnDailyRewardOpenedEvent += TurnOnDailyBonusButton;
            mainMenuDailyBonusButton.onClick.AddListener(OpenDailyRewardScreen);
        }
        private void TurnOnDailyBonusButton()
        {
            mainMenuDailyBonusButton.gameObject.SetActive(true);
        }
        private void OpenDailyRewardScreen()
        {
            dailyBonusScreen.SetActive(true);
            chestsScreen.SetActive(true);
            rewardScreen.SetActive(false);
        }
        public void OpenChest(int index)
        {
            if (isCanTapChest)
            {
                dailyBonusChests[index].Play("Open");
                ChestsRewardsController.Instance.OpenDailyChest(chestsScreen, rewardScreen,rewardImage,mainMenuView,rewardCountText);
                isCanTapChest = false;
            }
            DailyBonusController.stringLastRewardDate = DateTime.Now.ToString();

            mainMenuDailyBonusButton.gameObject.SetActive(false);
        }
        
    }
}