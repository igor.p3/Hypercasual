﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Lean.Pool;

namespace Matchmania
{
    public class SceneLoadingController : MonoBehaviour
    {
        [SerializeField] private Slider loadingSlider;

        void Start()
        {
            GameModel.LoadlevelConfig(this);
            GameModel.LoadItemsProgressionConfig(this);
            StartCoroutine(LoadScene());
        }
        public IEnumerator LoadScene()
        {
            loadingSlider.value = 0;
            loadingSlider.maxValue = GameController.items.Count;

            for (int i = 0; i < GameController.items.Count; i++)
            {
                loadingSlider.value = i;

                var item_01 = LeanPool.Spawn(GameController.items[i]);
                var item_02 = LeanPool.Spawn(GameController.items[i]);
                var item_03 = LeanPool.Spawn(GameController.items[i]);

                yield return null;

                LeanPool.Despawn(item_01);
                LeanPool.Despawn(item_02);
                LeanPool.Despawn(item_03);
            }
            yield return new WaitUntil(() => GameModel.loadingIndex == 2);

            UpdateUnlockedItems();

            SceneManager.LoadScene(1);
        }

        public static void UpdateUnlockedItems()
        {
            GameController.unlockedItems.Clear();

            foreach (var item in GameModel.Items.Items)
            {
                if (LevelController.CurrentLevelIndex >= item.levelComplete)
                {
                    GameController.lastUnlockedItems.Clear();

                    foreach (var unlockedItem in item.progressionItems)
                    {
                        GameController.unlockedItems.Add(LevelController.GetItemByName(unlockedItem));
                        GameController.lastUnlockedItems.Add(LevelController.GetItemByName(unlockedItem));
                    }
                }
                else break;
            }
            if (LevelController.CurrentLevelIndex > 5)
            {
                for (int i = 0; i < GameController.unlockedItems.Count; i++)
                {
                    for (int j = 0; j < GameController.lastUnlockedItems.Count; j++)
                    {
                        if (GameController.unlockedItems[i].name == GameController.lastUnlockedItems[j].name)
                        {
                            GameController.unlockedItems.Remove(GameController.unlockedItems[i]);
                        }
                    }
                }
            }
        }
    }
}