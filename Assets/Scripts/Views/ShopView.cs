using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Matchmania
{
    public class ShopView : MonoBehaviour
    {
        public IAPController IAPController;

        [Header("Offers android")]
        public GameObject specialOfferAndroidButton;
        
        [Header("Shop android")]
        public GameObject starterBundleAndroidButton;
        public GameObject premiumBundleAndroidButton;
        public GameObject epicBundleAndroidButton;
        public GameObject removeAdsBundleAndroidButton;
        public GameObject removeAdsBundleAndroidShopButton;
        public GameObject megaBundleAndroidButton;
        
        [Header("Offers iOS")]
        public GameObject specialOfferIOSButton;

        [Header("Shop iOS")]

        public GameObject starterBundleIOSButton;
        public GameObject premiumBundleIOSButton;
        public GameObject epicBundleIOSButton;
        public GameObject removeAdsBundleIOSButton;
        public GameObject removeAdsBundleIOSShopButton;
        public GameObject megaBundleIOSButton;

        void Awake()
        {
            SetUpIAPButtons();
        }

        private void SetUpIAPButtons()
        {
#if UNITY_ANDROID
            specialOfferAndroidButton.SetActive(true);
            starterBundleAndroidButton.SetActive(true);
            premiumBundleAndroidButton.SetActive(true);
            epicBundleAndroidButton.SetActive(true);
            removeAdsBundleAndroidButton.SetActive(true);
            removeAdsBundleAndroidShopButton.SetActive(true);
            megaBundleAndroidButton.SetActive(true);

            IAPController.removeAdsButton = removeAdsBundleAndroidButton;

            Debug.Log("Android iAP buttons are initiailized");

#elif UNITY_IPHONE
            specialOfferIOSButton.SetActive(true);
            starterBundleIOSButton.SetActive(true);
            premiumBundleIOSButton.SetActive(true);
            epicBundleIOSButton.SetActive(true);
            removeAdsBundleIOSButton.SetActive(true);
            removeAdsBundleIOSShopButton.SetActive(true);
            megaBundleIOSButton.SetActive(true);

            IAPController.removeAdsButton = removeAdsBundleIOSButton;

            Debug.Log("iOS iAP buttons are initiailized");
#endif
        }
    }
}