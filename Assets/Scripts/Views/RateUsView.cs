using UnityEngine;
#if UNITY_IPHONE
using UnityEngine.iOS;
#endif

using UnityEngine.UI;
using TMPro;

namespace Matchmania
    {
    public class RateUsView : MonoBehaviour
    {
        #region Singleton
        private static RateUsView _instance;
        public static RateUsView Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<RateUsView>();
                }

                return _instance;
            }
        }
        #endregion
        [SerializeField] LevelView levelView;

        [SerializeField] private GameObject rateUsScreen;
        [SerializeField] private Image[] stars;
        [SerializeField] private Sprite goldenStar;
        [SerializeField] private Sprite greyStar;

        [SerializeField] private Button star_01;
        [SerializeField] private Button star_02;
        [SerializeField] private Button star_03;
        [SerializeField] private Button star_04;
        [SerializeField] private Button star_05;

        [SerializeField] private Button closeButton;
        [SerializeField] private Button rateUsButton;
        [SerializeField] private Button notNowButton;

        [SerializeField] TextMeshProUGUI submitText;

        public static bool isCanShowRateUs = true;

        private float rateValue = 1;

        void Start()
        {
            EnableStars(false, 5);

            star_01.onClick.AddListener(Star_01_ButtonPressed);
            star_02.onClick.AddListener(Star_02_ButtonPressed);
            star_03.onClick.AddListener(Star_03_ButtonPressed);
            star_04.onClick.AddListener(Star_04_ButtonPressed);
            star_05.onClick.AddListener(Star_05_ButtonPressed);

            closeButton.onClick.AddListener(CloseRateUsScreen);
            rateUsButton.onClick.AddListener(RateUsButtonPressed);
            notNowButton.onClick.AddListener(CloseRateUsScreen);
        }
        private void RateUsButtonPressed()
        {
            if (rateValue == 1 || rateValue == 2 || rateValue == 3)
            {
                CloseRateUsScreen();
            }
            else
            {
                CloseRateUsScreen();
#if UNITY_IOS
 Device.RequestStoreReview();
#elif UNITY_ANDROID

                Application.OpenURL("https://play.google.com/store/apps/details?id=com.Moonee.Matchmania3d");
#endif
            }
        }
        private void EnableStars(bool isToEnable, int count)
        {
            for (int i = 0; i < count; i++)
            {
                if (isToEnable == true)
                {
                    stars[i].sprite = goldenStar;
                }
                else stars[i].sprite = greyStar;
            }
        }
        public void ShowRateUsScreen()
        {
            if (isCanShowRateUs)
            {
                rateUsScreen.SetActive(true);
                levelView.victoryScreen.SetActive(false);
            }
        }
        private void CloseRateUsScreen()
        {
            rateUsScreen.SetActive(false);
            submitText.color = Color.grey;
            levelView.victoryScreen.SetActive(true);
        }
        private void Star_01_ButtonPressed()
        {
            rateValue = 1;
            EnableStars(false, 5);
            EnableStars(true, 1);

            isCanShowRateUs = false;
            GameController.SaveGameData();

            submitText.color = Color.white;
        }
        private void Star_02_ButtonPressed()
        {
            rateValue = 2;
            EnableStars(false, 5);
            EnableStars(true, 2);

            isCanShowRateUs = false;
            GameController.SaveGameData();

            submitText.color = Color.white;
        }
        private void Star_03_ButtonPressed()
        {
            rateValue = 3;
            EnableStars(false, 5);
            EnableStars(true, 3);

            isCanShowRateUs = false;
            GameController.SaveGameData();

            submitText.color = Color.white;
        }
        private void Star_04_ButtonPressed()
        {
            rateValue = 4;
            EnableStars(false, 5);
            EnableStars(true, 4);

            isCanShowRateUs = false;
            GameController.SaveGameData();

            submitText.color = Color.white;

        }
        private void Star_05_ButtonPressed()
        {
            rateValue = 5;
            EnableStars(false, 5);
            EnableStars(true, 5);

            isCanShowRateUs = false;
            GameController.SaveGameData();

            submitText.color = Color.white;
        }
    }
}