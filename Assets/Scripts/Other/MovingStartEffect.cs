﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Matchmania
{
    public class MovingStartEffect : MonoBehaviour
    {
        [SerializeField] private GameObject starImage;
        [SerializeField] private VFXConfig VFX_Config;
        [SerializeField] private SFXConfig SFX_Config;
        [SerializeField] private float movementDuration = 0.75f;
        private RectTransform targetTransform;
        private Vector3 targetPosition;

        void Start()
        {
            targetTransform = GameObject.FindGameObjectWithTag("Star").GetComponent<RectTransform>();
            targetPosition = targetTransform.position;
            StartCoroutine(MoveToTarget());
        }

        IEnumerator MoveToTarget()
        {
            yield return new WaitForSeconds(0.3f);


           var movement = transform.DOMove(targetPosition, movementDuration);

            movement.SetEase(Ease.InCubic);

            transform.DOScale(0.7f, movementDuration);

            while (Vector2.Distance(transform.position, targetPosition) > 0.1f)
            {
                yield return null;
            }
            transform.position = targetPosition;

            //Vector3 worldPos = Camera.main.ViewportToWorldPoint(GetComponent<RectTransform>().position);
            //var effect = VFX_Config.PlayEffect(worldPos, VFX_Config.starGlow);
            //effect.transform.SetParent(transform);
            //effect.transform.localPosition = Vector3.zero;
            //effect.transform.parent = null;

            SFX_Config.PlaySoundEffect(SFX_Config.starHitsUI);
            starImage.SetActive(false);
        }
    }
}