﻿using System;

namespace Matchmania
{
    public class CurrencyController
    {
        public static Action OnChangeCurrencyEventHandler;

        public static int Stars { get; set; }
        public static int LevelsChestCount { get; set; }
        public static int StarsChestCount { get; set; }
        public static int Coins { get; set; } = 100;
        public static int Hints { get; set; } = 5;
        public static int Fans { get; set; } = 1;
        public static int Hearts { get; set; } = 5;
        public static int TTTokens { get; set; } = 0;
    }
}