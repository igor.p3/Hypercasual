using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Matchmania
{
    public class NewOpenedItemsView : MonoBehaviour
    {
        public GameObject openedItemsScreen;
        [SerializeField] private GameObject[] itemsPositions;
        [SerializeField] private Button getItemsButton;

        private void Start()
        {
            getItemsButton.onClick.AddListener(GetItemsButtonPressed);
        }
        private void GetItemsButtonPressed()
        {
            LevelView.CloseScreen(openedItemsScreen);
            ClearItems();
        }
        public void SetItemsView()
        {
            ClearItems();
            StartCoroutine(ShowItems());
           
        }
        IEnumerator ShowItems()
        {

            SceneLoadingController.UpdateUnlockedItems();

            for (int i = 0; i < GameController.lastUnlockedItems.Count; i++)
            {
                var item = Instantiate(GameController.lastUnlockedItems[i]);

                item.isRotate = true;
                item.rigidbody.isKinematic = true;
                item.transform.parent = itemsPositions[i].transform;
                item.transform.localPosition = Vector3.zero;
                item.transform.rotation = Quaternion.Euler(Vector3.zero);
                item.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
                item.transform.DOScale(0.7f, 0.25f);
                item.gameObject.layer = 10;

                yield return new WaitForSeconds(0.1f);

                if(i == itemsPositions.Length -1)
                {
                    break;
                }
            }
        }
        private void ClearItems()
        {
            foreach (var item in itemsPositions)
            { 
                if(item.transform.childCount != 0)
                {
                    Destroy(item.transform.GetChild(0).gameObject);
                }
            }
        }
    }
}