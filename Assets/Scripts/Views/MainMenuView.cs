﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Matchmania
{
    public class MainMenuView : MonoBehaviour
    {
        [Header("References")]

        [SerializeField] private LevelView levelView;
        [SerializeField] private SFXConfig SFXConfig;

        [Header("Screens")]

        [SerializeField] private GameObject levelScreen;
        [SerializeField] private GameObject mainMenuScreen;
        [SerializeField] private GameObject levelsChestScreen;
        [SerializeField] private GameObject starsChestScreen;
        [SerializeField] private GameObject startLevelScreen;
        [SerializeField] private GameObject settingsScreen;
        public GameObject footerScreen;

        [Header("Texts")]

        [SerializeField] private TextMeshProUGUI coinsAmountText;
        [SerializeField] private TextMeshProUGUI coinsInShopAmountText;
        [SerializeField] private TextMeshProUGUI currentLevelIndexText;
        [SerializeField] private TextMeshProUGUI playerLevelIndexText;
        [SerializeField] private TextMeshProUGUI levelsChestCount;
        [SerializeField] private TextMeshProUGUI starsChestCount;

        [Header("Animators")]

        [SerializeField] private Animator levelsChestAnimator;
        [SerializeField] private Animator starsChestAnimator;
        [SerializeField] private Animator levelsChestRewardAnimator;
        [SerializeField] private Animator starsChestRewardAnimator;

        [Header("Sprites")]

        public Sprite hintSprite;
        public Sprite coinsSprite;
        public Sprite fanSprite;
        public Sprite unlimHeartsSprite;

        [Header("Images")]

        [SerializeField] private Image levelsChestRewardImage;
        [SerializeField] private Image starsChestRewardImage;
        [SerializeField] private Image levelChestImage;
        [SerializeField] private Image starChestImage;
        [SerializeField] private Image playerLevelFilledImage;

        [Header("Buttons")]

        [SerializeField] private Button claimLevelsChestRewardButton;
        [SerializeField] private Button claimStarsChestRewardButton;

        [SerializeField] private Button startClaimLevelsChestRewardButton;
        [SerializeField] private Button startClaimStarsChestRewardButton;

        [SerializeField] private Button levelsChestRewardButton;
        [SerializeField] private Button starsChestRewardButton;
        [SerializeField] private Button startLevelButton;
        [SerializeField] private Button settingsButton;
        [SerializeField] private Button closeSettingsButton;
        [SerializeField] private Button startGameButton;

        [Header("Sliders")]

        [SerializeField] Slider levelsChestSlider;
        [SerializeField] Slider starsChestSlider;

        [Header("Sprites")]

        [SerializeField] Sprite levelsChestSprite;
        [SerializeField] Sprite starsChestSprite;

        public Action OnMenuStartLevelButtonPressed;

        


        private void Start()
        {
            LevelView.GoHomeEventHandler += UpdateMainMenuView;
            UpdateMainMenuView();
            startLevelButton.onClick.AddListener(StartLevelButtonPressed);
            startGameButton.onClick.AddListener(StartTheGame);
            CurrencyController.OnChangeCurrencyEventHandler += UpdateMainMenuView;

            claimLevelsChestRewardButton.onClick.AddListener(ClaimLevelsChestRewardButtonPressed);
            claimStarsChestRewardButton.onClick.AddListener(ClaimStarsChestRewardButtonPressed);

            settingsButton.onClick.AddListener(OpenSettingsButtonPressed);
            closeSettingsButton.onClick.AddListener(CloseSettingsButtonPressed);

            UpdateMainMenuView();
            CurrencyController.OnChangeCurrencyEventHandler += UpdateMainMenuView;
   
            UpdatePlayerLevel();

            PlayerLevelUpgrade.OnUpdateEventHandler += UpdatePlayerLevel;

            startClaimStarsChestRewardButton.onClick.AddListener(StarsChestRewardButtonPressed);
            startClaimLevelsChestRewardButton.onClick.AddListener(LevelsChestRewardButtonPressed);

            levelsChestRewardButton.onClick.AddListener(OpenLevelsChestRewardScreen);
            starsChestRewardButton.onClick.AddListener(OpenStarsChestRewardScreen);

            if(LevelController.CurrentLevelIndex == 0)
            {
                StartLevelButtonPressed();
            }

        }
        public void OpenSettingsButtonPressed()
        {
            if(LevelController.Instance.isReviveScreenOpened == false)
            LevelView.OpenScreen(settingsScreen);
        }
        public void CloseSettingsButtonPressed()
        {
            LevelView.CloseScreen(settingsScreen);
        }
        public void UpdatePlayerLevel()
        {
            playerLevelIndexText.text = PlayerLevelUpgrade.GetPlayerLevel().ToString();
            float value = (float)CurrencyController.StarsChestCount / 500f;
            playerLevelFilledImage.fillAmount = value;
        }
        public void StartLevelButtonPressed()
        {
            //if (HeartsController.Instance.HeartsCheck() == false) return;
            OnMenuStartLevelButtonPressed?.Invoke();
            // todo uncomment this line to get the start level screen back  
            //startLevelScreen.SetActive(true);
            // todo Comment this string to get the start level screen back
            StartTheGame();
            footerScreen.SetActive(false);
        }

        private void StartTheGame()
        {
            //if (HeartsController.Instance.HeartsCheck() == false) return;
            LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
            levelScreen.SetActive(true);
            startLevelScreen.SetActive(false);
            mainMenuScreen.SetActive(false);
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
        }
        public void UpdateMainMenuView()
        {
            currentLevelIndexText.text =/* "Level " +*/ (LevelController.CurrentLevelIndex + 1).ToString();
            coinsAmountText.text = CurrencyController.Coins.ToString();
            coinsInShopAmountText.text = CurrencyController.Coins.ToString();

            levelsChestCount.text = CurrencyController.LevelsChestCount.ToString() + "/" + levelsChestSlider.maxValue.ToString();
            levelsChestSlider.value = CurrencyController.LevelsChestCount;

            starsChestCount.text = CurrencyController.StarsChestCount.ToString() + "/" + starsChestSlider.maxValue.ToString();
            starsChestSlider.value = CurrencyController.StarsChestCount;

            if (CurrencyController.StarsChestCount >= starsChestSlider.maxValue)
            {
                starsChestCount.text = "Open";
                starsChestSlider.value = starsChestSlider.maxValue;
            }
            else
            {
                starsChestCount.text = CurrencyController.StarsChestCount.ToString() + "/" + starsChestSlider.maxValue.ToString();
                starsChestSlider.value = CurrencyController.StarsChestCount;
            }
            if (CurrencyController.LevelsChestCount >= levelsChestSlider.maxValue)
            {
                levelsChestCount.text = "Open";
                levelsChestSlider.value = levelsChestSlider.maxValue;
            }
            else
            {
                levelsChestCount.text = CurrencyController.LevelsChestCount.ToString() + "/" + levelsChestSlider.maxValue.ToString();
                levelsChestSlider.value = CurrencyController.LevelsChestCount;
            }
        }

        #region Chests
        public void OpenLevelsChestRewardScreen()
        {
            if (!(CurrencyController.LevelsChestCount >= 10)) return;

            levelsChestScreen.gameObject.SetActive(true);


            //levelsChestAnimator.Play("Opening", 0, 0f);
            levelsChestAnimator.speed = 0f;
            levelsChestAnimator.enabled = false;
            levelChestImage.sprite = levelsChestSprite;

            footerScreen.SetActive(false);
        }
        public void OpenStarsChestRewardScreen()
        {
    
            if (!(CurrencyController.StarsChestCount >= 500)) return;

            starsChestScreen.gameObject.SetActive(true);

    
            //starsChestAnimator.Play("Opening", 0, 0f);
            starsChestAnimator.speed = 0f;
            starsChestAnimator.enabled = false;
            starChestImage.sprite = starsChestSprite;

            footerScreen.SetActive(false);
        }
        public void LevelsChestRewardButtonPressed()
        {
         
            CurrencyController.LevelsChestCount -= 10;
            ChestsRewardsController.Instance.GenerateRewards(claimLevelsChestRewardButton, levelsChestScreen, levelsChestAnimator, levelsChestRewardAnimator, levelsChestRewardImage, this);
            UpdateMainMenuView();
            GameController.SaveGameData();
            startClaimLevelsChestRewardButton.gameObject.SetActive(false);
            levelView.UpdateRewardedChestsView();

            levelsChestAnimator.speed = 1f;


        }
     
        public void StarsChestRewardButtonPressed()
        {
            CurrencyController.StarsChestCount -= 500;
            ChestsRewardsController.Instance.GenerateRewards(claimStarsChestRewardButton, starsChestScreen, starsChestAnimator, starsChestRewardAnimator, starsChestRewardImage, this);
            UpdateMainMenuView();
            GameController.SaveGameData();
            startClaimStarsChestRewardButton.gameObject.SetActive(false);
            levelView.UpdateRewardedChestsView();

            starsChestAnimator.speed = 1f;
        }
        private void ClaimLevelsChestRewardButtonPressed()
        {
            levelsChestScreen.SetActive(false);
            levelsChestRewardImage.gameObject.SetActive(false);
            startClaimLevelsChestRewardButton.gameObject.SetActive(true);
            claimLevelsChestRewardButton.gameObject.SetActive(false);

            if (FooterView.isCanSwipe == true)
            {
                footerScreen.SetActive(true);
            }
        }
        private void ClaimStarsChestRewardButtonPressed()
        {
            starsChestScreen.SetActive(false);
            starsChestRewardImage.gameObject.SetActive(false);
            startClaimStarsChestRewardButton.gameObject.SetActive(true);
            claimStarsChestRewardButton.gameObject.SetActive(false);

            if (FooterView.isCanSwipe == true)
            {
                footerScreen.SetActive(true);
            }
        }

        #endregion

    }
}