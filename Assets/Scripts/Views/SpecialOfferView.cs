using Matchmania;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SpecialOfferView : MonoBehaviour
{
    [Header("Special offer screens")]
    [SerializeField] private GameObject specialOfferScreen;
    [SerializeField] private GameObject casualOfferScreen;
    [SerializeField] private GameObject exclusiveOfferScreen;
    
    [Header("Timers")]
    public TextMeshProUGUI[] specialOfferTimerScreenTexts; 
    public TextMeshProUGUI specialOfferTimerMenuText;
    
    [Header("Buttons")]
    [SerializeField] private Button[] specialOfferCloseButtons;
    [SerializeField] private Button specialOfferButton;

    void Start()
    {
        InitializeSpecialOfferButton();
    }
    public void InitializeSpecialOfferButton()
    {
        foreach (Button specialOfferCloseButton in specialOfferCloseButtons)
        {
            specialOfferCloseButton.onClick.AddListener(SpecialOfferController.Instance.HideOfferScreen);
        }
        specialOfferButton.onClick.AddListener(SpecialOfferController.Instance.ShowOfferScreen);
    }

    public void ShowSpecialOfferButtons()
    {
        specialOfferButton.gameObject.SetActive(true);
    }

    public void HideSpecialOfferButtons()
    {
        specialOfferButton.gameObject.SetActive(false);
    }

    public void HideOfferScreen()
    {
        LevelView.CloseScreen(specialOfferScreen);
        LevelView.CloseScreen(casualOfferScreen);
        LevelView.CloseScreen(exclusiveOfferScreen);
    }

    public void ShowOfferScreen(SpecialOfferType specialOfferType)
    {
        switch (specialOfferType)
        {
            case SpecialOfferType.Special:
               LevelView.OpenScreen(specialOfferScreen);
                break;
            case SpecialOfferType.Casual:
                LevelView.OpenScreen(casualOfferScreen);
                break;
            case SpecialOfferType.Exclusive:
                LevelView.OpenScreen(exclusiveOfferScreen);
                break;
        }
    }

    public void UpdateTimersText(string textToShow)
    {
        foreach (TextMeshProUGUI specialOfferTimerScreenText in specialOfferTimerScreenTexts)
        {
            specialOfferTimerScreenText.text = textToShow;
        }
        specialOfferTimerMenuText.text = textToShow;
    }
}
