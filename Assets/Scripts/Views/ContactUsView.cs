using DG.Tweening;
using TMPro;
using UnityEngine;

public class ContactUsView : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputField;
    [SerializeField] private TextMeshProUGUI textWarning, textSuccess;
    [SerializeField] private float textFadeTime = 2f;

    public void ShowTextWarning()
    {
        textWarning.DOFade(1, 0f);
        textWarning.gameObject.SetActive(true);
        textWarning.DOFade(0, textFadeTime).OnComplete(() => textWarning.gameObject.SetActive(false));
    }

    public void ShowTextSuccess()
    {
        _inputField.text = "";
        textWarning.DOFade(1, 0f);
        textSuccess.gameObject.SetActive(true);
        textWarning.DOFade(0, textFadeTime).OnComplete(() => textWarning.gameObject.SetActive(false));
    }
    
    public string GetInputText() => _inputField.text;
}
