using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using TMPro;

namespace Matchmania
{
    public class FooterView : MonoBehaviour
    {
        public enum CurrentScreenType
        {
            home,
            shop,
            leaderboard
        }
        private Vector2 fingerDownPos;
        private Vector2 fingerUpPos;
        private bool detectSwipeAfterRelease = true;
        private float SWIPE_THRESHOLD = 150f;
        public static bool isCanSwipe = true;

        public static CurrentScreenType currentScreenType;

        [SerializeField] private Button shopButton;
        [SerializeField] private Button homeButton;
        [SerializeField] private Button leaderboardButton;

        [SerializeField] private GameObject leaderboardMainMenuButton;

        [SerializeField] private GameObject shopSelection;
        [SerializeField] private GameObject homeSelection;
        [SerializeField] private GameObject leaderboardSelection;

        public GameObject footerView;

        [SerializeField] RectTransform shopScreen;
        [SerializeField] RectTransform mainMenuScreen;
        [SerializeField] RectTransform leaderboardScreen;

        [SerializeField] RectTransform shopIcon;
        [SerializeField] RectTransform mainMenuIcon;
        [SerializeField] RectTransform leaderboardIcon;

        [SerializeField] RectTransform selectionTab;

        [SerializeField] RectTransform shopSelectionRectTransform;
        [SerializeField] RectTransform homeSelectionRectTransform;
        [SerializeField] RectTransform leaderboardSelectionRectTransform;

        [SerializeField] private LeaderBoardView LeaderBoardView;
        [SerializeField] private ChangeNicknameView ChangeNicknameView;

        void Start()
        {
            leaderboardMainMenuButton.SetActive(false);

            shopButton.onClick.AddListener(ShopButtonPressed);
            homeButton.onClick.AddListener(HomeButtonPressed);
            leaderboardButton.onClick.AddListener(LeaderboardButtonPressed);

            shopSelection.SetActive(false);
            homeSelection.SetActive(true);
            leaderboardSelection.SetActive(false);
        }
        // Update is called once per frame
        void Update()
        {
            if (isCanSwipe == true)
            {
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        fingerUpPos = touch.position;
                        fingerDownPos = touch.position;
                    }

                    //Detects Swipe while finger is still moving on screen
                    if (touch.phase == TouchPhase.Moved)
                    {
                        if (!detectSwipeAfterRelease)
                        {
                            fingerDownPos = touch.position;
                            DetectSwipe();
                        }
                    }

                    //Detects swipe after finger is released from screen
                    if (touch.phase == TouchPhase.Ended)
                    {
                        fingerDownPos = touch.position;
                        DetectSwipe();
                    }
                }
            }
        }

        void DetectSwipe()
        {

            if (VerticalMoveValue() > SWIPE_THRESHOLD && VerticalMoveValue() > HorizontalMoveValue())
            {
                //Debug.Log("Vertical Swipe Detected!");
                if (fingerDownPos.y - fingerUpPos.y > 0)
                {
                    OnSwipeUp();
                }
                else if (fingerDownPos.y - fingerUpPos.y < 0)
                {
                    OnSwipeDown();
                }
                fingerUpPos = fingerDownPos;

            }
            else if (HorizontalMoveValue() > SWIPE_THRESHOLD && HorizontalMoveValue() > VerticalMoveValue())
            {
                //Debug.Log("Horizontal Swipe Detected!");
                if (fingerDownPos.x - fingerUpPos.x > 0)
                {

                    OnSwipeRight();
                }
                else if (fingerDownPos.x - fingerUpPos.x < 0)
                {
                    OnSwipeLeft();
                }
                fingerUpPos = fingerDownPos;

            }
            else
            {
                //Debug.Log("No Swipe Detected!");
            }
        }

        public void ShopButtonPressed()
        {
            if (currentScreenType == CurrentScreenType.shop || !isCanSwipe) return;

            isCanSwipe = false;
            Vector3 screenPos = shopScreen.localPosition;
            shopScreen.localScale = Vector3.one;
            screenPos.x = -1500;
            shopScreen.localPosition = screenPos;
            shopScreen.gameObject.SetActive(true);
            var mainMove = shopScreen.DOLocalMoveX(0, 0.15f);
            mainMove.SetEase(Ease.InCubic);

            if (shopScreen.GetComponent<CanvasGroup>())
                shopScreen.GetComponent<CanvasGroup>().alpha = 1;


            if (currentScreenType == CurrentScreenType.home)
            {
              var move =  mainMenuScreen.DOLocalMoveX(1500, 0.15f).OnComplete(() => mainMenuScreen.gameObject.SetActive(false));
              move.SetEase(Ease.InCubic);
            }
            else if(currentScreenType == CurrentScreenType.leaderboard)
            {
              var move =  leaderboardScreen.DOLocalMoveX(1500, 0.15f).OnComplete(() => mainMenuScreen.gameObject.SetActive(false));
                move.SetEase(Ease.InCubic);
            }

            currentScreenType = CurrentScreenType.shop;

            shopSelection.SetActive(true);
            homeSelection.SetActive(false);
            leaderboardSelection.SetActive(false);

            var movement =  selectionTab.DOMoveX(shopSelectionRectTransform.position.x, 0.25f);
            movement.SetEase(Ease.InCubic);

            shopIcon.DOLocalMoveY(75, 0f);
            shopIcon.DOLocalMoveX(-75, 0f);
            shopIcon.DOLocalMoveX(0, 0.25f).OnComplete(() => isCanSwipe = true); ;
            mainMenuIcon.DOLocalMoveY(0, 0);
            leaderboardIcon.DOLocalMoveY(0, 0);

            ChangeNicknameView.CloseButtonPressed();

        }
        public void HomeButtonPressed()
        {
            if (currentScreenType == CurrentScreenType.home || !isCanSwipe) return;

            isCanSwipe = false;
            if (currentScreenType == CurrentScreenType.shop)
            {
                Vector3 screenPos = mainMenuScreen.localPosition;
                mainMenuScreen.localScale = Vector3.one;
                screenPos.x = 1500;
                mainMenuScreen.localPosition = screenPos;
                mainMenuScreen.gameObject.SetActive(true);
               var move1 =  mainMenuScreen.DOLocalMoveX(0, 0.15f);
               var move2 = shopScreen.DOLocalMoveX(-1500, 0.15f).OnComplete(() => shopScreen.gameObject.SetActive(false));

                move1.SetEase(Ease.InCubic);
                move2.SetEase(Ease.InCubic);
            }
            else if (currentScreenType == CurrentScreenType.leaderboard)
            {
                Vector3 screenPos = mainMenuScreen.localPosition;
                mainMenuScreen.localScale = Vector3.one;
                screenPos.x = -1500;
                mainMenuScreen.localPosition = screenPos;
                mainMenuScreen.gameObject.SetActive(true);
               var move1 =  mainMenuScreen.DOLocalMoveX(0, 0.15f);
               var move2 = leaderboardScreen.DOLocalMoveX(1500, 0.15f).OnComplete(() => leaderboardScreen.gameObject.SetActive(false));

                move1.SetEase(Ease.InCubic);
                move2.SetEase(Ease.InCubic);
            }
            currentScreenType = CurrentScreenType.home;
            var movement = selectionTab.DOMoveX(homeSelectionRectTransform.position.x, 0.25f);
            movement.SetEase(Ease.InCubic);
            shopSelection.SetActive(false);
            homeSelection.SetActive(true);
            leaderboardSelection.SetActive(false);

            shopIcon.DOLocalMoveY(0, 0f);
            leaderboardIcon.DOLocalMoveY(0, 0f);

            mainMenuIcon.DOLocalMoveY(75, 0f);
            mainMenuIcon.DOLocalMoveX(-75, 0f);
            mainMenuIcon.DOLocalMoveX(0, 0.25f).OnComplete(() => isCanSwipe = true); ;

            ChangeNicknameView.CloseButtonPressed();
        }
        public void LeaderboardButtonPressed()
        {
            if (currentScreenType == CurrentScreenType.leaderboard || !isCanSwipe) return;

            isCanSwipe = false;
            Vector3 screenPos = leaderboardScreen.localPosition;
            leaderboardScreen.localScale = Vector3.one;
            screenPos.x = 1500;
            leaderboardScreen.localPosition = screenPos;
            leaderboardScreen.gameObject.SetActive(true);
            var mainMove = leaderboardScreen.DOLocalMoveX(0, 0.15f);
            mainMove.SetEase(Ease.InCubic);


            if (currentScreenType == CurrentScreenType.home)
            {
               var move = mainMenuScreen.DOLocalMoveX(-1500, 0.15f).OnComplete(() => mainMenuScreen.gameObject.SetActive(false));
                move.SetEase(Ease.InCubic);
            }
            else if (currentScreenType == CurrentScreenType.shop)
            {
                var move = shopScreen.DOLocalMoveX(-1500, 0.15f).OnComplete(() => mainMenuScreen.gameObject.SetActive(false));
                move.SetEase(Ease.InCubic);
            }
         
            currentScreenType = CurrentScreenType.leaderboard;
            selectionTab.DOMoveX(leaderboardSelectionRectTransform.position.x, 0.25f);
            shopSelection.SetActive(false);
            homeSelection.SetActive(false);
            leaderboardSelection.SetActive(true);

            shopIcon.DOLocalMoveY(0, 0f);
            mainMenuIcon.DOLocalMoveY(0, 0);
            leaderboardIcon.DOLocalMoveY(75, 0f);
            leaderboardIcon.DOLocalMoveX(-75, 0f);
            leaderboardIcon.DOLocalMoveX(0, 0.25f).OnComplete(() =>isCanSwipe = true);

            LeaderBoardView.WorldSortButtonPressed();
        }

        #region Swipe
        float VerticalMoveValue()
        {
            return Mathf.Abs(fingerDownPos.y - fingerUpPos.y);
        }

        float HorizontalMoveValue()
        {
            return Mathf.Abs(fingerDownPos.x - fingerUpPos.x);
        }

        void OnSwipeUp()
        {
            //Do something when swiped up
        }

        void OnSwipeDown()
        {
            //Do something when swiped down
        }

        void OnSwipeLeft()
        {
            if (currentScreenType == CurrentScreenType.home)
            {
                LeaderboardButtonPressed();
            }
            else if (currentScreenType == CurrentScreenType.shop)
            {
                HomeButtonPressed();
            }

        }

        void OnSwipeRight()
        {
            if (currentScreenType == CurrentScreenType.home)
            {
                ShopButtonPressed();
            }
            else if (currentScreenType == CurrentScreenType.leaderboard)
            {
                HomeButtonPressed();
            }

        }
        #endregion

    }


}
