﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace Matchmania
{
    public class GameModel
    {
        private static string levelsConfigURL = "https://script.google.com/macros/s/AKfycbyktNX2LLfKqsauPoN7BIkAkUctBdlfbWkgQp9OORxmbmk4m1k/exec";
        private static string itemsProgressionURL = "https://script.google.com/macros/s/AKfycbxBMxB42FlTti9UHmt0vJI8ZR4CgXEnZo0fCaIlps0KQ-O8FP8/exec";

        public const int itemMatchCount = 3;

        public static LevelsConfig Levels;
        public static ItemsProgressionConfig Items;

        public static int loadingIndex = 0;

        public static void LoadlevelConfig(MonoBehaviour component)
        {
            component.StartCoroutine(DownloadLevelsConfig());
        }
        public static void LoadItemsProgressionConfig(MonoBehaviour component)
        {
            component.StartCoroutine(DownloadItemsProgressionConfig());
        }
        private static IEnumerator DownloadLevelsConfig()
        {
            //UnityWebRequest www = UnityWebRequest.Get(levelsConfigURL);
            //yield return www.SendWebRequest();

            TextAsset textAsset = Resources.Load<TextAsset>("LevelsConfig");
            string json = textAsset.text;

            //if (www.isNetworkError || www.isHttpError)
            //{
            //    Debug.Log(www.error);
            //}
            //else
            //{
            //    json = www.downloadHandler.text;
            //}
            Levels = JsonUtility.FromJson<LevelsConfig>(json);

            loadingIndex++;
            yield return null;
        }
        private static IEnumerator DownloadItemsProgressionConfig()
        {
            //UnityWebRequest www = UnityWebRequest.Get(itemsProgressionURL);
            //yield return www.SendWebRequest();

            TextAsset textAsset = Resources.Load<TextAsset>("ItemsProgressionConfig");
            string json = textAsset.text;

            //if (www.isNetworkError || www.isHttpError)
            //{
            //    Debug.Log(www.error);
            //}
            //else
            //{
            //    json = www.downloadHandler.text;
            //}
            Items = JsonUtility.FromJson<ItemsProgressionConfig>(json);

            foreach (var item in Items.Items)
            {
                item.CheckItems();
            }
            loadingIndex++;
            yield return null;
        }
    }
    [System.Serializable]
    public class LevelsConfig
    {
      public  LevelModel[] Levels;
    }
    [System.Serializable]
    public class ItemsProgressionConfig
    {
        public ItemsProgressionModel[] Items;
    }
}