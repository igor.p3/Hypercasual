using HyperCasualTemplate;
using System;
using System.Collections;
using UnityEngine;

namespace Matchmania
{
    public class BannerController : MonoBehaviour
    {

        public static void CheckBanner()
        {
           //LevelController.Instance.StartCoroutine(_CheckBanner());
        }
       private static IEnumerator _CheckBanner()
        {
            yield return new WaitForSeconds(2f);

            var startLevel = Convert.ToInt32(FirebaseInitializer.defaults["StartLevel"]);
            var levelCooldown = Convert.ToInt32(FirebaseInitializer.defaults["LevelCooldown"]);

            if (LevelController.CurrentLevelIndex % levelCooldown == 0 && LevelController.CurrentLevelIndex >= startLevel)
            {
                AdvertisementSystem.ShowBanner();
                AdvertisementSystem.isBannerActive = true;
                Debug.Log("BannerShouldBeLoaded");
            }
        }
    }
}
