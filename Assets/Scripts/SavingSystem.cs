﻿using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace HyperCasualTemplate
{
    public static class SavingSystem<T> where T : class
    {
        private static string savePath = Path.Combine(Application.persistentDataPath, "SaveData");
        public static void SaveJsonGameData(T gameData)
        {
            string jsonData = JsonUtility.ToJson(gameData);
            File.WriteAllText(savePath, jsonData);
        }
        public static T LoadJsonGameData()
        {
            if (File.Exists(savePath))
            {
                string jsonData = File.ReadAllText(savePath);
                T gameData = JsonUtility.FromJson<T>(jsonData);
                return gameData;
            }
            else
            { 
                Debug.Log("No save data to load");
                return null;
            }
        }
        public static void SaveXMLGameData(T gameData)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                serializer.Serialize(stream, gameData);
            }
        }
        public static T LoadXMLGameData()
        {
            var serializer = new XmlSerializer(typeof(T));
            if (File.Exists(savePath))
            {
                using (var stream = new FileStream(savePath, FileMode.Open))
                {
                    return serializer.Deserialize(stream) as T;
                }
            }
            else
            {
                Debug.LogError("No save data to load");
                return null;
            }
        }
        public static void SaveBinaryGameData(T gameData)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(savePath, FileMode.OpenOrCreate);
            bf.Serialize(file, gameData);
        }
        public static T LoadBinaryGameData()
        {
            if (File.Exists(savePath))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream stream = new FileStream(savePath, FileMode.Open);
                T saveData = formatter.Deserialize(stream) as T;
                stream.Close();
                return saveData;
            }
            else
            {
                Debug.LogError("No save data to load");
                return null;
            }
        }
    }
}