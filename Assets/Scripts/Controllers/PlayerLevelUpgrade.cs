using UnityEngine;
using System;

namespace Matchmania
{
    public class PlayerLevelUpgrade : MonoBehaviour
    {
        public static Action OnLevelUpgradeEventHandler;
        public static Action OnUpdateEventHandler;

        public static int plyerLevel;
        public static void UpgradeLevel()
        {
           if(CheckLevelUpgrade() == true)
            {
                GetPlayerLevel();
                OnLevelUpgradeEventHandler?.Invoke();
            }
        }
        public static int GetPlayerLevel()
        {
            plyerLevel = CurrencyController.Stars / 500;

            return plyerLevel + 1;
        }
        public static bool  CheckLevelUpgrade()
        {
            bool isCanUpgrade = false;

            if (CurrencyController.StarsChestCount >= 500)
            {
                isCanUpgrade = true;
            }

            return isCanUpgrade;
        }
    }
}