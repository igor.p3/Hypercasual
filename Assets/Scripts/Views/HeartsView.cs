using System;
using System.Collections;
using System.Collections.Generic;
using HyperCasualTemplate;
using Matchmania;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HeartsView : MonoBehaviour
{
    [Header("Screens")]
    [SerializeField] private GameObject outOfHeartsDialogue;
    [SerializeField] private GameObject heartsDialogue;
    [SerializeField] private GameObject noAvailableVideo;
    [SerializeField] private GameObject loadingScreen;
    
    [SerializeField] private SFXConfig SFXConfig;
    
    [Header("Buttons")]
    [SerializeField] private Button heartAdButton, heartCoinButton;
    
    [Header("Text")]
    [SerializeField] private TextMeshProUGUI mmHeartsAmount, heartsDialogueHeartsAmount;
    [SerializeField] private TextMeshProUGUI[] timerTexts;

    [Header("Other")] [SerializeField] private GameObject unlimitedHeartImage;

    private void Start()
    {
        heartAdButton.onClick.AddListener(AddHeartsAdButton);
        heartCoinButton.onClick.AddListener(AddHeartsCoinsButton);
    }

    public void ShowOutOfHearts()
    {
        outOfHeartsDialogue.SetActive(true);
    }

    public void ShowHeartsDialogue()
    {
        if(HeartsController.Instance.CurrentHeartsAmount == HeartsController.Instance.maxHeartsAmount) return;
        if (HeartsController.isUnlimited) return;
        heartsDialogue.SetActive(true);
    }

    public void UpdateTimersText(string textToShow)
    {
        foreach (TextMeshProUGUI timer in timerTexts)
        {
            timer.text = textToShow;
        }
    }

    public void UpdateHeartsAmounts(int value)
    {
        mmHeartsAmount.text = value.ToString();
        heartsDialogueHeartsAmount.text = value.ToString();
    }

    public void ShowUnlimitedIcon()
    {
        mmHeartsAmount.text = "";
        unlimitedHeartImage.SetActive(true);
        
    }

    public void HideUnlimitedIcon(int valueOfCurrentHearts)
    {
        UpdateHeartsAmounts(valueOfCurrentHearts);
        unlimitedHeartImage.SetActive(false);
    }

    private void AddHeartsAdButton()
    {
        bool available = AdvertisementSystem.IsRewardedAdReady();

        if (available)
        {
            loadingScreen.SetActive(true);
            AdvertisementSystem.ShowRewardedAd();
            AudioController.Instance.TurnOnMusicAL(false);
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);

            AdvertisementSystem.OnEndRewardVideo += AddHeartsMethod;
        }
        else
        {
            noAvailableVideo.SetActive(true);
        }
    }

    private void AddHeartsMethod()
    {
        outOfHeartsDialogue.SetActive(false);
        heartsDialogue.SetActive(false);
        HeartsController.Instance.CurrentHeartsAmount++;
    }

    private void AddHeartsCoinsButton()
    {
        if(HeartsController.Instance.AddHeartsForCoins()) outOfHeartsDialogue.SetActive(false);
    }
}
