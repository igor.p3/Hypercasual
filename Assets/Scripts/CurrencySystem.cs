﻿namespace HyperCasualTemplate
{
    public static class CurrencySystem
    {
        public static int SoftCurrencyAmount { get; private set; }
        public static int HardCurrencyAmount { get; private set; }

        public static void ChangeCurrencyAmount(CurrencyType currencyType, int count, bool isAdd)
        {
            if (currencyType == CurrencyType.SoftCurreny)
            {
                if (isAdd == true)
                {
                    SoftCurrencyAmount += count;
                }
                else SoftCurrencyAmount -= count;
            }
            else if (currencyType == CurrencyType.HardCurrency)
            {
                if (isAdd == true)
                {
                    HardCurrencyAmount += count;
                }
                else HardCurrencyAmount -= count;
            }
        }
    }
    public enum CurrencyType
    {
        SoftCurreny,
        HardCurrency
    }
}