﻿using UnityEngine;

namespace Matchmania
{
    [CreateAssetMenu(fileName = "VFX", menuName = "Matchmania/VFX", order = 1)]
    public class VFXConfig : ScriptableObject
    {
        public GameObject matchEffect;
        public ParticleSystem itemTrailEffect;
        public ParticleSystem starGlow;
        public GameObject star;
        public GameObject starFromTimer;

        public ParticleSystem PlayEffect(Vector3 position, ParticleSystem particleSystem)
        {
            var effect = Instantiate(particleSystem, position, Quaternion.identity);
            effect.Play();
            return effect;
        }
        public GameObject PlayEffect(Vector3 position, GameObject effectGameObject, Vector3 rotation)
        {
            var effect = Instantiate(effectGameObject, position, Quaternion.Euler(rotation));
            return effect;
        }
        public ParticleSystem  PlayEffect(Vector3 position, ParticleSystem particleSystem, Transform parent)
        {
            var effect = Instantiate(particleSystem, position, Quaternion.identity);
            effect.Play();
            effect.transform.parent = parent;
            effect.transform.localPosition = Vector3.zero;
            return effect;
        }
    }
}