﻿namespace Matchmania
{
    public enum ItemType
    {
        Acorn,
        Airplane,
        Ant,
        Apple,
        Backpack,
        Bag,
        Ball,
        Balloon,
        Banana,
        Basket
    }
}