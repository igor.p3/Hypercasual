﻿using System.Collections.Generic;
using UnityEngine;
using System;
using HyperCasualTemplate;

namespace Matchmania
{
    public class HintController : MonoBehaviour
    {
        #region Singleton
        private static HintController _instance;
        public static HintController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<HintController>();
                }

                return _instance;
            }
        }
        #endregion

        public Action HintUsedEventHandler;

        [SerializeField] private VFXConfig VFXConfig;
        [SerializeField] private SFXConfig SFXConfig;
        [SerializeField] private Transform levelCenter;

        [SerializeField] GameObject hintAdIcon;
        [SerializeField] GameObject hintIcon;

        [SerializeField] GameObject fanAdIcon;
        [SerializeField] GameObject fanIcon;

        private bool isCanUseHint = true;

        private float timeToAdd = 60;

        private void Start()
        {
           
        }
        public void CheckBoostersIcons()
        {
            //if (CurrencyController.Hints <= 0)
            //{
            //    //hintAdIcon.SetActive(true);
            //    //hintIcon.SetActive(false);
            //}
            //else
            //{
            //    //hintAdIcon.SetActive(false);
            //    //hintIcon.SetActive(true);
            //}
            //if (CurrencyController.Fans <= 0)
            //{
            //    //fanAdIcon.SetActive(true);
            //    //fanIcon.SetActive(false);
            //}
            //else
            //{
            //    //fanAdIcon.SetActive(false);
            //    //fanIcon.SetActive(true);
            //}

            HintUsedEventHandler?.Invoke();
        }
        private void HintLogic()
        {
            isCanUseHint = false;
            ItemsSpawnController.Instance.StartCoroutine(LevelController.Instance.SetCanMakeTurn(0.3f));
            LevelController.Instance.CurrentLevelItems.RemoveAll(x => x == null);

            ItemController randomItem = null;

            int rnd = UnityEngine.Random.Range(0, LevelController.Instance.CurrentLevelItems.Count - 1);

            int counter = 0;

            for (int i = LevelController.Instance.slots.Length -1; i >= 0; i--)
            {
                if(LevelController.Instance.slots[i].ItemController != null)
                {
                    randomItem = LevelController.Instance.slots[i].ItemController;
                    counter++;
                
                    break;
                }
            }
            if(counter == 0)
            {
                randomItem = LevelController.Instance.CurrentLevelItems[rnd];
            }

            List<ItemController> hintedItems = new List<ItemController>();

            if (LevelController.Instance.CurrentLevelItems.Count > 0)
            {
                
            }
            else
            {
                isCanUseHint = true;
                return;
            }
            for (int i = 0; i < LevelController.Instance.CurrentLevelItems.Count; i++)
            {
                if (LevelController.Instance.CurrentLevelItems[i] != null && randomItem != null)
                {
                    if (LevelController.Instance.CurrentLevelItems[i].name == randomItem.name)
                    {
                        hintedItems.Add(LevelController.Instance.CurrentLevelItems[i]);
                    }
                }
            }
            foreach (var item in hintedItems)
            {
                StartCoroutine(item.MoveToTarget(item.transform.position + Vector3.up * 2, () =>
                {
                    LevelController.Instance.MoveItemsPositions();
                    StartCoroutine(item.MoveToTarget(levelCenter.position, () =>
                    {
                        //LevelController.Instance.MoveItemsPositions();
                        VFXConfig.PlayEffect(levelCenter.position, VFXConfig.matchEffect, new Vector3(90, 0,0));
                        Destroy(item.gameObject);
                        LevelController.Instance.CurrentLevelItems.RemoveAll(x => x == null);
                        LevelController.Instance.CurrentLevelItems.Remove(item);
                        LevelController.Instance.CheckTurnResult();
                        
                        hintedItems.Clear();
                        isCanUseHint = true;
         
                    }));
                }));
            }
        }

        public void ContiniueLevelAfterDefeat()
        {
            if (!LevelController.Instance.isOutOfBox)
            {
                LevelController.Timer += timeToAdd;
            }
      
            LevelController.Instance.IsCanMakeTurn = true;
            LevelView.isPlayingTimerSound = false;
            BackItemsToPlayfiled();
        }

        public void BackItemsToPlayfiled()
        {
            foreach (var slot in LevelController.Instance.slots)
            {
                if (slot.ItemController != null)
                {
                    ItemController item = slot.ItemController;

                    StartCoroutine(item.MoveToTarget(Vector3.zero + Vector3.up * UnityEngine.Random.Range(1, 7), () =>
                    {
                        item.isRotate = false;
                        item.rigidbody.isKinematic = false;
                        item.collider.enabled = true;
                        item.isMovingToBar = false;
                        item.isOutlining = false;

                    }));
                    slot.ItemController = null;

                    StartCoroutine(item.ScaleItem(item.normalScale));
                }
            }
        }
        public void UseHint()
        {
            if (CurrencyController.Hints <= 0)
            {
                if (AdvertisementSystem.IsRewardedAdReady())
                {
                    AdvertisementSystem.ShowRewardedAd();
                    CurrencyController.Hints++;
                    return;
                }
            }
            if (isCanUseHint == true && CurrencyController.Hints > 0)
            {
                HintLogic();
                CurrencyController.Hints--;
                GameController.SaveGameData();
                HintUsedEventHandler?.Invoke();
                SFXConfig.PlaySoundEffect(SFXConfig.matchItems);
            }
           
            HintController.Instance.CheckBoostersIcons();
        }
    }
}