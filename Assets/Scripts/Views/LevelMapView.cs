using UnityEngine;
using UnityEngine.UI;

namespace Matchmania
{
    public class LevelMapView : MonoBehaviour
    {

        [SerializeField] private LevelsMapPartView[] mapParts;
        public MainMenuView menuView;
        public Button levelsChestButton;

        private static bool isTookReward;

        [SerializeField] private ScrollRect myScrollRect;

        private void Start()
        {
            menuView = FindObjectOfType<MainMenuView>();
            SetMapPartsView();

            myScrollRect.verticalNormalizedPosition = 0f;

            if (LevelController.CurrentLevelIndex > 0 && (LevelController.CurrentLevelIndex + 1) % 10 == 0)
            {
                if (!isTookReward)
                {
                    menuView.LevelsChestRewardButtonPressed();
                    isTookReward = true;
                }
            }
            else
            {
                isTookReward = false;
            }
        }
        private void SetMapPartsView()
        {
            for (int i = 0; i < mapParts.Length; i++)
            {
                if (LevelController.CurrentLevelIndex == 0)
                {
                    mapParts[i].SetPathView((LevelController.CurrentLevelIndex + i), false);
                }
                else if (LevelController.CurrentLevelIndex == 1)
                {
                    if (i == 0)
                    {
                        mapParts[i].SetPathView((LevelController.CurrentLevelIndex - 1), true);
                    }
                    else
                    {
                        mapParts[i].SetPathView((LevelController.CurrentLevelIndex + i) - 1, false);
                    }
                }
                else if (LevelController.CurrentLevelIndex == 2)
                {
                    if (i < 2)
                    {
                        mapParts[0].SetPathView((LevelController.CurrentLevelIndex - 2), true);
                        mapParts[1].SetPathView((LevelController.CurrentLevelIndex - 1), true);
                    }
                    else
                    {
                        mapParts[i].SetPathView((LevelController.CurrentLevelIndex + i) - 2, false);
                    }
                }
                else if (LevelController.CurrentLevelIndex > 2)
                {
                    if (i < 3)
                    {
                        mapParts[i].SetPathView((LevelController.CurrentLevelIndex - 2) + i, true);
                    }
                    else
                    {
                        mapParts[i].SetPathView((LevelController.CurrentLevelIndex + i) - 2, false);
                    }
                }
            }
        }
    }
}