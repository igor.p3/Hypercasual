﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;
using HyperCasualTemplate;
using System.Collections;
using DG.Tweening;
using Dreamteck.Splines;
using MoreMountains.NiceVibrations;
using com.adjust.sdk;
using Lean.Pool;

namespace Matchmania
{
    public class LevelView : MonoBehaviour
    {
        #region Singleton
        private static LevelView _instance;
        public static LevelView Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<LevelView>();
                }

                return _instance;
            }
        }
        #endregion

        public static event Action GoHomeEventHandler;
        public static event Action UpdateLevelIndexTexts;

        [SerializeField] private VFXConfig VFXConfig;
        [SerializeField] private Transform starEffectParent;
        [SerializeField] private SplineComputer starEffectSpline;
        [SerializeField] public MainMenuView mainMenuView;
        [SerializeField] public FooterView FooterView;


        [SerializeField] private Transform obstacleParent;
        [SerializeField] private Transform[] tutorialItemsPositions;
        [SerializeField] private GameObject slotsParent;
        [SerializeField] private GameObject mainMenuScreen;
        [SerializeField] private GameObject levelScreen;
        [SerializeField] private GameObject shopScreen;
        [SerializeField] private GameObject OnPurchaseFailScreen;
        [SerializeField] private Transform itemsParent;
        [SerializeField] private GameObject hintsButtons;
        [SerializeField] private SFXConfig SFXConfig;
        [SerializeField] private GameObject loadingScreen;
        [SerializeField] private Slider comboBonusSlider;
        [SerializeField] private Image timerClockImage;
        [SerializeField] private ParticleSystem confeti;

        public Transform itemStartPosition;

        [Header("Texts")]
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private TextMeshProUGUI levelStarText;
        [SerializeField] private TextMeshProUGUI levelIndexText;
        [SerializeField] private TextMeshProUGUI finishLevelStarsCountText;
        [SerializeField] private TextMeshProUGUI finishLevelIndexText;
        [SerializeField] private TextMeshProUGUI hintsCountText;
        [SerializeField] private TextMeshProUGUI fansCountText;
        [SerializeField] private TextMeshProUGUI afterFinishLevelStartCountText;
        [SerializeField] private TextMeshProUGUI comboBonusSliderText;
        [SerializeField] private TextMeshProUGUI levelChestSliderText;
        [SerializeField] private TextMeshProUGUI starChestSliderText;
        [SerializeField] private TextMeshProUGUI[] coinHoldersTexts;

        [Header("Sliders")]

        [SerializeField] private Slider levelChestSlider;
        [SerializeField] private Slider starChestSlider;

        [Header("Screens")]
        public GameObject victoryScreen;
        public GameObject mainMenuHeader;
        [SerializeField] private GameObject afterVictoryScreen;
        [SerializeField] private GameObject defeatScreenOOF;
        [SerializeField] private GameObject defeatScreenTU;
        [SerializeField] private GameObject afterDefeatScreen;
        [SerializeField] private GameObject noAvailableVideo;
        //[SerializeField] private GameObject levelsMap;
        //[SerializeField] private GameObject levelsMapPrefab;
        [SerializeField] private GameObject fanShop;
        [SerializeField] private GameObject hintShop;

        [Header("Defeat Screen")]
        [SerializeField] private TextMeshProUGUI reviveCoinsCountText;
        [SerializeField] private TextMeshProUGUI reviveCoinsTimeOutCountText;
        [SerializeField] private TextMeshProUGUI reviveCostText;
        [SerializeField] private TextMeshProUGUI reviveCostTimeOutText;



        [Header("Buttons")]
        [SerializeField] private Button nextLevelButton;
        [SerializeField] private Button restartLevelButton;
        [SerializeField] private Button pauseHomeButton;
        [SerializeField] private Button defeatHomeButton;
        [SerializeField] private Button hintButton;
        [SerializeField] private Button fanButton;
        [SerializeField] private Button closeDefeatScreenButton;
        [SerializeField] private Button closeTUDefeatScreenButton;
        [SerializeField] private Button closeOOBDefeatScreenButton;
        [SerializeField] private Button finishLevelClaimRewardButton;
        [SerializeField] private Button finishLevelClaimBonusRewardButton;
        [SerializeField] private Button pauseButton;
        [SerializeField] private Button shopOpenButton;
        [SerializeField] private Button shopCloseButton;
        [SerializeField] private Button continiueLevelHardCurrencyButtonTIO;
        [SerializeField] private Button continiueLevelHardCurrencyButtonOOB;
        [SerializeField] private Button continiueLevelAddButtonOOB;
        [SerializeField] private Button starChestButton;
        [SerializeField] private Button levelChestButton;

        private IncrementalReviveCostController _incrementalReviveCostController;

        public static bool isPlayingTimerSound;
        private bool isPlayingVictory;
        private bool isDefeat;
        [SerializeField] private Obstacle rotationBeam;
        private Obstacle currentObstacle;

        private void Start()
        {
            LevelController.Instance.StartLevelEventHandler += StartLevel;
            LevelController.Instance.DefeatEventHandler += OnDefeat;
            LevelController.Instance.VictoryEventHandler += OnVictory;
            LevelController.Instance.MarchEventHandler += UpdateLevelStars;
            HintController.Instance.HintUsedEventHandler += UpdateHintsCount;
            FanController.Instance.FanUsedEventHandler += UpdateHintsCount;
            IAPController.OnPurchaseEventHandler += CloaseLoadingScreen;
            AdvertisementSystem.OnFinishVideo += CloaseLoadingScreen;
            //PlayerLevelUpgrade.OnLevelUpgradeEventHandler += OpenPlayerUpgradeScreen;
            IAPController.OnPurchaseEventHandler += UpdateReviveCoinsCount;

            hintButton.onClick.AddListener(HintButtonPressed);
            fanButton.onClick.AddListener(FanButtonPressed);
            nextLevelButton.onClick.AddListener(NextLevelButtonPressed);
            restartLevelButton.onClick.AddListener(RestartLevelButtonPressed);
            finishLevelClaimRewardButton.onClick.AddListener(OnFinishLevelClaimRewardButtonPressed);
            finishLevelClaimBonusRewardButton.onClick.AddListener(OnFinishLevelClaimBonusRewardButtonPressed);
            defeatHomeButton.onClick.AddListener(OnHomeButtonPressed);
            //closeDefeatScreenButton.onClick.AddListener(CloseDefeatScreenButtonPressed);
            pauseButton.onClick.AddListener(PauseButtonPressed);
            shopCloseButton.onClick.AddListener(CloseShop);
            closeTUDefeatScreenButton.onClick.AddListener(CloseDefeatScreenButtonPressed);
            closeOOBDefeatScreenButton.onClick.AddListener(CloseDefeatScreenButtonPressed);
            //starChestButton.onClick.AddListener(mainMenuView.StarsChestRewardButtonPressed);
            //levelChestButton.onClick.AddListener(mainMenuView.LevelsChestRewardButtonPressed);
            //starChestButton.onClick.AddListener(UpdateFinishLevelChestsView);
            //levelChestButton.onClick.AddListener(UpdateFinishLevelChestsView);
            continiueLevelHardCurrencyButtonTIO.onClick.AddListener(OnContiniueLevelHardCurrencyButtonPressed);
            continiueLevelHardCurrencyButtonOOB.onClick.AddListener(OnContiniueLevelHardCurrencyButtonPressed);
            continiueLevelAddButtonOOB.onClick.AddListener(OnContiniueLevelAddButtonPressed);
            shopOpenButton.onClick.AddListener(OpenShop);

            starChestButton.onClick.AddListener(mainMenuView.OpenStarsChestRewardScreen);
            levelChestButton.onClick.AddListener(mainMenuView.OpenLevelsChestRewardScreen);
            IAPController.OnPurchaseEventHandler += UpdateAllCoinsHolderAmounts;


            UpdateAllCoinsHolderAmounts();
            //Topor
            _incrementalReviveCostController = new IncrementalReviveCostController();
            //End of topor

            UpdateRewardedChestsView();
        }
        //private void OpenPlayerUpgradeScreen()
        //{
        //    mainMenuView.StarsChestRewardButtonPressed();
        //}
        private void Update()
        {
            if (LevelController.Timer > 0)
            {
                DisplayTime(LevelController.Timer);
            }
            if (LevelController.Timer < 10 && LevelController.Timer > 0)
            {
                if (LevelController.Instance.IsPlayingLevel && isPlayingTimerSound == false)
                {
                    SFXConfig.PlaySoundEffect(SFXConfig.timerRemaining);
                    isPlayingTimerSound = true;
                }
            }

            comboBonusSlider.value = LevelController.Instance.comboBonusTimer;
            comboBonusSlider.maxValue = LevelController.Instance.comboBonusTimerMax;
            comboBonusSliderText.text = "x" + LevelController.Instance.comboBonusIndex;

        }
        public void OpenShop()
        {
            //if (LevelController.Instance.isReviveScreenOpened == false)
            //{
                OpenScreen(shopScreen);
                mainMenuView.UpdateMainMenuView();
                if (FooterView.isCanSwipe == true)
                {
                    FooterView.ShopButtonPressed();
                }
            //}
        }
        private void CloaseLoadingScreen()
        {
            CloseScreen(loadingScreen);
        }
        public void OnPurchaseFail()
        {
            OnPurchaseFailScreen.SetActive(true);
            AdvertisementSystem.isCanShowInactiveAdd = true;
            CloaseLoadingScreen();
        }

        private void CloseShop()
        {
            CloseScreen(shopScreen);
            if (FooterView.isCanSwipe == true)
            {
                FooterView.HomeButtonPressed();
            }

            shopScreen.SetActive(false);
        }
        private void PauseButtonPressed()
        {
            PauseController.PauseGame(true);
        }
        private void DisplayTime(float timeToDisplay)
        {
            float minutes = Mathf.FloorToInt(timeToDisplay / 60);
            float seconds = Mathf.FloorToInt(timeToDisplay % 60);
            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }
        public void StartLevel()
        {
            slotsParent.SetActive(true);
            //TTEventController.Instance.HideTheBar();
            //if(levelsMap != null)
            //{
            //    Destroy(levelsMap);
            //}
            //StartCoroutine(LevelController.Instance.SetCanMakeTurn(2));

            FooterView.isCanSwipe = false;

            if (LevelController.CurrentLevelIndex > 0)
            {
                ItemsSpawnController.Instance.SpawnItems(itemsParent, false, tutorialItemsPositions);
            }
            else ItemsSpawnController.Instance.SpawnItems(itemsParent, true, tutorialItemsPositions);

            LevelController.Timer = LevelController.Instance.CurrentLevelModel.timer;

            isPlayingTimerSound = false;
            LevelController.Instance.CurrentLevelStars = 0;
            levelStarText.text = LevelController.Instance.CurrentLevelStars.ToString();
            levelIndexText.text = "Level " + (LevelController.CurrentLevelIndex + 1).ToString();
            UpdateHintsCount();
            hintsButtons.SetActive(true);
            mainMenuHeader.SetActive(false);
            LeaveConfirmationView.Instance.HideConfirmation();
            _incrementalReviveCostController.ResetPrice();

            // Temporary
            if (LevelController.Instance.CurrentLevelModel.blocker_01 != "none")
            {
                if (currentObstacle == null)
                {
                    currentObstacle = LeanPool.Spawn(rotationBeam);
                    currentObstacle.transform.parent = obstacleParent;
                    currentObstacle.SetPosition();
                }
            }
            else
            {
                LeanPool.Despawn(currentObstacle);
            }
            // end temprorary
        }
        private void OnVictory()
        {
            confeti.Play();
            SFXConfig.PlayVictorySFX(this);
            AudioController.Instance.PlayMusic(false);

            if (SettingsController.isVibrationOn == true)
                MMVibrationManager.Haptic(HapticTypes.Success, false, true, this);

            if (!isPlayingVictory)
                StartCoroutine(TurnOnVIctoryScreen());

        }
        IEnumerator TurnOnVIctoryScreen()
        {
            isPlayingVictory = true;
            UpdateAllCoinsHolderAmounts();
            RectTransform targetPos = GameObject.FindGameObjectWithTag("Star").GetComponent<RectTransform>();
            Vector3 splinePointPos = targetPos.position;
            splinePointPos.z = starEffectSpline.GetPointPosition(0).z;
            starEffectSpline.SetPointPosition(2, splinePointPos);

            while (LevelController.Timer >= 0)
            {
                int timer = Mathf.RoundToInt(LevelController.Timer);

                if (timer % 10 == 0)
                {
                    LevelController.Instance.CurrentLevelStars++;
                    UpdateLevelStars();

                    GameObject starEffect = VFXConfig.PlayEffect(Vector3.zero, VFXConfig.starFromTimer, Vector3.zero);

                    starEffect.GetComponent<SplineFollower>().spline = starEffectSpline;

                    RectTransform rect = starEffect.GetComponent<RectTransform>();
                    rect.transform.SetParent(starEffectParent);
                    rect.transform.localScale = Vector3.one;

                    rect.DOScale(2, 0.1f);
                    yield return new WaitForSeconds(0.1f);
                    rect.DOScale(1, 1f);
                }
                yield return null;
            }

            yield return new WaitUntil(() => LevelController.Timer <= 0);
            OpenScreen(afterVictoryScreen);


            CurrencyController.Stars += LevelController.Instance.CurrentLevelStars;
            CurrencyController.StarsChestCount += LevelController.Instance.CurrentLevelStars;
            CurrencyController.LevelsChestCount++;



            confeti.Stop();
            afterFinishLevelStartCountText.text = LevelController.Instance.CurrentLevelStars.ToString();
            levelStarText.text = LevelController.Instance.CurrentLevelStars.ToString();
            hintsButtons.SetActive(false);

            yield return new WaitForSeconds(1f);
            finishLevelClaimRewardButton.gameObject.SetActive(true);
            finishLevelClaimRewardButton.GetComponent<CanvasGroup>().alpha = 0;
            finishLevelClaimRewardButton.GetComponent<CanvasGroup>().DOFade(1f, 1f);

            if (LevelController.CurrentLevelIndex == 10)
            {
                AnalyticsEvents.ExecuteAdjustEvent("ift5y8");
            }
            else if (LevelController.CurrentLevelIndex == 50)
            {
                AnalyticsEvents.ExecuteAdjustEvent("pm8y3v");
            }
            else if (LevelController.CurrentLevelIndex == 100)
            {
                AnalyticsEvents.ExecuteAdjustEvent("lamjue");
            }
            isPlayingVictory = false;
            LevelController.CurrentLevelIndex++;
            GameController.SaveGameData();

        }

        public void UpdateRewardedChestsView()
        {
            levelChestSlider.value = CurrencyController.LevelsChestCount;
            starChestSlider.value = CurrencyController.StarsChestCount;
            UpdateFinishLevelChestsView();
        }
        private void UpdateFinishLevelChestsView()
        {


            if (CurrencyController.LevelsChestCount >= levelChestSlider.maxValue)
            {
                levelChestSliderText.text = "Tap to Claim";
            }
            else levelChestSliderText.text = Mathf.RoundToInt(levelChestSlider.value).ToString() + "/" + levelChestSlider.maxValue.ToString();

            if (CurrencyController.StarsChestCount >= starChestSlider.maxValue)
            {
                starChestSliderText.text = "Tap to Claim";
            }
            else starChestSliderText.text = Mathf.RoundToInt(starChestSlider.value).ToString() + "/" + starChestSlider.maxValue.ToString();
        }
        private void UpdateReviveCoinsCount()
        {
            reviveCoinsCountText.text = CurrencyController.Coins.ToString();
            reviveCoinsTimeOutCountText.text = CurrencyController.Coins.ToString();
        }
        IEnumerator _OnDefeat(bool isOutOfBox)
        {
            isDefeat = true;
            UpdateReviveCoinsCount();
            reviveCostText.text = _incrementalReviveCostController.CurrentPrice.ToString();
            reviveCostTimeOutText.text = _incrementalReviveCostController.CurrentPrice.ToString();

            if (isOutOfBox)
            {
                OpenScreen(defeatScreenOOF);
            }
            else
            {
                OpenScreen(defeatScreenTU);
            }
            UpdateAllCoinsHolderAmounts();
            LevelController.Instance.isOutOfBox = isOutOfBox;
            mainMenuHeader.SetActive(true);
            SFXConfig.PlaySoundEffect(SFXConfig.defeat);
            AudioController.Instance.PlayMusic(false);
            LevelController.Instance.IsPlayingLevel = false;

            if (SettingsController.isVibrationOn == true)
                MMVibrationManager.Haptic(HapticTypes.Failure, false, true, this);
            hintsButtons.SetActive(false);

            AnalyticsEvents.ExecuteEvent("Level_Failed_" + LevelController.CurrentLevelIndex);

            yield return new WaitForSeconds(1f);
            isDefeat = false;
        }
        private void OnDefeat(bool isOutOfBox)
        {
            if (!isDefeat)
                StartCoroutine(_OnDefeat(isOutOfBox));

            LevelController.Instance.isReviveScreenOpened = true;
            UpdateLevelIndexTexts?.Invoke();
        }
        public void RestartLevelButtonPressed()
        {
            //if (HeartsController.Instance.HeartsCheck() == false) return;

            //HeartsController.Instance.CurrentHeartsAmount--;


            var startLevel = Convert.ToInt32(FirebaseInitializer.defaults["StartLevel"]);
            var levelCooldown = Convert.ToInt32(FirebaseInitializer.defaults["LevelCooldown"]);

            if (LevelController.CurrentLevelIndex % levelCooldown == 0 && LevelController.CurrentLevelIndex >= startLevel)
            {
                if (AdvertisementSystem.IsInterstitialdAdReady() && !AdvertisementSystem.isAdsRemoved && AdvertisementSystem.isCanShowInterstital)
                {
                    //if (LevelController.isCanShowInterstitialAfterHomeAndRestartButtons)
                    //{
                    AdvertisementSystem.ShowInterstitial();
                    LevelController.isRestartLevelAfterInterstitional = true;
                    //}
                    /* else*/ /*LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);*/
                }
                else
                {
                    LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
                }
            }
            else
            {
                LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
            }
            CloseScreen(victoryScreen);
            CloseScreen(defeatScreenTU);
            CloseScreen(defeatScreenOOF);
            CloseScreen(afterDefeatScreen);
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            mainMenuHeader.SetActive(false);
        }
        private void HintButtonPressed()
        {
            if (CurrencyController.Hints <= 0)
            {
                OpenHintBoosterShop();
                return;
            }
            HintController.Instance.UseHint();
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
        }
        public void NextLevelButtonPressed()
        {
            victoryScreen.SetActive(false);
            afterVictoryScreen.SetActive(false);
            defeatScreenOOF.SetActive(false);
            defeatScreenTU.SetActive(false);
            slotsParent.gameObject.SetActive(false);
            hintsButtons.SetActive(false);

            LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
            mainMenuScreen.SetActive(false);
            levelScreen.SetActive(true);
            mainMenuHeader.SetActive(false);

            PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
        }
        private void UpdateLevelStars()
        {
            levelStarText.text = LevelController.Instance.CurrentLevelStars.ToString();
            SFXConfig.PlaySoundEffect(SFXConfig.matchItems);
        }
        private void OnFinishLevelClaimRewardButtonPressed()
        {
            CloseScreen(afterVictoryScreen);
            OpenScreen(victoryScreen);
            mainMenuHeader.SetActive(true);

            FinishLevel();

            float time = 1f;

            float levelChestValue = levelChestSlider.value + 1;
            float starChestValue = starChestSlider.value + LevelController.Instance.CurrentLevelStars;

            DOTween.To(() => levelChestSlider.value, x => levelChestSlider.value = x, levelChestValue, time).OnUpdate(UpdateFinishLevelChestsView).OnComplete(UpdateRewardedChestsView);
            DOTween.To(() => starChestSlider.value, x => starChestSlider.value = x, starChestValue, time).OnUpdate(UpdateFinishLevelChestsView).OnComplete(UpdateRewardedChestsView);

            var startLevel = Convert.ToInt32(FirebaseInitializer.defaults["StartLevel"]);
            var levelCooldown = Convert.ToInt32(FirebaseInitializer.defaults["LevelCooldown"]);

            if (LevelController.CurrentLevelIndex % 11 == 0 && LevelController.CurrentLevelIndex != 0)
            {
                RateUsView.Instance.ShowRateUsScreen();
            }
            else if (LevelController.CurrentLevelIndex >= startLevel && LevelController.CurrentLevelIndex % levelCooldown == 0)
            {
                if (AdvertisementSystem.IsInterstitialdAdReady())
                {
                    AdvertisementSystem.ShowInterstitial();
                    if (AdvertisementSystem.isBannerActive == false)
                    {
                        AdvertisementSystem.ShowBanner();
                    }
                }
            }
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            finishLevelClaimRewardButton.gameObject.SetActive(false);
        }
        private void OnFinishLevelClaimBonusRewardButtonPressed()
        {
            bool available = AdvertisementSystem.IsRewardedAdReady();

            if (available)
            {
                OpenScreen(loadingScreen);
                AdvertisementSystem.ShowRewardedAd();

                CloseScreen(afterVictoryScreen);
                OpenScreen(victoryScreen);
                mainMenuHeader.SetActive(true);
                PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
                CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
                LevelController.Instance.CurrentLevelStars *= 2;
                FinishLevel();

                GameController.SaveGameData();
                SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);

                finishLevelClaimRewardButton.gameObject.SetActive(false);
            }
            else
            {
                OpenScreen(noAvailableVideo);
            }
        }
        private void FinishLevel()
        {
            //finishLevelIndexText.text = "Level " + (LevelController.CurrentLevelIndex + 1).ToString();
            finishLevelStarsCountText.text = LevelController.Instance.CurrentLevelStars.ToString();
            //TTEventController.Instance.HandleMaxAmountOfCombo(LevelController.Instance.maxCombo);
            //TTEventController.Instance.ShowEndLevelBar();
            //TTEventController.Instance.UpdateOpenedMainMenu();
            TournamentsController.AddToPlayerScores(LevelController.Instance.CurrentLevelStars, 1);
            if (LevelController.CurrentLevelIndex >= GameModel.Levels.Levels.Length)
            {
                LevelController.CurrentLevelIndex = GameModel.Levels.Levels.Length - 1;
            }
            GameController.SaveGameData();

            PlayerLevelUpgrade.UpgradeLevel();
        }
        public void GoToMainMenu()
        {
            PlayerLevelUpgrade.OnUpdateEventHandler?.Invoke();
            //TTEventController.Instance.ShowTheBar();
            mainMenuScreen.SetActive(true);
            LevelController.Instance.ClearLevel();
            victoryScreen.SetActive(false);
            afterVictoryScreen.SetActive(false);
            afterDefeatScreen.SetActive(false);
            defeatScreenTU.SetActive(false);
            defeatScreenOOF.SetActive(false);
            slotsParent.gameObject.SetActive(false);
            hintsButtons.SetActive(false);
            mainMenuScreen.SetActive(true);
            mainMenuHeader.SetActive(true);
            levelScreen.SetActive(false);
            GoHomeEventHandler?.Invoke();
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            FooterView.isCanSwipe = true;
            LevelController.Instance.isReviveScreenOpened = false;
            mainMenuView.footerScreen.SetActive(true);
            LevelController.Instance.IsPlayingLevel = false;

            AdvertisementSystem.HideBanner();
        }
        public void OnHomeButtonPressed()
        {
            if (AdvertisementSystem.IsInterstitialdAdReady())
            {
                //if (LevelController.isCanShowInterstitialAfterHomeAndRestartButtons)
                //{
                AdvertisementSystem.ShowInterstitial();
                LevelController.floatinterstitialAfterHomeAndRestartButtonTimer = LevelController.interstitialAfterHomeAndRestartButtonsCooldown;
                //}
            }
            GoToMainMenu();
            //HeartsController.Instance.CurrentHeartsAmount--;
        }
        private void CloseDefeatScreenButtonPressed()
        {
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            if (LeaveConfirmationView.Instance.ShowConfirmation() == false) return;
            defeatScreenOOF.SetActive(false);
            defeatScreenTU.SetActive(false);
            afterDefeatScreen.SetActive(true);
            afterDefeatScreen.transform.localScale = Vector3.one;
            afterDefeatScreen.transform.GetChild(0).GetComponent<Image>().DOFade(0.9f, 0);
        }
        private void OnContiniueLevelHardCurrencyButtonPressed()
        {
            if (CurrencyController.Coins >= _incrementalReviveCostController.CurrentPrice)
            {
                HintController.Instance.ContiniueLevelAfterDefeat();
                CloseScreen(defeatScreenOOF);
                CloseScreen(defeatScreenTU);
                SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
                AudioController.Instance.TurnOnMusicAL(true);
                CurrencyController.Coins -= _incrementalReviveCostController.CurrentPrice;
                hintsButtons.SetActive(true);
                LevelController.Instance.IsPlayingLevel = true;
                LeaveConfirmationView.Instance.HideConfirmation();
                _incrementalReviveCostController.IncreasePrice();
                mainMenuHeader.SetActive(false);
                GoHomeEventHandler?.Invoke();
                LevelController.Instance.isReviveScreenOpened = false;
            }
            else
            {
                OpenScreen(shopScreen);
                shopScreen.GetComponent<RectTransform>().localPosition = new Vector3(0, shopScreen.GetComponent<RectTransform>().localPosition.y, 0);
                mainMenuView.UpdateMainMenuView();
            }
        }
        private void OnContiniueLevelAddButtonPressed()
        {
            bool available = AdvertisementSystem.IsRewardedAdReady();

            if (available)
            {
                loadingScreen.SetActive(true);
                LevelController.Instance.IsPlayingLevel = false;
                AdvertisementSystem.ShowRewardedAd();
                AudioController.Instance.TurnOnMusicAL(false);
                SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
                mainMenuHeader.SetActive(false);

                AdvertisementSystem.OnEndRewardVideo += Revive;
                LevelController.Instance.isReviveScreenOpened = false;
            }
            else
            {
                noAvailableVideo.SetActive(true);
            }
        }
        public void Revive()
        {
            HintController.Instance.ContiniueLevelAfterDefeat();
            defeatScreenOOF.SetActive(false);
            defeatScreenTU.SetActive(false);
            hintsButtons.SetActive(true);
            LeaveConfirmationView.Instance.HideConfirmation();
            UpdateAllCoinsHolderAmounts();
        }
        private void UpdateHintsCount()
        {
            hintsCountText.text = CurrencyController.Hints.ToString();
            fansCountText.text = CurrencyController.Fans.ToString();

            if (CurrencyController.Hints <= 0)
            {
                hintsCountText.text = "+";
            }
            else
            {
                hintsCountText.text = CurrencyController.Hints.ToString();
            }
            if (CurrencyController.Fans <= 0)
            {
                fansCountText.text = "+";
            }
            else
            {
                fansCountText.text = CurrencyController.Fans.ToString();
            }
        }
        public void StartPurchusing()
        {
            AdvertisementSystem.isCanShowInactiveAdd = false;
        }

        private void OpenFanBoosterShop()
        {
            OpenScreen(fanShop);
            UpdateAllCoinsHolderAmounts();
            LevelController.Instance.IsCanMakeTurn = false;
            LevelController.Instance.IsPlayingLevel = false;
        }

        private void OpenHintBoosterShop()
        {
            OpenScreen(hintShop);
            UpdateAllCoinsHolderAmounts();
            LevelController.Instance.IsCanMakeTurn = false;
            LevelController.Instance.IsPlayingLevel = false;
        }

        public void UpdateAllCoinsHolderAmounts()
        {
            foreach (var coinHolderText in coinHoldersTexts)
            {
                coinHolderText.text = CurrencyController.Coins.ToString();
            }
        }
        public void SetCanMakeTurn()
        {
            StartCoroutine(_SetCanMakeTurn());
        }
        IEnumerator _SetCanMakeTurn()
        {
            yield return new WaitForSeconds(.1f);
            LevelController.Instance.IsCanMakeTurn = true;
            LevelController.Instance.IsPlayingLevel = true;
        }
        public void BuyFanFromShop()
        {
            //if (LevelController.Instance.IsPlayingLevel == false) return;

            if (CurrencyController.Coins >= 90)
            {
                CurrencyController.Coins -= 90;
                CurrencyController.Fans += 3;
                UpdateHintsCount();
                CloseScreen(fanShop);
                HintController.Instance.CheckBoostersIcons();
                SetCanMakeTurn();
                mainMenuView.UpdateMainMenuView();

            }
            else
            {
                OpenShop();
            }

        }

        public void BuyHintFromShop()
        {
            //if (LevelController.Instance.IsPlayingLevel == false) return;

            if (CurrencyController.Coins >= 90)
            {
                CurrencyController.Coins -= 90;
                CurrencyController.Hints += 3;
                UpdateHintsCount();
                CloseScreen(hintShop);
                HintController.Instance.CheckBoostersIcons();
                mainMenuView.UpdateMainMenuView();
                SetCanMakeTurn();
            }
            else
            {
                OpenShop();
            }

        }
        private void FanButtonPressed()
        {
            if (CurrencyController.Fans <= 0)
            {
                OpenFanBoosterShop();
                return;
            }
            FanController.Instance.UseFan();
        }
        public static void OpenScreen(GameObject screen, float targetFade = 0.9f)
        {
            screen.SetActive(true);

            Image imageBG = screen.transform.GetChild(0).GetComponent<Image>();
            imageBG.DOFade(0f, 0f);

            screen.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

            if (!screen.GetComponent<CanvasGroup>())
            {
                screen.AddComponent<CanvasGroup>();
            }

            screen.GetComponent<CanvasGroup>().alpha = 0;
            screen.GetComponent<CanvasGroup>().DOFade(1, 0.5f);
            var ease = screen.transform.DOScale(1, .25f);
            ease.SetEase(Ease.InCubic);
            ease.onComplete += () => imageBG.DOFade(targetFade, 0.25f);


        }
        public static void CloseScreen(GameObject screen)
        {
            Image imageBG = screen.transform.GetChild(0).GetComponent<Image>();
            imageBG.transform.SetParent(screen.transform.parent);
            imageBG.transform.localScale = Vector3.one * 75;

            var easeFade = imageBG.DOFade(0f, 0.25f);

            easeFade.onComplete += LocalMethod();


            TweenCallback LocalMethod()
            {
                screen.GetComponent<CanvasGroup>().DOFade(0, 0.5f);
                var ease = screen.transform.DOScale(0.01f, .25f);
                ease.SetEase(Ease.InCubic);
                ease.onComplete += () => screen.SetActive(false);

                imageBG.DOFade(1f, 0f);
                imageBG.transform.SetParent(screen.transform);
                imageBG.transform.SetAsFirstSibling();

                TweenCallback callback = null;
                return callback;
            }
        }
    }
}