using System;
using System.Collections;
using System.Collections.Generic;
using Matchmania;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TournamentsView : MonoBehaviour
{
    [Header("Buttons")] 
    [SerializeField] private Button button_menu_Levels;
    [SerializeField] private Button button_menu_Stars;

    [Header("Timers")] 
    [SerializeField] private TextMeshProUGUI[] textTourTimers;

    [Header("Place in text")] 
    [SerializeField] private TextMeshProUGUI[] textsPlaceTaken;
    
    [Header("Screens")] 
    [SerializeField] private GameObject leaderboardScreen;
    [SerializeField] private GameObject enterTournamentScreen;
    [SerializeField] private GameObject tournamentCompleteScreen;

    [Header("End tournament screen")] 
    [SerializeField] private TextMeshProUGUI endTextPlaceReached;
    [SerializeField] private TextMeshProUGUI endTextEndName;
    [SerializeField] private TextMeshProUGUI endTextAmountOfRewardEarned;
    [SerializeField] private Image endRewardImage;
    [SerializeField] private Transform endLeaderBoardParent;
    [SerializeField] private TournamentLeaderboardPanel endPlayerLeaderboardPanel;
    [SerializeField] private Button getButton, xButton;

    [Header("Entry tour screen")] 
    [SerializeField] private TextMeshProUGUI textEntry;
    [SerializeField] private TextMeshProUGUI textEntryTip;
    [SerializeField] private Image bannerStartImage;

    [Header("TextsToShow")] 
    [SerializeField] private string levelTournamentEntryText = "Level Tournament";
    [SerializeField] private string starTournamentEntryText = "Star Tournament";
    [SerializeField] private string levelTournamentTipText = "Complete levels and get amazing rewards!";
    [SerializeField] private string starTournamentTipText = "Collect stars and get amazing rewards!";

    [Header("Images")] 
    [SerializeField] private Sprite star;
    [SerializeField] private Sprite cup;

    [Header("Leaderboard panel")] 
    [SerializeField] private GameObject panelPrefab;
    [SerializeField] private TextMeshProUGUI textLeaderboardTitle;
    [SerializeField] private Transform leaderboardParent;
    [SerializeField] private LeaderBoardView _leaderBoardView;
    [SerializeField] private MainMenuView _mainMenuView;

    // Start is called before the first frame update
    void Start()
    {
        button_menu_Levels.gameObject.SetActive(false);
        button_menu_Stars.gameObject.SetActive(false);
        button_menu_Levels.onClick.AddListener(ShowLeaderboardsScreen);
        button_menu_Stars.onClick.AddListener(ShowLeaderboardsScreen);
        UpdateTakenPlaces();
        getButton.onClick.AddListener(GiveRewards);
        xButton.onClick.AddListener(GiveRewards);
        TournamentsController.OnTakenPlaceChanged += UpdateTakenPlaces;
    }

    private void GiveRewards()
    {
        TournamentsController.GiveRewardsForPlace();
        tournamentCompleteScreen.SetActive(false);
    }

    #region Menu Buttons

    public void ShowButtonLevels() => button_menu_Levels.gameObject.SetActive(true);

    public void ShowButtonStars() => button_menu_Stars.gameObject.SetActive(true);

    public void HideButtonLevels() => button_menu_Levels.gameObject.SetActive(false);

    public void HideButtonStars() => button_menu_Stars.gameObject.SetActive(false);

    #endregion

    public void UpdateTimers(string textToShow)
    {
        foreach (var textLevelTimer in textTourTimers) textLevelTimer.text = textToShow;
    }

    public void UpdateTakenPlaces()
    {
        foreach (var textMeshProUGUI in textsPlaceTaken)
            textMeshProUGUI.text = $"#{TournamentsController.savedTakenPlace.ToString()}";
    }

    public void ShowTournamentEntryScreen(TournamentType tournamentType)
    {
        switch (tournamentType)
        {
            case TournamentType.Level:
                textEntry.text = levelTournamentEntryText;
                textEntryTip.text = levelTournamentTipText;
                bannerStartImage.sprite = cup;
                break;
            case TournamentType.Star:
                textEntry.text = starTournamentEntryText;
                textEntryTip.text = starTournamentTipText;
                bannerStartImage.sprite = star;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(tournamentType), tournamentType, null);
        }

        enterTournamentScreen.SetActive(true);
    }

    public void ShowTournamentEndedScreen(TournamentType tournamentType, int reachedPlaceNum)
    {
        // Show text according to tournament type
        switch (tournamentType)
        {
            case TournamentType.Level:
                endTextEndName.text = levelTournamentEntryText;
                break;
            case TournamentType.Star:
                endTextEndName.text = starTournamentEntryText;
                break;
        }
        
        // End rank
        endTextPlaceReached.text = (reachedPlaceNum).ToString();

        // Show the players leaderboard style
        TournamentsController.GetCurrentLeaderBoards();
        StartCoroutine(WaitAndShowLeaderboards(TournamentsController.tournamentServerData.tournamentType, endLeaderBoardParent));
        
        // Show the player
        var scoresGained = TournamentsController.savedTournamentProgression.collectedStars;
        Sprite scoresImage = star;
        if (TournamentsController.tournamentServerData.tournamentType == TournamentType.Level)
        {
            scoresGained = TournamentsController.savedTournamentProgression.passedLevel;
            scoresImage = cup;
        }

        // Loot related
        Sprite rewardImage = null;
        TTEventDataModel.Loot lootForThisPlace = default;
        if (TournamentsController.TournamentModel.LootTable.ContainsKey(TournamentsController.savedTakenPlace))
        {
            lootForThisPlace = TournamentsController.TournamentModel.LootTable[TournamentsController.savedTakenPlace];
            switch (lootForThisPlace.lootType)
            {
                case TTEventDataModel.LootType.Hint:
                    rewardImage = _mainMenuView.hintSprite;
                    break;
                case TTEventDataModel.LootType.Coins:
                    rewardImage = _mainMenuView.coinsSprite;
                    break;
            }
            
            endTextAmountOfRewardEarned.text = lootForThisPlace.amountOfLoot.ToString();
            var imageForReward = lootForThisPlace.lootType switch
            {
                TTEventDataModel.LootType.Hint => _mainMenuView.hintSprite,
                TTEventDataModel.LootType.Coins => _mainMenuView.coinsSprite,
                _ => null
            };
            endRewardImage.sprite = imageForReward;
        }
        var amountOfLoot = lootForThisPlace.amountOfLoot > 0? lootForThisPlace.amountOfLoot : 0;
        
        endPlayerLeaderboardPanel.SetView(GameController.playerName, _leaderBoardView.GetFlag(TournamentsController.savedTournamentProgression.country), 
            TournamentsController.savedTakenPlace, (int)scoresGained, scoresImage, rewardImage,amountOfLoot,true);
        tournamentCompleteScreen.SetActive(true);
    }

    public void ShowLeaderboardsScreen()
    {
        StartCoroutine(TournamentsController.GetCurrentLeaderBoards());
        StartCoroutine(WaitAndShowLeaderboards(TournamentsController.tournamentServerData.tournamentType, leaderboardParent));
        
    }

    private IEnumerator WaitAndShowLeaderboards(TournamentType tournamentType,
        Transform panelParent)
    {
        yield return new WaitUntil(() => TournamentsController._tourLeaderboardPlayers.Count >= 1);
        ChangeNicknameView.Instance.ShowNicknameChangeScreen();

        switch (tournamentType)
        {
            case TournamentType.Level:
                textLeaderboardTitle.text = levelTournamentEntryText;
                break;
            case TournamentType.Star:
                textLeaderboardTitle.text = starTournamentEntryText;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(tournamentType), tournamentType, null);
        }
        ClearLeaderboardView();
        for (var index = 0; index < TournamentsController._tourLeaderboardPlayers.Count; index++)
        {
            var player = TournamentsController._tourLeaderboardPlayers[index];
            var panelSpawned = Instantiate(panelPrefab, panelParent, true);
            panelSpawned.transform.localPosition = Vector3.zero;
            panelSpawned.transform.localScale = Vector3.one;
            Sprite imageForReward = null;
            var lootForThisPlace = TournamentsController.TournamentModel.LootTable[index];
            switch (lootForThisPlace.lootType)
            {
                case TTEventDataModel.LootType.Hint:
                    imageForReward = _mainMenuView.hintSprite;
                    break;
                case TTEventDataModel.LootType.Coins:
                    imageForReward = _mainMenuView.coinsSprite;
                    break;
            }

            bool isGolden = player.playerName == GameController.playerName;

            var scoresGained = tournamentType == TournamentType.Level? player.passedLevel : player.collectedStars;
            var scoreImage = tournamentType == TournamentType.Level ? cup : star; 
            panelSpawned.GetComponent<TournamentLeaderboardPanel>()
                .SetView(player.playerName, _leaderBoardView.GetFlag(player.country), index + 1, (int) scoresGained,
                    scoreImage, imageForReward, lootForThisPlace.amountOfLoot, isGolden);
        }

        leaderboardScreen.SetActive(true);
    }

    private void ClearLeaderboardView()
    {
        for (int i = 0; i < leaderboardParent.childCount; i++)
        {
            Destroy(leaderboardParent.GetChild(i).gameObject); 
        } 
    }
}