using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

namespace Matchmania
{
    public class VictoryScreenView : MonoBehaviour
    {
        [SerializeField] private NewOpenedItemsView newOpenedItemsView;
        [SerializeField] private LevelView levelView;

        [SerializeField] private Slider itemsProgressionSlider;
        [SerializeField] private Slider fullScreenItemsProgressionSlider;
        [SerializeField] private TextMeshProUGUI itemsProgressionText;
        [SerializeField] private TextMeshProUGUI fullScreenItemsProgressionText;

        [SerializeField] private GameObject itemsProgressionTab;
        [SerializeField] private GameObject fullScreenProgressionTab;
        [SerializeField] private RectTransform texts;

        [SerializeField] private GameObject oneButtonTab;
        [SerializeField] private GameObject twoButtonsTab;

        [SerializeField] private Button nextLevelButton;

        void OnEnable()
        {
            CheckView();

            nextLevelButton.onClick.AddListener(levelView.NextLevelButtonPressed);
        }
        private void CheckView()
        {
            ItemsProgressionModel nextProgression = null;
            ItemsProgressionModel currentProgression = null;

            CheckButtons(); 

            foreach (var item in GameModel.Items.Items)
            {
                if (item.levelComplete >= LevelController.CurrentLevelIndex)
                {
                    nextProgression = item;
                    break;
                }
                else
                {
                    currentProgression = item;
                }
            }
            if (nextProgression != null)
            {
                texts.localPosition = new Vector3(0, 276, 0);
                if (LevelController.CurrentLevelIndex == 1)
                {
                    itemsProgressionSlider.DOValue(20, 1f).OnUpdate(UpdateText);

                    return;
                }
        
                float levelDifference = nextProgression.levelComplete - currentProgression.levelComplete;

                float currentLevelDifference = nextProgression.levelComplete - LevelController.CurrentLevelIndex;

                float percents = ((currentLevelDifference ) / levelDifference) * 100;

                percents = (-100 + percents) * -1;

                if(currentLevelDifference == 0)
                {
                    LevelView.OpenScreen(fullScreenProgressionTab, 1f);
                    fullScreenItemsProgressionSlider.value = 90;
                    UpdateText();
                    fullScreenItemsProgressionSlider.DOValue(100, 1.5f).OnComplete(OpenProgressionItemsScreen).OnUpdate(UpdateText);
                    itemsProgressionTab.SetActive(false);
                    texts.localPosition = new Vector3(0, 500, 0);
                    return;
                }
                itemsProgressionTab.SetActive(true);
                UpdateText();
                itemsProgressionSlider.DOValue(percents, 1f).OnComplete(OpenProgressionItemsScreen).OnUpdate(UpdateText);
            }
            else
            {
                itemsProgressionTab.SetActive(false);
            }
        }

        private void UpdateText()
        {
            itemsProgressionText.text = Mathf.RoundToInt(itemsProgressionSlider.value).ToString() + "%" ;
            fullScreenItemsProgressionText.text = Mathf.RoundToInt(fullScreenItemsProgressionSlider.value).ToString() + "%";
        }
        private void OpenProgressionItemsScreen()
        {
            if(itemsProgressionSlider.value == 100 || fullScreenItemsProgressionSlider.value == 100)
            {
                itemsProgressionSlider.value = 0;
                fullScreenItemsProgressionSlider.value = 0;
                UpdateText();
                LevelView.OpenScreen(newOpenedItemsView.openedItemsScreen, 1f);
                newOpenedItemsView.SetItemsView();

                LevelView.CloseScreen(fullScreenProgressionTab);
            }

          
        }
        private void CheckButtons()
        {
            if (CurrencyController.LevelsChestCount >= 10 || CurrencyController.StarsChestCount >= 500)
            {
                oneButtonTab.SetActive(true);
                twoButtonsTab.SetActive(false);
            }
            else
            {
                oneButtonTab.SetActive(false);
                twoButtonsTab.SetActive(true);
            }
        }
    }

    
}