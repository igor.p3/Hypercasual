using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Firebase.Extensions;
using Firebase.Firestore;
using Matchmania;
using UnityEngine;
using System.Threading.Tasks;

public class TournamentsController : MonoBehaviour
{
    // Serialized fields
    [SerializeField] private TournamentsView _tournamentsView;
    [SerializeField] private int _levelToReach = 10;

    // Saved values
    public static string savedJoinedRoomReference = "";
    public static TourLeaderboardPlayer savedTournamentProgression = new TourLeaderboardPlayer();
    public static string savedTournamentId;
    public static int savedTakenPlace = 11;
    public static event Action OnTakenPlaceChanged;

    public static List<TourLeaderboardPlayer> _tourLeaderboardPlayers = new List<TourLeaderboardPlayer>();
    public static TournamentServerData tournamentServerData = null;

    private bool _isNoInternet;
    private bool isDataLoaded = false;
    private static int timeForNewEvents = 172800;
    private static float amountOfCDSeconds = 100;
    private static Timer internalCDTimer;

    // Helper classes
    public static TournamentModel TournamentModel = new TournamentModel();
    private static TournamentRoomHandler _tournamentRoomHandler;

    private void Start()
    {
        if (DatabaseModel.FirestoreDatabase == null || InternetCheckerController.isNoInternetConnection)
        {
            Debug.LogError($"{name} - There is no connection to database!");
            _isNoInternet = true;
            _tournamentsView.HideButtonLevels();
            _tournamentsView.HideButtonStars();
            return;
        }
        
        CheckLevelReached();
        LevelController.OnLevelNumChanged += (i) => CheckLevelReached();
        ChangeNicknameView.Instance.OnNicknameChanged += RejoinRoomWithNewNickname;
        InternetCheckerController.OnNoInternet += () =>
        {
            _tournamentsView.HideButtonLevels();
            _tournamentsView.HideButtonStars();
        };

    }

    private void RejoinRoomWithNewNickname(string oldName)
    {
        if(_isNoInternet) return;
        if (LevelController.CurrentLevelIndex + 1 < _levelToReach) return;

        // Rejoin the player in the DB
        _tournamentRoomHandler.RejoinPlayerWithNewNickname(oldName);
        // Change the nickname in local saves
        savedTournamentProgression.playerName = GameController.playerName;
        // Remove it from local List of players for the loading of the correct data
        var oldPlayerInLeaderBoards = _tourLeaderboardPlayers.FirstOrDefault((player) => player.playerName == oldName);
        _tourLeaderboardPlayers.Remove(oldPlayerInLeaderBoards);
        // Update the local view
        _tournamentsView.ShowLeaderboardsScreen();
        GameController.SaveGameData();
    }

    private void CheckLevelReached()
    {
        if (_isNoInternet) return;
        if (isDataLoaded) return;

        if (LevelController.CurrentLevelIndex + 1 < _levelToReach) return;

        InitGetTourInfo();
        StartCoroutine(WaitUntilTourDataIsReceived());
    }

    private void InitGetTourInfo()
    {
        TournamentModel.tourInfoDocument =
            DatabaseModel.FirestoreDatabase.Collection("tournaments").Document("currentTourInfo");
        TournamentModel.RoomCollectionReference = DatabaseModel.FirestoreDatabase.Collection("tournaments")
            .Document("tourRooms").Collection("roomsInternational");
        _tournamentRoomHandler = new TournamentRoomHandler(TournamentModel.RoomCollectionReference, savedJoinedRoomReference);
        Debug.LogWarning("Started loading the data about current tournament");        
        // Get the values about the current tour 
        TournamentModel.tourInfoDocument.GetSnapshotAsync().ContinueWith((Task<DocumentSnapshot> task) =>
        {
            if (task.IsFaulted)
            {
                Debug.LogError("Data was not retrieved");
            }

            else if (task.IsCompleted)
            {
                DocumentSnapshot documentSnapshot = task.Result;
                try
                {
                    tournamentServerData = documentSnapshot.ConvertTo<TournamentServerData>();
                }
                catch (Exception e)
                {
                    Debug.LogWarning(e);
                    throw;
                }
            }
            Debug.LogWarning($"Data was received, there is a tournament with id of {tournamentServerData.tournamentId}");
        });
        
        
    }

    private IEnumerator WaitUntilTourDataIsReceived()
    {
        yield return new WaitUntil(() => tournamentServerData != null);

        // Fill the real time timer
        DateTime dateStarted = tournamentServerData.timeStarted.ToDateTime();
        TournamentModel.localTimer = new RealTimeTimer(dateStarted, tournamentServerData.timeForEvent);
        isDataLoaded = true;

        var isSavedIdEmpty = savedTournamentId == null || string.IsNullOrEmpty(savedTournamentId) || savedTournamentId.Equals("");
        var isJoinedRoomReferenceEmpty = 
            savedJoinedRoomReference == null || string.IsNullOrEmpty(savedJoinedRoomReference) || savedJoinedRoomReference.Equals("");

        if (isSavedIdEmpty || isJoinedRoomReferenceEmpty)
        {
            // User never started tournament before - start it
            _tournamentRoomHandler.JoinFreeRoom(new TourLeaderboardPlayer());
            StartNewLocalTournament();
            
            GameController.SaveGameData();
            yield break;
        }

        if (TournamentModel.localTimer.IsDone)
        {
            // Tournament has ended but haven't updated the currentTourInfo in DB
            _tournamentsView.ShowTournamentEndedScreen(tournamentServerData.tournamentType, savedTakenPlace);
            
            UpdateServerCurrentTourInfo();
            // Closes current room and joins a new room
            _tournamentRoomHandler.CloseCurrentRoomJoinNewFree();
            StartNewLocalTournament();
        }

        if (savedTournamentId != tournamentServerData.tournamentId)
        {
            // Means last tournament has ended and the new started on the server
            _tournamentsView.ShowTournamentEndedScreen(tournamentServerData.tournamentType, savedTakenPlace);

            // Start the new Local tournament
            _tournamentRoomHandler.CloseCurrentRoomJoinNewFree();
            StartNewLocalTournament();
            
            GameController.SaveGameData();
            yield break;
        }
        

        StartLocalTournament();
    }

/*#if UNITY_EDITOR || DEBUG
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            // Ends tournament by making timer go isDone;
            
            // Tournament has ended but haven't updated the currentTourInfo in DB
            _tournamentsView.ShowTournamentEndedScreen(tournamentServerData.tournamentType, savedTakenPlace);
            
            UpdateServerCurrentTourInfo();
            // Closes current room and joins a new room
            _tournamentRoomHandler.CloseCurrentRoomJoinNewFree();
            StartNewLocalTournament();
        }

        if (Input.GetKeyDown(KeyCode.J))
        {
            // Ends tournament by making the current tournament info be different from one on server
            savedTournamentId = "wrong_id";
            StartCoroutine(WaitUntilTourDataIsReceived());
        }
    }
#endif*/

    #region Leaderboard

    public static IEnumerator GetCurrentLeaderBoards()
    {
        if (internalCDTimer.IsDone == false)
        {
            Debug.LogError("The CD timer hasn't finished yet");
            LeaderboardsRearrangeLocally();
            yield break;
        }

        internalCDTimer = new Timer(amountOfCDSeconds);
        
        yield return new WaitUntil(() => tournamentServerData != null);
        _tournamentRoomHandler.GetCurrentRoomLeaderboard(tournamentServerData.tournamentType);
    }

    public static void HandleData(Task<QuerySnapshot> task)
    {
        if (task.IsFaulted)
        {
            Debug.LogError("Data was not retrieved");
        }

        else if (task.IsCompleted)
        {
            Debug.LogWarning($"Data of the size {task.Result.Count} was received");
            _tourLeaderboardPlayers.Clear();
            QuerySnapshot querySnapshot = task.Result;
            foreach (DocumentSnapshot documentSnapshot in querySnapshot)
            {
                TourLeaderboardPlayer tourLeaderboardConverted = documentSnapshot.ConvertTo<TourLeaderboardPlayer>();
                _tourLeaderboardPlayers.Add(tourLeaderboardConverted);
            }
        }
        Debug.LogWarning("The leaderboard list was filled successfully");
        var currentPlayerInfo =
            _tourLeaderboardPlayers.FirstOrDefault((playerInfo) => playerInfo.playerName.Equals(GameController.playerName));
        if (currentPlayerInfo == null) return;
        savedTournamentProgression ??= currentPlayerInfo;
        savedTakenPlace = _tourLeaderboardPlayers.IndexOf(currentPlayerInfo) + 1;
        OnTakenPlaceChanged?.Invoke();
        GameController.SaveGameData();
    }

    #endregion

    #region Updating player scores

    public static void ChangePlayerScores(int collectedStars, int levelsPassed)
    {
        savedTournamentProgression ??= new TourLeaderboardPlayer
        {
            collectedStars = collectedStars, passedLevel = levelsPassed
        };
        GameController.SaveGameData();
        _tournamentRoomHandler.UpdatePlayerInfoInCurrentRoom(savedTournamentProgression);
    }

    private static void LeaderboardsRearrangeLocally()
    {
        if (_tourLeaderboardPlayers.Count == 0)
        {
            Debug.LogError("Didn't fetch any leaderboards yet");
            return;
        }

        var playerIndex = -1;
        var playerReferenceInList = _tourLeaderboardPlayers.FirstOrDefault((player) => player.playerName == GameController.playerName);
        
        if(playerReferenceInList != null)
            playerIndex = _tourLeaderboardPlayers.IndexOf(playerReferenceInList);

        if (playerIndex == -1)
        {
            Debug.LogError("Somehow the player is not in the list in leaderboards list");
            // Add the player in the leaderboards list
            _tourLeaderboardPlayers.Add(savedTournamentProgression);
        }
        else
        {
            _tourLeaderboardPlayers[playerIndex] = savedTournamentProgression;
        }
        
        switch (tournamentServerData.tournamentType)
        {
            case TournamentType.Level:
                _tourLeaderboardPlayers = _tourLeaderboardPlayers.OrderByDescending(player => player.passedLevel).ToList();

                break;
            case TournamentType.Star:
                _tourLeaderboardPlayers =
                    _tourLeaderboardPlayers.OrderByDescending(player => player.collectedStars).ToList();
                break;
        }
    }

    public static void AddToPlayerScores(int collectedStarsToAdd, int levelsToAdd)
    {
        savedTournamentProgression ??= new TourLeaderboardPlayer();
        savedTournamentProgression.collectedStars += collectedStarsToAdd;
        savedTournamentProgression.passedLevel += levelsToAdd;
        savedTournamentProgression.country = RegionIdentifier.Instance.GetCurrentRegion().DisplayName;
        GameController.SaveGameData();
        if(_tournamentRoomHandler != null) _tournamentRoomHandler.UpdatePlayerInfoInCurrentRoom(savedTournamentProgression);
    }

    #endregion

    public static void GiveRewardsForPlace()
    {
        if (TournamentModel.LootTable.ContainsKey(savedTakenPlace) == false) return;

        var lootGathered = TournamentModel.LootTable[savedTakenPlace];
        switch (lootGathered.lootType)
        {
            case TTEventDataModel.LootType.Heart:
                HeartsController.Instance.SwitchToUnlimited(lootGathered.amountOfLoot);
                ParticleSpawner.Instance.SpawnFromMiddleOfScreen(ParticleSpawner.ParticleObjects.Hearts, lootGathered.amountOfLoot);
                break;
            case TTEventDataModel.LootType.Hint:
                CurrencyController.Hints += lootGathered.amountOfLoot;
                ParticleSpawner.Instance.SpawnFromMiddleOfScreen(ParticleSpawner.ParticleObjects.Hints, lootGathered.amountOfLoot);
                break;
            case TTEventDataModel.LootType.Fan:
                CurrencyController.Fans += lootGathered.amountOfLoot;
                
                break;
            case TTEventDataModel.LootType.Coins:
                CurrencyController.Coins += lootGathered.amountOfLoot;
                ParticleSpawner.Instance.SpawnFromMiddleOfScreen(ParticleSpawner.ParticleObjects.Coins, lootGathered.amountOfLoot);
                break;
        }
        
    }

    private void StartLocalTournament()
    {
        if (_isNoInternet) return;
        switch (tournamentServerData.tournamentType)
        {
            case TournamentType.Level:
                _tournamentsView.ShowButtonLevels();
                break;
            case TournamentType.Star:
                _tournamentsView.ShowButtonStars();
                break;
        }
    }

    private void StartNewLocalTournament()
    {
        if (_isNoInternet) return;

        _tournamentsView.ShowTournamentEntryScreen(tournamentServerData.tournamentType);
        savedTournamentProgression = new TourLeaderboardPlayer();
        savedTournamentId = tournamentServerData.tournamentId;
        TournamentModel.localTimer = new RealTimeTimer(DateTime.Now, timeForNewEvents);
        GameController.SaveGameData();
        switch (tournamentServerData.tournamentType)
        {
            case TournamentType.Level:
                _tournamentsView.ShowButtonLevels();
                break;
            case TournamentType.Star:
                _tournamentsView.ShowButtonStars();
                break;
        }
    }

    private string GenerateTournamentId()
    {
        var numbersFromId = string.Join("", tournamentServerData.tournamentId.Where(char.IsDigit));
        int number = Int32.Parse(numbersFromId);
        return $"tour_{number + 1}";
    }

    private void UpdateServerCurrentTourInfo()
    {
        if (_isNoInternet) return;
        // Substitute the old tournamentServerData with a new
        TournamentType newTournamentType;
        newTournamentType = tournamentServerData.tournamentType == TournamentType.Level
            ? TournamentType.Star
            : TournamentType.Level;

        TournamentServerData newTournamentServerData = new TournamentServerData(newTournamentType, timeForNewEvents,
            GenerateTournamentId(), Timestamp.GetCurrentTimestamp());

        // Create a new collection with a new name for new tournament
        TournamentModel.tourInfoDocument.SetAsync(newTournamentServerData).ContinueWith(task =>
        {
            Debug.LogWarning("Server current tour data has been updated");
        });

        // Assign new server data to local tournament server data
        tournamentServerData = newTournamentServerData;
    }

    private void FixedUpdate()
    {
        if(_isNoInternet) return;
        if(isDataLoaded == false) return;

        if (TournamentModel.localTimer.IsDone)
        {
            // Tournament has ended but haven't updated the currentTourInfo in DB
            UpdateServerCurrentTourInfo();
            _tournamentRoomHandler.CloseCurrentRoomJoinNewFree();
            _tournamentRoomHandler.JoinFreeRoom(new TourLeaderboardPlayer());
            StartNewLocalTournament();
        }
        var timeSpan = TimeSpan.FromSeconds(TournamentModel.localTimer.TimeLeft);
        _tournamentsView.UpdateTimers($"{timeSpan.ToString(@"hh\:mm\:ss")}");
    }
}