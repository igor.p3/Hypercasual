﻿using System.Collections;
using UnityEngine;

namespace Matchmania
{
    [CreateAssetMenu(fileName = "SFX", menuName = "Matchmania/SFX", order = 2)]
    public class SFXConfig : ScriptableObject
    {
        public AudioClip buttonClick;
        public AudioClip pickUpItem;
        public AudioClip matchItems;
        public AudioClip victory;
        public AudioClip defeat;
        public AudioClip timerRemaining;
        public AudioClip starHitsUI;

        private bool isPlayingWinSFX;
        public void PlaySoundEffect(AudioClip audioClip)
        {
            AudioController.Instance.sfx.PlayOneShot(audioClip);
        }
        public void PlayVictorySFX(MonoBehaviour monoBehaviour)
        {
            if (isPlayingWinSFX == false)
            {
                monoBehaviour.StartCoroutine(PlayVoctorySound());
            }
        }
        IEnumerator PlayVoctorySound()
        {
            isPlayingWinSFX = true;

            AudioController.Instance.sfx.PlayOneShot(victory);

            yield return new WaitForSeconds(5);

            isPlayingWinSFX = false;
        }
    }
}