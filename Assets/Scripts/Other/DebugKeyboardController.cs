using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using IngameDebugConsole;
using Matchmania;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using VoxelBusters.Parser;
using JsonUtility = UnityEngine.JsonUtility;


public class DebugKeyboardController : MonoBehaviour
{
[SerializeField] private MainMenuView _mainMenuView;
[SerializeField] private Transform _particleSpawnTransform;
//#if DEBUG
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            //GetCoins(100);
            Debug.Log("Just added 100 coins");
            GameController.SaveGameData();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            CurrencyController.Stars += 10;
            CurrencyController.StarsChestCount += 10;
            _mainMenuView.UpdateMainMenuView();
            Debug.Log("Just added 10 stars");
            GameController.SaveGameData();
        }

        // if (Input.GetKeyDown(KeyCode.J))
        // {
        //     HeartsController.Instance.CurrentHeartsAmount--;
        //     Debug.Log("Removed 1 heart");
        //     GameController.SaveGameData();
        // }
        
        // if (Input.GetKeyDown(KeyCode.H))
        // {
        //     HeartsController.Instance.CurrentHeartsAmount++;
        //     Debug.Log("Added 1 heart");
        //     GameController.SaveGameData();
        // }
        
        // if (Input.GetKeyDown(KeyCode.U))
        // {
        //     HeartsController.Instance.SwitchToUnlimited(100);
        //     Debug.Log("Turned on unlimited hearts for 100 seconds");
        // }
        // if (Input.GetKeyDown(KeyCode.B))
        // {
        //     HeartsController.Instance.SwitchToLimited();
        //     Debug.Log("Turned off unlimited hearts");
        // }

        
        if (Input.GetKeyDown(KeyCode.Q))
        {
            CurrencyController.StarsChestCount += 500;
            CurrencyController.Stars += 500;
            _mainMenuView.UpdateMainMenuView();
            Debug.Log("Star Chest is filled");
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            CurrencyController.Hints += 500;

            Debug.Log("500 Hints added");
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            //LevelUP();
        }
    }


    
//#endif
    
    private void Start()
    {
        // DebugLogConsole.AddCommand("LevelUp", "Adds 1 level to you, watch it, it DOESN'T GIVE YOU ACHIEVEMENTS", LevelUP);
        // DebugLogConsole.AddCommand<int>("GetTokens", "Adds n amount of tokens to you - default 1", GetTokens);
        // DebugLogConsole.AddCommand("NextTTEvent", "Activates next TT event", NextTTEvent);
        // DebugLogConsole.AddCommand("GetCurrentTTEventName", "Gives the name of current TT event", GetCurrentTTEventName);
         DebugLogConsole.AddCommand<string, long>("AddInDatabase", "Adds row in database", AddInDatabase);
        // DebugLogConsole.AddCommand<int>("GetHearts", "Adds specified amount of hearts to the player", GetHearts);
        DebugLogConsole.AddCommand("GetCultureInfo", "Displays info about current region", GetCultureInfo);

        DebugLogConsole.AddCommand<int>("Level", "Displays info about current region", TurnOnLevel);
    }

    private void TurnOnLevel(int level)
    {
        LevelController.Instance.StartLevel(level);
    }
    private void LevelUP()
    {
        LevelController.CurrentLevelIndex++;
        _mainMenuView.UpdateMainMenuView();
        Debug.Log("Leveled Up !");
    }

    private void GetTokens(int amountToGet = 1)
    {
        if (TTEventController.currentEvent == TTEventDataModel.TTEventName.PostGodsEscapePause ||
            TTEventController.currentEvent == TTEventDataModel.TTEventName.NeverStarted ||
            TTEventController.currentEvent == TTEventDataModel.TTEventName.PostSummerBreezePause ||
            TTEventController.currentEvent == TTEventDataModel.TTEventName.PostZooRamaPause)
        {
            Debug.Log("There is no active TT event, try NextTTEvent() command to start one first");
        }
        TTEventController.Instance.TokensCollected += amountToGet;
        TTEventController.Instance.UpdateOpenedMainMenu();
    }

    private void NextTTEvent()
    {
        TTEventController.Instance.StartNextEvent();
    }

    private void GetCurrentTTEventName()
    {
        Debug.Log($"Current TT event is: {TTEventController.currentEvent.ToString()}");
    }

    private void AddInDatabase(string playerName, long playerScore)
    {
        LeaderboardController.TestSendPlayerData(playerName, playerScore);
    }

    private void GetCoins(int coinsAmount)
    {
        CurrencyController.Coins += coinsAmount;
        _mainMenuView.UpdateMainMenuView();
        ParticleSpawner.Instance.SpawnRewardVFX(ParticleSpawner.ParticleObjects.Coins, coinsAmount, _particleSpawnTransform.position);
    }

    private void GetHearts(int heartAmount)
    {
        HeartsController.Instance.CurrentHeartsAmount += heartAmount;
        _mainMenuView.UpdateMainMenuView();
        ParticleSpawner.Instance.SpawnRewardVFX(ParticleSpawner.ParticleObjects.Hearts, heartAmount, _particleSpawnTransform.position);
    }

    private void GetCultureInfo()
    {
        var region = RegionInfo.CurrentRegion;
        var localZone = TimeZone.CurrentTimeZone;
        var cultureInfo = CultureInfo.CurrentCulture;
        var cultureUIInfo = CultureInfo.CurrentUICulture;
        Debug.LogWarning($"Current Region info is {region.DisplayName}");
        Debug.LogWarning($"Local timezone culture is {localZone.StandardName}");
        Debug.LogWarning($"Local CultureInfo culture is {cultureInfo.DisplayName}");
        Debug.LogWarning($"Local CultureUIInfo culture is {cultureUIInfo.DisplayName}");
        StartCoroutine(GetCountryByIP());
    }
    
    
    private IEnumerator GetCountryByIP()
    {
        
        if (InternetCheckerController.isNoInternetConnection)
        {
            Debug.LogWarning("No internet to check IP's country");
            yield break;
        }
        
        UnityWebRequest www = UnityWebRequest.Get("https://ipapi.co/country/");
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogWarning(www.error);
        }
        else
        {
            var info = www.downloadHandler.text;
            Debug.LogWarning(info);

            if (info != null)
            {
                var region = new RegionInfo(info);
                Debug.LogWarning(region.EnglishName);
            }
            else
            {
                Debug.LogError("Couldn't read json file");
            }
        }
    }
[System.Serializable]
    private class IpInfo
    {
        //country
        public string country { get; set; }
    }
}
